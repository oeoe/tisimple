package com.example.tisimple.generator;

import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.core.TiGlobalConfig;
import com.gitee.oeoe.tisimple.entityfactory.EntityBuilderFactory;
import com.gitee.oeoe.tisimple.page.mysql.MysqlPageFactory;
import com.mysql.cj.jdbc.MysqlDataSource;

/**
 * 实体生成工厂
 */
public class EntityGenerator {
    public static final String url = "jdbc:mysql://localhost:3306/alis";
    public static final String username = "mime";
    public static final String password = "123";
    public static final String driver = "com.mysql.cj.jdbc.Driver";

    static {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        TiGlobalConfig.setGlobalPageFactory(MysqlPageFactory.class, 5);//注册全局分页工厂,设置默认分页大小
        TiDBPoolManager.register(dataSource);//注册连接池
    }

    public static void main(String[] args) {
        EntityBuilderFactory.buildFromYaml();
    }
}