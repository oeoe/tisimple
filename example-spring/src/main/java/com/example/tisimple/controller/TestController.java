package com.example.tisimple.controller;

import com.example.tisimple.domain.Order;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.page.IPage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping("/test")
    public IPage<User> test(User user) {
        IPage<User> query = user.page();
        System.out.println(query.toString());
        return query;
    }

    @RequestMapping("/test2")
    public IPage<Order> test2(Order order) {
        IPage<Order> query = order.page();
        System.out.println(query.toString());
        return query;
    }
}
