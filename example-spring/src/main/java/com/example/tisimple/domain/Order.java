package com.example.tisimple.domain;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;
import com.gitee.oeoe.tisimple.extra.keygenertefactory.UUIDFactory;
import com.gitee.oeoe.tisimple.extra.matchtypefactory.GeRuleFactory;

@TiTable(value="k_order")
public class Order extends TiEntity<Order> {
	@TiColumn(value="id",comment="订单号",nullAble=false,key=true,keyGenerateFactory = UUIDFactory.class)
	private String id;
	@TiColumn(value="status",comment="订单状态，0未支付,1已支付，2待发货，3已发货，4待收货，5已完成")
	private Integer status;
	@TiColumn(value="order_time",comment="下单时间", matchRuleFactory = GeRuleFactory.class)
	private java.time.LocalDateTime orderTime;
	@TiColumn(value="total_money",comment="订单总金额")
	private java.math.BigDecimal totalMoney;
	@TiColumn(value="actual_amount_paid",comment="实际支付金额")
	private java.math.BigDecimal actualAmountPaid;
	@TiColumn(value="payment",comment="支付方式")
	private String payment;
	@TiColumn(value="user_id",comment="顾客ID")
	private String userId;
	@TiColumn(value="user_name",comment="顾客昵称")
	private String userName;

	public void setId(String id){this.id=id;}

	public String getId(){return this.id;}

	public void setStatus(Integer status){this.status=status;}

	public Integer getStatus(){return this.status;}

	public void setOrderTime(java.time.LocalDateTime orderTime){this.orderTime=orderTime;}

	public java.time.LocalDateTime getOrderTime(){return this.orderTime;}

	public void setTotalMoney(java.math.BigDecimal totalMoney){this.totalMoney=totalMoney;}

	public java.math.BigDecimal getTotalMoney(){return this.totalMoney;}

	public void setActualAmountPaid(java.math.BigDecimal actualAmountPaid){this.actualAmountPaid=actualAmountPaid;}

	public java.math.BigDecimal getActualAmountPaid(){return this.actualAmountPaid;}

	public void setPayment(String payment){this.payment=payment;}

	public String getPayment(){return this.payment;}

	public void setUserId(String userId){this.userId=userId;}

	public String getUserId(){return this.userId;}

	public void setUserName(String userName){this.userName=userName;}

	public String getUserName(){return this.userName;}


	@Override
	public String toString() {
		return "Order{"+
			"id="+id+","+
			"status="+status+","+
			"orderTime="+orderTime+","+
			"totalMoney="+totalMoney+","+
			"actualAmountPaid="+actualAmountPaid+","+
			"payment="+payment+","+
			"userId="+userId+","+
			"userName="+userName+
		"}";
	}
}