package com.example.tisimple.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Configuration
public class DateConverterConfig {
    @Bean
    public Converter<String, LocalDate> localDateConverter() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String source) {
                try {
                    return LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                } catch (DateTimeParseException e) {
                    try {
                        return LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy/MM/dd"));

                    } catch (DateTimeParseException e1) {
                        return LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy|MM|dd"));
                    }
                }
            }
        };
    }

    @Bean
    public Converter<String, LocalDateTime> localDateTimeConverter() {
        return new Converter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String source) {
                try {
                    return LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                } catch (DateTimeParseException e) {
                    try {
                        return LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
                    } catch (DateTimeParseException e1) {
                        return LocalDateTime.of(LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd")), LocalTime.of(0, 0, 0));
                    }
                }
            }
        };

    }
}