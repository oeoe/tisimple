package com.example.tisimple.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.core.TiGlobalConfig;
import com.gitee.oeoe.tisimple.page.IPageFactory;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@SpringBootConfiguration
@ConfigurationProperties(prefix = "tisimple.datasource")
public class TiConfiguration {
    private String driverClassName;
    private String url;
    private String username;
    private String password;
    private Class<? extends IPageFactory> pageFactory;
    private Integer defaultPageSize;


    @Bean
    public DataSource getDataSource() {
        /*DruidDataSource source = new DruidDataSource();
        source.setDriverClassName(this.driverClassName);
        source.setUrl(this.url);
        source.setUsername(this.username);
        source.setPassword(this.password);
        source.setMaxActive(3);*/
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    ;

    @Bean
    public TiGlobalConfig tiConfig(DataSource dataSource) {
        TiGlobalConfig cfg = new TiGlobalConfig();
        //注册数据源
        TiDBPoolManager.register(dataSource);
        //设置分页工厂
        TiGlobalConfig.setGlobalPageFactory(pageFactory, defaultPageSize);
        //如果有自定义模版函数,注册模版函数
        //SqlTemplate.registerFunc("foreach", ForeachFunc.class);
        return cfg;
    }

    /**
     * fastjson 配置
     *
     * @return 转换器
     */
    @Bean
    public HttpMessageConverter<?> configureMessageConverters() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setSerializerFeatures(
                // 保留map空的字段
                SerializerFeature.WriteMapNullValue,
                // 将String类型的null转成""
                SerializerFeature.WriteNullStringAsEmpty,
                // 将Number类型的null转成0
                SerializerFeature.WriteNullNumberAsZero,
                // 将List类型的null转成[]
                SerializerFeature.WriteNullListAsEmpty,
                // 将Boolean类型的null转成false
                SerializerFeature.WriteNullBooleanAsFalse,
                // 避免循环引用
                SerializerFeature.DisableCircularReferenceDetect,
                //不序列化transient的字段
                SerializerFeature.SkipTransientField);

        converter.setFastJsonConfig(config);
        converter.setDefaultCharset(StandardCharsets.UTF_8);
        List<MediaType> mediaTypeList = new ArrayList<>();
        // 解决中文乱码问题，相当于在Controller上的@RequestMapping中加了个属性produces = "application/json"
        mediaTypeList.add(MediaType.APPLICATION_JSON);
        converter.setSupportedMediaTypes(mediaTypeList);
        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
        return converter;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Class<? extends IPageFactory> getPageFactory() {
        return pageFactory;
    }

    public void setPageFactory(Class<? extends IPageFactory> pageFactory) {
        this.pageFactory = pageFactory;
    }

    public Integer getDefaultPageSize() {
        return defaultPageSize;
    }

    public void setDefaultPageSize(Integer defaultPageSize) {
        this.defaultPageSize = defaultPageSize;
    }

}
