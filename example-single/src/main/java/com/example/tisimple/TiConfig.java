package com.example.tisimple;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.core.TiGlobalConfig;
import com.gitee.oeoe.tisimple.page.mysql.MysqlPageFactory;

/**
 * 只需要创建任何一个连接池,注册到TiDBPoolManager即可
 */
public class TiConfig {

    public static final String url = "jdbc:mysql://localhost:3306/alis";
    public static final String username = "mime";
    public static final String password = "123";
    public static final String driver = "com.mysql.cj.jdbc.Driver";

    static {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        TiGlobalConfig.setGlobalPageFactory(MysqlPageFactory.class,5);//注册全局分页工厂,设置默认分页大小
        TiDBPoolManager.register(dataSource);//注册连接池
        //SqlTemplate.registerFunc("foreach", ForeachFunc.class);//如果有自定义模版函数,注册模版函数
    }

    public static long id() {
        return System.nanoTime();
    }
}
