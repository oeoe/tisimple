package com.example.tisimple.domain;


import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;

public class Example extends TiEntity<Example> {
    private Integer age;
    @TiColumn("order_id")
    private String orderId;
    @TiColumn("user_name")
    private String userName;


    @Override
    public String toString() {
        return "Example{" +
                "age=" + age +
                ", orderId='" + orderId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}