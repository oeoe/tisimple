package com.example.tisimple.domain;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value="m_type")
public class MysqlType extends TiEntity<MysqlType> {
	@TiColumn(value="id",nullAble=false,key=true)
	private java.lang.String id;
	@TiColumn(value="order",nullAble=false)
	private java.lang.Integer order;
	@TiColumn(value="TINYINT")
	private java.lang.Integer tinyint;
	@TiColumn(value="SMALLINT")
	private java.lang.Integer smallint;
	@TiColumn(value="INTEGER")
	private java.lang.Integer integer;
	@TiColumn(value="BIGINT")
	private java.lang.Long bigint;
	@TiColumn(value="DECIMAL")
	private java.math.BigDecimal decimal;
	@TiColumn(value="DATE")
	private java.sql.Date date;
	@TiColumn(value="TIME")
	private java.sql.Time time;
	@TiColumn(value="YEAR")
	private java.sql.Date year;//mysql中year类型的数据建议用int类型,在java对应的date类型是无法进行插入和更新的
	@TiColumn(value="DATETIME")
	private java.time.LocalDateTime datetime;
	@TiColumn(value="TIMESTAMP")
	private java.sql.Timestamp timestamp;
	@TiColumn(value="CHAR")
	private java.lang.String char1;
	@TiColumn(value="VARCHAR")
	private java.lang.String varchar;
	@TiColumn(value="TINYBLOB")
	private byte[] tinyblob;
	@TiColumn(value="TINYTEXT")
	private java.lang.String tinytext;
	@TiColumn(value="BLOB")
	private byte[] blob;
	@TiColumn(value="TEXT")
	private java.lang.String text;
	@TiColumn(value="MEDIUMBLOB")
	private byte[] mediumblob;
	@TiColumn(value="MEDIUMTEXT")
	private java.lang.String mediumtext;
	@TiColumn(value="LONGBLOB")
	private byte[] longblob;
	@TiColumn(value="LONGTEXT")
	private java.lang.String longtext;

	public void setId(java.lang.String id){this.id=id;}

	public java.lang.String getId(){return this.id;}

	public void setOrder(java.lang.Integer order){this.order=order;}

	public java.lang.Integer getOrder(){return this.order;}

	public void setTinyint(java.lang.Integer tinyint){this.tinyint=tinyint;}

	public java.lang.Integer getTinyint(){return this.tinyint;}

	public void setSmallint(java.lang.Integer smallint){this.smallint=smallint;}

	public java.lang.Integer getSmallint(){return this.smallint;}

	public void setInteger(java.lang.Integer integer){this.integer=integer;}

	public java.lang.Integer getInteger(){return this.integer;}

	public void setBigint(java.lang.Long bigint){this.bigint=bigint;}

	public java.lang.Long getBigint(){return this.bigint;}

	public void setDecimal(java.math.BigDecimal decimal){this.decimal=decimal;}

	public java.math.BigDecimal getDecimal(){return this.decimal;}

	public void setDate(java.sql.Date date){this.date=date;}

	public java.sql.Date getDate(){return this.date;}

	public void setTime(java.sql.Time time){this.time=time;}

	public java.sql.Time getTime(){return this.time;}

	public void setYear(java.sql.Date year){this.year=year;}

	public java.sql.Date getYear(){return this.year;}

	public void setDatetime(java.time.LocalDateTime datetime){this.datetime=datetime;}

	public java.time.LocalDateTime getDatetime(){return this.datetime;}

	public void setTimestamp(java.sql.Timestamp timestamp){this.timestamp=timestamp;}

	public java.sql.Timestamp getTimestamp(){return this.timestamp;}

	public void setChar(java.lang.String char1){this.char1=char1;}

	public java.lang.String getChar(){return this.char1;}

	public void setVarchar(java.lang.String varchar){this.varchar=varchar;}

	public java.lang.String getVarchar(){return this.varchar;}

	public void setTinyblob(byte[] tinyblob){this.tinyblob=tinyblob;}

	public byte[] getTinyblob(){return this.tinyblob;}

	public void setTinytext(java.lang.String tinytext){this.tinytext=tinytext;}

	public java.lang.String getTinytext(){return this.tinytext;}

	public void setBlob(byte[] blob){this.blob=blob;}

	public byte[] getBlob(){return this.blob;}

	public void setText(java.lang.String text){this.text=text;}

	public java.lang.String getText(){return this.text;}

	public void setMediumblob(byte[] mediumblob){this.mediumblob=mediumblob;}

	public byte[] getMediumblob(){return this.mediumblob;}

	public void setMediumtext(java.lang.String mediumtext){this.mediumtext=mediumtext;}

	public java.lang.String getMediumtext(){return this.mediumtext;}

	public void setLongblob(byte[] longblob){this.longblob=longblob;}

	public byte[] getLongblob(){return this.longblob;}

	public void setLongtext(java.lang.String longtext){this.longtext=longtext;}

	public java.lang.String getLongtext(){return this.longtext;}


	@Override
	public String toString() {
		return "MysqlType{"+
			"id="+id+","+
			"order="+order+","+
			"tinyint="+tinyint+","+
			"smallint="+smallint+","+
			"integer="+integer+","+
			"bigint="+bigint+","+
			"decimal="+decimal+","+
			"date="+date+","+
			"time="+time+","+
			"year="+year+","+
			"datetime="+datetime+","+
			"timestamp="+timestamp+","+
			"char="+char1+","+
			"varchar="+varchar+","+
			"tinyblob="+tinyblob+","+
			"tinytext="+tinytext+","+
			"blob="+blob+","+
			"text="+text+","+
			"mediumblob="+mediumblob+","+
			"mediumtext="+mediumtext+","+
			"longblob="+longblob+","+
			"longtext="+longtext+
		"}";
	}
}