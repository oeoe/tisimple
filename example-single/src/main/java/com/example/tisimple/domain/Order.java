package com.example.tisimple.domain;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;
import com.gitee.oeoe.tisimple.extra.keygenertefactory.UUIDFactory;

@TiTable(value="k_order")
public class Order extends TiEntity<Order> {
	@TiColumn(value="id",comment="订单号",nullAble=false,key=true,keyGenerateFactory = UUIDFactory.class)
	private java.lang.String id;
	@TiColumn(value="status",comment="订单状态，0未支付,1已支付，2待发货，3已发货，4待收货，5已完成")
	private java.lang.Integer status;
	@TiColumn(value="order_time",comment="下单时间")
	private java.time.LocalDateTime orderTime;
	@TiColumn(value="total_money",comment="订单总金额")
	private java.math.BigDecimal totalMoney;
	@TiColumn(value="actual_amount_paid",comment="实际支付金额")
	private java.math.BigDecimal actualAmountPaid;
	@TiColumn(value="payment",comment="支付方式")
	private java.lang.String payment;
	@TiColumn(value="user_id",comment="顾客ID")
	private java.lang.String userId;
	@TiColumn(value="user_name",comment="顾客昵称")
	private java.lang.String userName;

	public void setId(java.lang.String id){this.id=id;}

	public java.lang.String getId(){return this.id;}

	public void setStatus(java.lang.Integer status){this.status=status;}

	public java.lang.Integer getStatus(){return this.status;}

	public void setOrderTime(java.time.LocalDateTime orderTime){this.orderTime=orderTime;}

	public java.time.LocalDateTime getOrderTime(){return this.orderTime;}

	public void setTotalMoney(java.math.BigDecimal totalMoney){this.totalMoney=totalMoney;}

	public java.math.BigDecimal getTotalMoney(){return this.totalMoney;}

	public void setActualAmountPaid(java.math.BigDecimal actualAmountPaid){this.actualAmountPaid=actualAmountPaid;}

	public java.math.BigDecimal getActualAmountPaid(){return this.actualAmountPaid;}

	public void setPayment(java.lang.String payment){this.payment=payment;}

	public java.lang.String getPayment(){return this.payment;}

	public void setUserId(java.lang.String userId){this.userId=userId;}

	public java.lang.String getUserId(){return this.userId;}

	public void setUserName(java.lang.String userName){this.userName=userName;}

	public java.lang.String getUserName(){return this.userName;}


	@Override
	public String toString() {
		return "Order{"+
			"id="+id+","+
			"status="+status+","+
			"orderTime="+orderTime+","+
			"totalMoney="+totalMoney+","+
			"actualAmountPaid="+actualAmountPaid+","+
			"payment="+payment+","+
			"userId="+userId+","+
			"userName="+userName+
		"}";
	}
}