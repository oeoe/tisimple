package com.example.tisimple.domain;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value="s_user")
public class User extends TiEntity<User> {
	@TiColumn(value="id",comment="主键",nullAble=false,key=true,autoIncrement=true)
	private java.lang.Integer id;
	@TiColumn(value="name",comment="用户名")
	private java.lang.String name;
	@TiColumn(value="age",comment="年龄")
	private java.lang.Integer age;
	@TiColumn(value="address",comment="地址")
	private java.lang.String address;

	public void setId(java.lang.Integer id){this.id=id;}

	public java.lang.Integer getId(){return this.id;}

	public void setName(java.lang.String name){this.name=name;}

	public java.lang.String getName(){return this.name;}

	public void setAge(java.lang.Integer age){this.age=age;}

	public java.lang.Integer getAge(){return this.age;}

	public void setAddress(java.lang.String address){this.address=address;}

	public java.lang.String getAddress(){return this.address;}


	@Override
	public String toString() {
		return "User{"+
			"id="+id+","+
			"name="+name+","+
			"age="+age+","+
			"address="+address+
		"}";
	}
}