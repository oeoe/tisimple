package com.example.tisimple.entitybuilder;

import com.example.tisimple.TiConfig;
import com.gitee.oeoe.tisimple.entityfactory.EntityBuilderFactory;

/**
 * 通过yaml配置，从表中生成实体
 *
 */
public class GenerateFormYaml {
    public static final Long serialVersionUID = TiConfig.id();

    public static void main(String[] args) {
        //classpath 目录下配置一个 entity.yaml 文件即可
        EntityBuilderFactory.buildFromYaml();
    }
}
