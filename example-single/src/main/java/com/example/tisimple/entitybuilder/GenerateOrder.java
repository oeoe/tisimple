package com.example.tisimple.entitybuilder;

import com.example.tisimple.TiConfig;
import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.entityfactory.EntityBuilderFactory;
import com.gitee.oeoe.tisimple.entityfactory.mysql.MysqlTableInfoFactory;
import com.gitee.oeoe.tisimple.exception.ConditionNotMetException;
import com.gitee.oeoe.tisimple.exception.ObtainConnectionException;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 从表中生成实体
 */
public class GenerateOrder {
    public static final Long serialVersionUID = TiConfig.id();

    public static void main(String[] args) throws ObtainConnectionException, ConditionNotMetException, SQLException, IOException {
        String path = new File("").getAbsoluteFile().getPath();
        path = path + "/example-single/src/main/java/com/example/tisimple/domain/";
        String className = "Order";
        try (Connection connection = TiDBPoolManager.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from k_order limit 1");
            ResultSet resultSet = preparedStatement.executeQuery();
            EntityBuilderFactory.build()
                    .setPackName("com.example.tisimple.domain")
                    .setClassName("Order")
                    .setTableName("k_order")
                    .setResultSet(resultSet)
                    .setOutPath(path)
                    .setInfoFactory(MysqlTableInfoFactory.class)
                    .generate();
            System.out.println("\n================" + className + " 实体生成成功======================\n");
        }

    }
}
