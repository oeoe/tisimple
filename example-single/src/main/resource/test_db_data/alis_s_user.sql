-- auto-generated definition
create table s_user
(
    id      int auto_increment comment '主键'  primary key,
    name    varchar(32)   null comment '用户名',
    age     int default 0 null comment '年龄',
    address varchar(1024) null comment '地址'
);

INSERT INTO alis.s_user (id, name, age, address) VALUES (1, '掌声', 118, '大海');
INSERT INTO alis.s_user (id, name, age, address) VALUES (2, '鲜花', 118, '山坡');
INSERT INTO alis.s_user (id, name, age, address) VALUES (3, '烈阳', 11, '天空');
INSERT INTO alis.s_user (id, name, age, address) VALUES (4, '你在哪儿', 118, '我在你身边');
INSERT INTO alis.s_user (id, name, age, address) VALUES (5, '你在哪儿', 118, '我在你身边');
INSERT INTO alis.s_user (id, name, age, address) VALUES (6, '你在哪儿', 118, '我在你身边');
INSERT INTO alis.s_user (id, name, age, address) VALUES (138430, '平方', 19, '共和国');


create table m_type
(
    `id`    varchar(64)     not null  primary key ,
    `order`    int     not null  unique key auto_increment ,
    `TINYINT`    tinyint     null,
    `SMALLINT`   smallint    null,
    `INTEGER`    int         null,
    `BIGINT`     bigint      null,
    `DECIMAL`    decimal     null,
    DATE         date        null,
    TIME         time        null,
    YEAR         year        null,
    DATETIME     datetime    null,
    TIMESTAMP    timestamp   null,
    `CHAR`       char        null,
    `VARCHAR`    varchar(32) null,
    `TINYBLOB`   tinyblob    null,
    `TINYTEXT`   tinytext    null,
    `BLOB`       blob        null,
    TEXT         text        null,
    `MEDIUMBLOB` mediumblob  null,
    `MEDIUMTEXT` mediumtext  null,
    `LONGBLOB`   longblob    null,
    `LONGTEXT`   longtext    null
)
    comment '类型测试';


INSERT INTO alis.m_type (id,`TINYINT`, `SMALLINT`, `INTEGER`, `BIGINT`, `DECIMAL`, DATE, TIME, YEAR, DATETIME, TIMESTAMP, `CHAR`, `VARCHAR`, `TINYBLOB`, `TINYTEXT`, `BLOB`, TEXT, `MEDIUMBLOB`, `MEDIUMTEXT`, `LONGBLOB`, `LONGTEXT`) VALUES ('zero',1, 2, 3, 4, 5, '2022-01-09', null, 2021, '2022-01-09 11:56:13', '2022-01-09 11:56:19', 'a', 'b', 0xEFBBBF31, 'c', 0xEFBBBF32, 'd', 0xEFBBBF33, 'e', 0x34, 'f');