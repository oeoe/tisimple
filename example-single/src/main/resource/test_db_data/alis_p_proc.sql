#带输出参数
DROP PROCEDURE IF EXISTS getOrderById;
DELIMITER // -- 定义存储过程结束符号为//
CREATE PROCEDURE getOrderById(IN oid INT(11), OUT tmoney decimal, OUT st INT(11)) -- 定义输入与输出参数
    COMMENT 'query order by their id' -- 提示信息
    SQL SECURITY DEFINER -- DEFINER指明只有定义此SQL的人才能执行，MySQL默认也是这个
BEGIN
    SELECT total_money, status INTO tmoney , st FROM k_order WHERE id = oid; -- 分号要加
END // -- 结束符要加
DELIMITER ; -- 重新定义存储过程结束符为分号

call getOrderById(1, @id, @status);
select @id, @status;

select *
from k_order
limit 5;


#单参数
DROP PROCEDURE IF EXISTS getOrderById2;
DELIMITER // -- 定义存储过程结束符号为//
CREATE PROCEDURE getOrderById2(IN oid INT(11)) -- 定义输入与输出参数
    COMMENT 'query order by their id' -- 提示信息
    SQL SECURITY DEFINER -- DEFINER指明只有定义此SQL的人才能执行，MySQL默认也是这个
BEGIN
    SELECT total_money, status FROM k_order WHERE id = oid; -- 分号要加
    SELECT total_money FROM k_order WHERE id = oid; -- 分号要加
END // -- 结束符要加
DELIMITER ; -- 重新定义存储过程结束符为分号

call getOrderById2(1);


#多参数
DELIMITER // -- 定义存储过程结束符号为//
CREATE PROCEDURE getOrderById3(IN oid INT(11), IN oname VARCHAR(32)) -- 定义输入与输出参数
    COMMENT 'query order by their id' -- 提示信息
    SQL SECURITY DEFINER -- DEFINER指明只有定义此SQL的人才能执行，MySQL默认也是这个
BEGIN
    SELECT total_money, status FROM k_order WHERE id = oid and user_name = oname; -- 分号要加
    SELECT total_money FROM k_order WHERE id = oid and user_name = oname; -- 分号要加
END // -- 结束符要加
DELIMITER ;