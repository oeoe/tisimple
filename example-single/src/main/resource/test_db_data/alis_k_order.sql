create table k_order
(
    id                 varchar(64) not null comment '订单号'
        primary key,
    status             int         null comment '订单状态，0未支付,1已支付，2待发货，3已发货，4待收货，5已完成',
    order_time         datetime    null comment '下单时间',
    total_money        decimal     null comment '订单总金额',
    actual_amount_paid decimal     null comment '实际支付金额',
    payment            varchar(32) null comment '支付方式',
    user_id            varchar(64) null comment '顾客ID',
    user_name          varchar(64) null comment '顾客昵称'
)
    comment '订单';

INSERT INTO alis.k_order (id, status, order_time, total_money, actual_amount_paid, payment, user_id, user_name) VALUES ('1', 1, '2022-01-12 09:59:06', 188, 100, 'wx', '1', null);
INSERT INTO alis.k_order (id, status, order_time, total_money, actual_amount_paid, payment, user_id, user_name) VALUES ('b61d932caf954530897fd24524a39551', 1, '2022-01-12 10:08:59', 335, 236, 'zfb', null, null);
INSERT INTO alis.k_order (id, status, order_time, total_money, actual_amount_paid, payment, user_id, user_name) VALUES ('b8da1ee90d454b2f9776f77071492774', 1, '2022-01-12 12:33:39', 335, 236, 'zfb', '1', '爱德华');
INSERT INTO alis.k_order (id, status, order_time, total_money, actual_amount_paid, payment, user_id, user_name) VALUES ('fb04c149da4d422aa963634afa696d12', 1, '2022-01-12 10:06:57', 335, 236, 'zfb', null, null);