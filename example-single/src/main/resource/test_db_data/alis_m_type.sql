
create table m_type
(
    `id`    varchar(64)     not null  primary key ,
    `order`    int     not null  unique key auto_increment ,
    `TINYINT`    tinyint     null,
    `SMALLINT`   smallint    null,
    `INTEGER`    int         null,
    `BIGINT`     bigint      null,
    `DECIMAL`    decimal     null,
    DATE         date        null,
    TIME         time        null,
    YEAR         year        null,
    DATETIME     datetime    null,
    TIMESTAMP    timestamp   null,
    `CHAR`       char        null,
    `VARCHAR`    varchar(32) null,
    `TINYBLOB`   tinyblob    null,
    `TINYTEXT`   tinytext    null,
    `BLOB`       blob        null,
    TEXT         text        null,
    `MEDIUMBLOB` mediumblob  null,
    `MEDIUMTEXT` mediumtext  null,
    `LONGBLOB`   longblob    null,
    `LONGTEXT`   longtext    null
)
    comment '类型测试';


INSERT INTO alis.m_type (id,`TINYINT`, `SMALLINT`, `INTEGER`, `BIGINT`, `DECIMAL`, DATE, TIME, YEAR, DATETIME, TIMESTAMP, `CHAR`, `VARCHAR`, `TINYBLOB`, `TINYTEXT`, `BLOB`, TEXT, `MEDIUMBLOB`, `MEDIUMTEXT`, `LONGBLOB`, `LONGTEXT`) VALUES ('zero',1, 2, 3, 4, 5, '2022-01-09', null, 2021, '2022-01-09 11:56:13', '2022-01-09 11:56:19', 'a', 'b', 0xEFBBBF31, 'c', 0xEFBBBF32, 'd', 0xEFBBBF33, 'e', 0x34, 'f');