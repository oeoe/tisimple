package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.exception.UnPackResultSetException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模版方法使用
 */
class TemplateFuncTest {
    public static final Long serialVersionUID = TiConfig.id();


    @Test
    public void test() throws SQLException, UnPackResultSetException {
        ISQL template = SqlFactory.sql("select * from s_user");
        ArrayList<String> list = new ArrayList<>();
        list.add("鲜花");
        list.add("烈阳");
        List<Map<String, Object>> maps = TiSimpleCore.queryList(template);
        System.out.println(maps);
        System.out.println("===================");
        ISQL SQL2 = SqlFactory.sql("select * from s_user [foreach@list#where  name in (|,|)]", list);
        List<Map<String, Object>> maps1 = TiSimpleCore.queryList(SQL2);
        System.out.println(maps1);
    }

    @Test
    public void test2() {
        ISQL template = SqlFactory.sql("select * from xx where a={2} and b={3} and c={1}", "c", "a", "b");
        System.out.println("==============================");
        System.out.println(template.getParsedSql());
        System.out.println("==============================1");

        HashMap<String, Object> p = new HashMap<>();
        p.put("name", "mim");
        p.put("age", 11);
        template = SqlFactory.sql("select * from xx where a={name} and b={age} and c={address}", p);
        System.out.println("==============================");
        System.out.println(template.getParsedSql());
        System.out.println("==============================2");

        template = SqlFactory.sql("select * from xx where 1=1 [null@name#|and a={name}] and b={age} and c={address}", p);
        System.out.println("==============================");
        System.out.println(template.getParsedSql());
        System.out.println("==============================3");

        User user = new User();
        user.setName("ma");
        user.setAge(33);
        ArrayList<Object> ls = new ArrayList<>();
        ls.add(101);
        ls.add(102);
        ls.add(103);
        ls.add("a");
        ls.add("b");
        ls.add("v");
        template = SqlFactory.sql("select * from xx where a={name} and b={age} [null@adress#[foreach@someList#and host in (|,|)]|and c={address}] and [null@name#name='123'|name={name}] ", user);
        template.addParam("someList", ls);//添加额外参数到参数空间
        System.out.println("==============================");
        System.out.println(template.getParsedSql());
        System.out.println("==============================4");
    }
}