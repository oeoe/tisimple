package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.Example;
import com.example.tisimple.domain.Order;
import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.page.mysql.MysqlPageFactory;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 基于实体的进阶用法示例
 */
class AdvanceTest {
    public static final Long serialVersionUID = TiConfig.id();

    @Test
    void insert() {
        Order order = new Order();
        order.setOrderTime(LocalDateTime.now());
        order.setStatus(1);
        order.setPayment("zfb");
        order.setTotalMoney(BigDecimal.valueOf(335));
        order.setActualAmountPaid(BigDecimal.valueOf(236.23));
        order.setUserId("1");
        order.setUserName("爱德华");
        int insert = order.insert();
        System.out.println(insert);
        System.out.println("=============也可以指定主键自动生成工厂=============");
        //在id 列的@TiColumn注解申明属性keyGenerateFactory 指定主键生成工厂UUIDFactory.class
        //使用主键自动生成,看一下生成的主键
        if (1 == insert) {
            System.out.println(order.getId());
        }
        //自定义sql,对于单表的增删改,不建议使用自定义的sql,提供这些接口只是方便在某些极端业务下有可用的处理方式
        String sql ="insert  into k_order(id,status,user_id) values({id},{status},{userId})";
        order.setId("1234567a");
        int insert1 = order.insert(sql);
        System.out.println(insert1);
    }

    @Test
    void delete() {
        Order condition = new Order();
        condition.setStatus(5);
        System.out.println(condition.delete());
        //如果默认条件匹配不符合,也可以自定义删除sql
        System.out.println("=============如果默认条件匹配不符合,也可以自定义删除sql===============");
        condition.setOrderTime(LocalDateTime.now());
        String sql = "delete from k_order where order_time>{orderTime}";
        System.out.println(condition.delete(sql));
    }

    @Test
    void update() {
        Order condition = new Order();
        condition.setId("12");
        Order order = new Order();
        order.setUserName("abc");
        System.out.println(condition.update(order));
        //与删除,都可以自定义更新sql
        System.out.println("========自定义更新SQL==========");
        //比如需要多表更新
        condition.setOrderTime(LocalDateTime.now());
        String sql = "update k_order,s_user set k_order.user_name=s_user.name where  k_order.user_id=s_user.id and k_order.order_time>{orderTime}";
        System.out.println(condition.update(sql));
        //如果你需要添加额外的参数，可以这样写
        System.out.println("========多表更新==========");
        ArrayList<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(2);
        statusList.add(3);
        sql = "update k_order,s_user set k_order.user_name=s_user.name where  k_order.user_id=s_user.id and k_order.order_time>{orderTime} and status in [foreach@list#(|,|)]";
        ISQL template = SqlFactory.sql(sql, condition).addParam("list", statusList);//statusList是一个列表,或是其他值
        System.out.println(condition.update(template));
    }


    @Test
    void query() {
        Order condition = new Order();
        List<Order> list = condition.query();
        //设置查询条件值,默认所有字段匹配类型为等于
        condition.setUserId("1");
        System.out.println(condition.query());
        System.out.println(list);
        //如果默认的不能满足要求，也可以拼写自己的查询sql
        System.out.println("=============指定sql语句==================");
        String sql = "select id,status from k_order where order_time<{orderTime}";
        condition.setOrderTime(LocalDateTime.now());
        System.out.println(condition.queryList(sql));
        System.out.println("=============更改字段的默认匹配方式==================");
        //如果觉得对order_time字段的匹配方式所有地方都应该是小于,那就在Order.class 中的此字段上设置注解属性 fieldMatchTypeFactory
        //	@TiColumn(value="order_time",comment="下单时间",fieldMatchTypeFactory = LeTypeFactory.class)
        //	private java.time.LocalDateTime orderTime;
        System.out.println(condition.query());
        //如果发现Order中的属性都不满足自己的条件，需要额外添加条件
        System.out.println("==========额外添加参数到参数空间==========");
        ArrayList<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(2);
        statusList.add(3);
        sql = "select id,status from k_order where status in [foreach@statusList#(|,|)]";
        //一种更好的写法,select id,status from k_order [foreach@statusList#where status in(|,|)]
        ISQL template = SqlFactory.sql(sql, condition);//先添加Order对象到参数空间
        template.addParam("statusList", statusList);//再添加一个List参数到参数空间
        System.out.println(condition.queryList(template));
        //如果一个要查询多个表中的字段并返回,那么只需要新建一个实体包含这些字段,字段若是基本类型,必须用包装类,继承类TiEntity.class即可
        System.out.println("==========多表查询,返回的字段来自多个表==========");
        //所有继承类TiEntity.class的实体类都必须是一个public申明的类,意味着它不能是内部类，内部静态类
        Example example = new Example();
        sql = "select a.age,a.name as user_name,b.id as order_id from k_order b,s_user a where a.id=b.user_id and order_time<{orderTime}";
        ISQL template1 = SqlFactory.sql(sql, condition);
        System.out.println(example.queryList(template1));
    }


    @Test
    void page() {
        Order condition = new Order();
        IPage<Order> page = condition.page();
        System.out.println(page);
        System.out.println("========设置分页参数=========");
        //也可以手动设置分页大小
        condition.pageSize(2);
        condition.pageIndex(1);
        System.out.println(condition.page());
        System.out.println("========设置自己的分页工厂=========");
        //如果你有自己的分页工厂,也可以指定此次使用的分页工厂
        MysqlPageFactory factory = new MysqlPageFactory();
        //也可以设置默认的分页大小
        factory.setDefaultPageSize(5);
        condition.setPageFactory(factory);
        System.out.println(condition.page());
        System.out.println("=========使用自己的分页查询SQL===========");
        //如果觉得默认的分页查询方式不满足要求,也可以手动书写查询SQL
        String sql = "select id,status from k_order where order_time<{orderTime}";
        condition.setOrderTime(LocalDateTime.now());
        System.out.println(condition.page(sql));

    }


    @Test
    void count() {
        Order condition = new Order();
        System.out.println(condition.count());
        System.out.println("========使用自己的统计方式============");
        //如果默认的统计方式不符合要求,可以使用自己的统计sql
        condition.setOrderTime(LocalDateTime.now());

        String sql = "select count(*)as total from  k_order b,s_user a where a.id=b.user_id and order_time<{orderTime}";
        System.out.println(condition.queryCount(sql, "total"));
        //也可以是这样的
        System.out.println("===========也可以是这样的===============");
        ArrayList<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(2);
        statusList.add(3);
        sql = "select count(*) as sum from k_order where status in [foreach@list#(|,|)]";
        ISQL template = SqlFactory.sql(sql, statusList);//sqlL()允许参数是一个list,会在参数空间给予默认参数名list
        //如果你还需要Order对象中的参数作为条件,可以这样写 SqlTemplate.sqlO(sql, condition).addParam("list", statusList)
        System.out.println(condition.queryCount(template, "sum"));//sum 为count(*)的别名
    }

    @Test
    void queryList() {
        Order condition = new Order();
        String sql = "select id,status from k_order where order_time<{orderTime}";
        condition.setOrderTime(LocalDateTime.now());
        List<Order> objects = condition.queryList(sql);
        System.out.println(objects);
    }

    @Test
    void queryOne() {
    }

    @Test
    void queryCount() {
    }

    @Test
    void testPage() {
    }
}