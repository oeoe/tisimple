package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.Order;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.transaction.ITxManager;
import com.gitee.oeoe.tisimple.transaction.TX;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 事务使用用例
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TxTest {
    public static final Long serialVersionUID = TiConfig.id();

    /**
     * 事务使用示例1
     */
    @Test
    boolean txTest1() {
        User user = new User();
        user.setName("cef");
        user.setAge(888);
        user.setId(111000);

        User condition = new User();
        condition.setAge(888);


        User up = new User();
        up.setName("笑笑");
        up.setAge(888);

        ITxManager txManager = TX.open();
        try (ITxManager tx = txManager) {
            tx.join(user).join(condition).join(up);//将在三个对象上的sql操作加入事务中
            int insert = user.insert();
            if (insert == 1) {
                //tx.commit();//如果需要和后面的操作互不影响,此处放开注释,提交一次到数据库,
            } else {
                return false;
            }
            int update = condition.update(up);
            if (update == 1) {
                tx.commit();//如果更新成功,则提交事务,在此处的commit将insert和update串联成一个事务,此insert和update也不一定是同一个表
                return true;
            }
            Object o = method1(condition);//此处在事务中调用另外一个方法
            return false;
        } catch (Exception e) {//如果期间出现任何异常,都回滚，也可根据业务判断指定的异常才回滚
            e.printStackTrace();
            txManager.rollback();
        }
        return false;
    }

    public Object method1(User condition) {
        Order order = new Order();
        order.setUserName("abc");
        ITxManager txManager = condition.getTxManager();//此处获取已有的事务
        //如果事务不存在,可以根据情况，是创建一个新事务,还是加入当前事务,也可以注释掉此行以非事务方式运行
        txManager = txManager == null ? txManager = TX.open() : txManager.join(order);
        try (ITxManager tx = txManager) {//如果txManager不是新事务,则不要try source,否则会导致此方法类执行结束后关闭事务,上一级方法中sql操作失败
            //...
        }
        return null;
    }
}
