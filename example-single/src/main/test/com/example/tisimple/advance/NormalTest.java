package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.page.mysql.MysqlPageFactory;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class NormalTest {
    public static final Long serialVersionUID = TiConfig.id();


    @Test
    @Order(1)
    void insert() {
        User user = new User();
        user.setName("abc");
        user.setAge(56);
        user.setAddress("china");
        int insert = user.insert();
        assert insert == 1;
    }


    @Test
    @Order(2)
    void update() {
        User condition = new User();
        condition.setName("abc");
        User user = new User();
        user.setName("cef");
        int update = condition.update(user);
        System.out.println(update);
    }

    @Test
    @Order(3)
    void query() {
        User condition = new User();
        condition.setAge(19);
        List<User> query = condition.query();
        System.out.println(query);
    }

    @Test
    @Order(4)
    void count() {
        User condition = new User();
        condition.setAge(19);
        Long count = condition.count();
        System.out.println(count);
    }

    @Test
    @Order(5)
    void delete() {
        User user = new User();
        user.setName("cef");
        int delete = user.delete();
        assert delete == 1;
    }

    @Test
    @Order(5)
    void page0() {
        User user = new User();
        user.setAge(19);
        user.pageSize(10);
        user.pageIndex(1);
        IPage<User> page = user.page();
        System.out.println(page);
    }

    @Test
    @Order(6)
    void query2() {
        MysqlPageFactory factory = new MysqlPageFactory();
        User user = new User();
        user.setName("你在哪儿");
        user.setPageFactory(factory);
        ISQL template = SqlFactory.sql("select * from s_user", user)
                .addParam("currentPage", 1)
                .addParam("pageSize", 2);
//        MysqlPage<User> page = user.page("select * from s_user where name={name}");
        IPage<User> page = user.page(template);
        System.out.println(page);
    }

    @Test
    @Order(7)
    void query3() {
        MysqlPageFactory factory = new MysqlPageFactory();
        User user = new User();
        user.setName("你在哪儿");
        user.setPageFactory(factory);
        ISQL template = SqlFactory.sql("select * from s_user", user)
                .addParam("currentPage", 1)//currentPage与pageSize是MysqlPageFactory中定义的分页参数
                .addParam("pageSize", 2);
        IPage<User> page = user.page(template);
        System.out.println(page);
    }

    @Test
    @Order(8)
    void query4() {
        User user = new User();
        user.setName("你在哪儿");
        //使用自定义分页配置
        MysqlPageFactory factory = new MysqlPageFactory();
        user.setPageFactory(factory);
        user.pageIndex(1);
        user.pageSize(5);
        String sql = "select * from s_user where name={name}";
        IPage<User> page = user.page(sql);
        System.out.println(page);
    }

    @Test
    @Order(9)
    void query5() {
        User user = new User();
        user.setName("你在哪儿");
        //创建mysql的分页工厂,可以实现接口IPageFactory 定义其他数据库的分页工厂
        MysqlPageFactory factory = new MysqlPageFactory();
        user.setPageFactory(factory);
        String sql = "select * from s_user where name={name}";
        //通过外部添加分页参数
        ISQL template = SqlFactory.sql(sql, user)
                .addParam(MysqlPageFactory.pageSizeVarName, 3)
                .addParam(MysqlPageFactory.pageIndexVarName, 1);
        IPage<User> page = user.page(template);
        System.out.println(page);
    }

    @Test
    @Order(10)
    void query6() {
        User user = new User();
        user.setName("你在哪儿");
        user.pageIndex(1);
        user.pageSize(5);
        System.out.println(user.page());

    }

}
