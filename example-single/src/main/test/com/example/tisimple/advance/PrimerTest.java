package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.page.IPage;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * 基本功能演示
 */
public class PrimerTest {
    static {
        TiConfig.id();
    }

    @Test
    void insert() {
        User user = new User();
        user.setName("测试1");
        user.setId(null);//id是int类型的自增,所以这里可以不用设置,可注释掉
        user.setAge(9);
        user.setAddress("china");
        //任何null的属性都将被忽略
        int insert = user.insert();
        System.out.println(user);
        assert insert == 1 : "insert 失败";
    }


    @Test
    void insertBatch() {
        List<User> users = new LinkedList<>();//使用linkedList
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("测试batch" + i);
            user.setId(null);//id是int类型的自增,所以这里可以不用设置,可注释掉
            user.setAge(i);
            user.setAddress("china");
            users.add(user);
        }
        User user = new User();
        int insert = user.insert(users);
        System.out.println("batch insert rows:" + insert);
        System.out.println("look for:\n" + users);//自增id将会被自动设置
    }

    @Test
    void delete() {
        User condition = new User();
        condition.setName("测试1");
        int delete = condition.delete();
        System.out.println("delete rows:" + delete);
    }

    @Test
    void update() {

        User condition = new User();
        condition.setName("测试1");

        User up = new User();
        up.setAge(99);
        up.setAddress("earth");

        System.out.println("update before:" + condition.query());
        int update = condition.update(up);
        System.out.println("update rows:" + update);
        System.out.println("update after:" + condition.query());
    }

    @Test
    void page() {
        //分页功能必须在注册分页工厂后才能使用，否则将返回null
        User condition = new User();
        //设置查询条件
        condition.setAddress("china");
        //本身有默认参数，也可设置分页参数,默认参数在注册分页工厂的时候设置
        condition.pageSize(10);//分页大小
        condition.pageIndex(1);//当前分页索引
        IPage<User> page = condition.page();
        System.out.println("page result:\n" + page);
    }

    @Test
    void query() {
        User condition = new User();
        condition.setName("测试1");
        List<User> query = condition.query();
        System.out.println("query result:\n" + query);
    }

    @Test
    void onlyOne(){
        //查询一条记录，如果返回多条，将抛出异常
        //一般用于主键查询
        User condition = new User();
        condition.setName("测试1");
        User one = condition.onlyOne();
        System.out.println(one);
    }

    @Test
    void count() {
        User condition = new User();
        condition.setAddress("china");
        Long count = condition.count();
        System.out.println("count result:\n" + count);
    }
}
