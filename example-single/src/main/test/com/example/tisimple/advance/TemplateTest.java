package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 模版语法
 */
public class TemplateTest {
    public static final Long serialVersionUID = TiConfig.id();

    @Test
    public void first() {
        //select * from user where age=? and name=?
        String sql = "select * from user where age=? and name=?";
        ISQL template = SqlFactory.sql(sql, 18, "小米");
        System.out.println(template.getParsedSql());
    }

    @Test
    public void second() {
        //select * from user where age=? and name=?
        String sql = "select * from user where age={2} and name={1}";//2,指定第二个参数,18;1,指定第一个参数
        ISQL template = SqlFactory.sql(sql, "小米", 18);
        System.out.println(template.getParsedSql());
    }

    @Test
    public void third() {
        User user = new User();
        user.setName("小米");
        user.setAge(22);

        HashMap<String, Object> p = new HashMap<>();
        p.put("name", "小米");
        p.put("age", 12);

        //select * from user where age=? and name=?
        String sql = "select * from user where age={age} and name={name}";
        ISQL templateM = SqlFactory.sql(sql, p);
        System.out.println(templateM.getParsedSql());
        ISQL templateO = SqlFactory.sql(sql, user);
        System.out.println(templateO.getParsedSql());
    }

    @Test
    public void third2() {
        String address = "('大海','天空')";

        User user = new User();
        user.setName("小米");
        user.setAge(22);
        user.setAddress(address);

        HashMap<String, Object> p = new HashMap<>();
        p.put("name", "小米");
        p.put("age", 12);
        p.put("address", address);

        //select * from user where address in  ('大海','天空');
        String sql = "select * from user where address in {#address}";

        ISQL templateM = SqlFactory.sql(sql, p);
        System.out.println(templateM.getParsedSql());
        ISQL templateO = SqlFactory.sql(sql, user);
        System.out.println(templateO.getParsedSql());
    }

    @Test
    public void four() {
        ArrayList<String> p = new ArrayList<>();
        p.add("大海");
        p.add("天空");
        //select * from user where address in  ('大海','天空');
        String sql = "select * from user where address in [foreach@list#(|,|)]";
        ISQL template = SqlFactory.sql(sql, p);
        System.out.println(template.getParsedSql());

    }

    @Test
    public void five() {
        ArrayList<String> p = new ArrayList<>();
//        p.add("大海");
//        p.add("天空");
        //select * from user where address in  ('大海','天空');
        String sql = " select * from user [foreach@list#where address in(|,|)]";

        ISQL template = SqlFactory.sql(sql, p);
        System.out.println(template.getParsedSql());
    }

}
