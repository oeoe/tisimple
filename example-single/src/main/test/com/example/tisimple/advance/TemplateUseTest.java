package com.example.tisimple.advance;

import com.example.tisimple.domain.User;
import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.exception.SqlInjectionException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 模版用法
 */
public class TemplateUseTest {

    /**
     * 无参sql
     */
    @Test
    public void zero() {
        String sql = "select * from s_user where id=1 and age=18 and address='china'";
        ISQL template = SqlFactory.sql(sql);
        //解析后的sql与原sql没有变化
        System.out.println("parsed SQL:" + template.getParsedSql());
    }


    /**
     * ?占位符
     */
    @Test
    public void one() {
        String sql = "select * from s_user where id=? and age=? and address=?";
        ISQL template = SqlFactory.sql(sql, 1, 18, "china");
        //解析后的sql与原sql没有变化
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from s_user where id='1' and age='18' and address='china'
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * {1}序列编号
     */
    @Test
    public void two() {
        String sql = "select * from user where id={2} and age={3} and address={1} ";
        //{2}对应的是第二个参数位置上的参数,花括号中的数字代表者参数位置，从1开始。
        ISQL template = SqlFactory.sql(sql, "china", 1, 18);
        //解析后的sql将变成？占位符形式的sql,select * from s_user where id=? and age=? and address=?
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from s_user where id='1' and age='18' and address='china'
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * {name}命名参数-map对象
     */
    @Test
    public void three() {
        String sql = "select * from user where id={id} and age={age} and address={address} ";
        HashMap<String, Object> param = new HashMap<>();
        param.put("id", 1);
        param.put("age", 18);
        param.put("address", "china");
        //花括号中的名称即参数名称，是map对象中key
        ISQL template = SqlFactory.sql(sql, param);
        //如果觉得创建map太麻烦，可以直接向ISQL中添加参数,如下：
        template.addParam("id", 1)
                .addParam("age", 18)
                .addParam("address", "china");
        //解析后的sql将变成？占位符形式的sql,select * from s_user where id=? and age=? and address=?
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from s_user where id='1' and age='18' and address='china'
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * {name}命名参数-实体对象
     */
    @Test
    public void four() {
        String sql = "select * from user where id={id} and age={age} and address={address} ";
        User user = new User();
        user.setId(1);
        user.setAge(18);
        user.setAddress("china");
        //花括号中的命名参数名称必须要与实体中的字段一致,默认所有的命名参数的值为字符串null
        ISQL template = SqlFactory.sql(sql, user);
        //解析后的sql将变成？占位符形式的sql,select * from s_user where id=? and age=? and address=?
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from s_user where id='1' and age='18' and address='china'
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * {name}命名参数-list对象
     */
    @Test
    public void five() {
        //[foreach@list#where id in(|,|)] 是foreach模版函数的用法,一般用于遍历集合,生成in 类型的sql
        String sql = "select * from user [foreach@list#where id in(|,|)]";
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        //参数是collection类型,将会默认给予一个叫list的参数
        ISQL template = SqlFactory.sql(sql, ids);
        //它等同于下面的方式
        template.addParam("list", ids);
        //解析后的sql将变成？占位符形式的sql,select * from user where id in(?,?,?)
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from user where id in('1','2','3')
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * {#name}特殊命名参数-命名参数以#开头,进行直接替换,不会生成预编译语句
     */
    @Test
    public void six() throws SqlInjectionException {
        String sql = "select * from user where id={#id} and age={#age} and address={#address} ";
        User user = new User();
        user.setId(1);
        user.setAge(18);
        user.setAddress("'china'");
        //如果花括号中的命名参数名称前以#开头,将会进行直接替换,不会生成预编译语句
        ISQL template = SqlFactory.sql(sql, user);
        //这种方式存在sql注入风险,可以调用下面的方法进行检查
        TiSimpleCore.checkSqlInjection(user.getAddress());
        //解析后的sql将变成,select * from user where id=1 and age=18 and address=china
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:select * from user where id=1 and age=18 and address=china
        System.out.println("refer SQL:" + template.getReferSql());
    }

    /**
     * 自定义参数
     */
    @Test
    public void seven() {
        String sql = "select * from user where id={id}";
        ISQL template = SqlFactory.sql(sql);
        //参数可以通过如下进行添加
        template.addParam("id", 1);
        //也可以获取参数进行查看
        Object id = template.getParam("id");
        System.out.println(id);
        //也可以复制一个模版,这个模版和原模版是两个对象
        ISQL copy = template.copy();
        System.out.println(copy);
        //也可以保留原模版的参数空间,更改原模版的sql
        String newSql = "select * from user where id>{id}";
        ISQL copy1 = template.copy(newSql);
        System.out.println(copy1);
    }

    /**
     * 限制的存储过程
     */
    @Test
    public void eight() {
        ISQL template = SqlFactory.sql("{call getOrderById3(?,?)}", 1, "爱德华");
        //解析后的sql不变
        System.out.println("parsed SQL:" + template.getParsedSql());
        //组合参数后的参考sql:{call getOrderById3('1','爱德华')}
        System.out.println("refer SQL:" + template.getReferSql());
    }

}
