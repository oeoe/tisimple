package com.example.tisimple.advance;

import com.example.tisimple.TiConfig;
import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.exception.UnPackResultSetException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * 存储过程调用示例
 *
 * @see ./alis_p_proc.sql
 */

public class ProcTest {
    public static final Long serialVersionUID = TiConfig.id();

    @Test
    public void test2() throws SQLException, UnPackResultSetException {
        ISQL sql = SqlFactory.sql("{call getOrderById2(?)}", 1);
        TiSimpleCore.CallResult maps = TiSimpleCore.execProc(sql);
        System.out.println(maps);
        System.out.println(maps.getLastQueryData());
        System.out.println(maps.getLastUpdateCount());
    }

    @Test
    public void test3() throws SQLException, UnPackResultSetException {
        ISQL sql = SqlFactory.sql("{call getOrderById3(?,?)}", 1, "爱德华");
        TiSimpleCore.CallResult maps = TiSimpleCore.execProc(sql);
        System.out.println(maps);
        System.out.println(maps.getLastQueryData());//获取存储过程最后一次select语句的返回结果集
        System.out.println(maps.getLastUpdateCount());//获取存储过程最后一次update或delete的影响的记录数量
        //如果定义了结果集对应的对象，可以使用TiSimpleCore.toEntityList()进行转换
    }
}
