package com.gitee.oeoe.tisimple.page.mysql;

import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.page.IPageFactory;
import com.gitee.oeoe.tisimple.template.ISQL;

import java.util.List;

/**
 * mysql 数据库分页工厂
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/12
 */
public class MysqlPageFactory implements IPageFactory {
    private static final String rowSelectName = "total_rows";
    public static final String pageSizeVarName = "pageSize";
    public static final String pageIndexVarName = "pageIndex";
    private int defaultPageSize = 10;
    private Integer pageSize;
    private Integer pageIndex;

    @Override
    public ISQL countSql(ISQL template) {
        String base = template.getOriginSql();
        String builder = "select count(*) as " + rowSelectName + " from ( " + base + " ) as ti_page_count_table";
        return template.copy(builder);
    }

    @Override
    public String countSqlSelectName() {
        return rowSelectName;
    }

    @Override
    public ISQL pageSql(ISQL template) {
        int pageSizeNumber = this.getPageSize(template);
        int currentPageNumber = this.getPageIndex(template);

        int offset = (currentPageNumber - 1) * pageSizeNumber;

        String base = template.getOriginSql();
        String builder = "select * from ( " + base + " ) as ti_page_table limit " + offset + "," + pageSizeNumber;
        return template.copy(builder);
    }

    @Override
    public <T> IPage<T> assemble(List<T> data, Long maxRows, int pageSize, int pageIndex) {
        return new MysqlPage<>(maxRows, pageSize, pageIndex, data);
    }

    @Override
    public int getPageSize(ISQL template) {
        Integer val = getIntegerVal(template, pageSizeVarName);
        return null == val ? defaultPageSize : val;
    }

    @Override
    public int getPageIndex(ISQL template) {
        Integer val = getIntegerVal(template, pageIndexVarName);
        return null == val ? 1 : val;
    }

    @Override
    public void setDefaultPageSize(int size) {
        this.defaultPageSize = size;
    }

    @Override
    public int getDefaultPageSize() {
        return this.defaultPageSize;
    }

    private Integer getIntegerVal(ISQL template, String key) {
        Integer pm = null;
        Object val = template.getParam(key);
        if (val instanceof String) {
            pm = Integer.parseInt((String) val);
        }
        if (val instanceof Number) {
            pm = ((Number) val).intValue();
        }
        return pm;
    }

    @Override
    public Integer pageSize() {
        return this.pageSize;
    }

    @Override
    public Integer pageIndex() {
        return this.pageIndex;
    }

    @Override
    public void pageSize(Integer size) {
        this.pageSize = size;
    }

    @Override
    public void pageIndex(Integer page) {
        this.pageIndex = page;

    }
}
