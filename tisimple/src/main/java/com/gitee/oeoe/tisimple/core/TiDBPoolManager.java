package com.gitee.oeoe.tisimple.core;


import com.gitee.oeoe.tisimple.exception.ConditionNotMetException;
import com.gitee.oeoe.tisimple.exception.ObtainConnectionException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 管理数据库连接池
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class TiDBPoolManager {
    private static final List<DataSource> dataSources = new ArrayList<>(8);
    private static Random rd = new Random();

    /**
     * 注册一个连接池
     *
     * @param dataSource 连接池
     */
    public static void register(DataSource dataSource) {
        TiDBPoolManager.dataSources.add(dataSource);
        TiDBPoolManager.rd = new Random(dataSources.size());
    }

    /**
     * 选择一个连接池，从中获取一个连接，必将保证获取一个可用的连接，否则异常
     *
     * @return 连接
     * @throws ConditionNotMetException  获取条件不满足
     * @throws ObtainConnectionException 获取连接失败
     */
    public static Connection getConnection() throws ConditionNotMetException, ObtainConnectionException {

        DataSource dataSource = findDataSource();
        if (null == dataSource) {
            throw new ConditionNotMetException("获取数据连接池失败,数据库连接池空,请注册!");
        }
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new ObtainConnectionException("获取连接失败", e);
        }
    }

    /**
     * 随机获取一个连接池
     *
     * @return 连接池
     */
    private static DataSource findDataSource() {
        if (dataSources.isEmpty()) {
            return null;
        }
        return dataSources.get(rd.nextInt(dataSources.size()));
    }

}
