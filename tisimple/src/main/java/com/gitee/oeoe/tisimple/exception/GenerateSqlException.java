package com.gitee.oeoe.tisimple.exception;

/**
 * 生成sql异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class GenerateSqlException extends RuntimeException {
    public GenerateSqlException(String message) {
        super(message);
    }

    public GenerateSqlException(String message, Throwable cause) {
        super(message, cause);
    }
}
