package com.gitee.oeoe.tisimple.extra.keygenertefactory;

import com.gitee.oeoe.tisimple.extra.IKeyGenerateFactory;

/**
 * 不做任何处理
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class NoneFactory implements IKeyGenerateFactory<Void> {
    @Override
    public Void key() {
        return null;
    }
}
