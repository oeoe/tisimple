package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.entityfactory.ITableInfoFactory;
import com.gitee.oeoe.tisimple.entityfactory.TableColumnInfo;
import com.gitee.oeoe.tisimple.entityfactory.TableInfo;
import com.gitee.oeoe.tisimple.entityfactory.mysql.MysqlSchemaTable;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;

import java.util.HashMap;
import java.util.List;

/**
 * oracle 数据库表信息工厂
 *
 * @author zhu qiang
 * @since 1.8+ 2022/2/6
 */
public class OracleTableInfoFactory implements ITableInfoFactory {

    @Override
    public TableInfo info(String tableName) {
        tableName=tableName.toUpperCase();
        TableInfo tableInfo = new TableInfo();
        tableInfo.setTableName(tableName);

        UserTableComment tabCondition = new UserTableComment();
        tabCondition.setTableName(tableName);
        UserTableComment userTableComments = tabCondition.onlyOne();
        tableInfo.setTableComment(null== userTableComments ?null: userTableComments.getComments());


        UserTableColumnComment colCondition = new UserTableColumnComment();
        colCondition.setTableName(tableName);
        List<UserTableColumnComment> colComments = colCondition.query();
        if (colComments.isEmpty()) {
            return tableInfo;
        }
        String sql = "select a.constraint_name, a.column_name\n" +
                "from user_cons_columns a,\n" +
                "     user_constraints b\n" +
                "where a.constraint_name = b.constraint_name\n" +
                "  and b.constraint_type = 'P'\n" +
                "  and a.table_name = ?";
        ISQL sqlTemplate = SqlFactory.sql(sql, tableName);
        ConstraintPrimaryKey client = new ConstraintPrimaryKey();
        List<ConstraintPrimaryKey> constraintPrimaryKeys = client.queryList(sqlTemplate);

        UserTableColumn columnsCondition = new UserTableColumn();
        columnsCondition.setTableName(tableName);
        List<UserTableColumn> tabColumns = columnsCondition.query();

        HashMap<String, TableColumnInfo> columnInfos = new HashMap<>();
        for (UserTableColumn x : tabColumns) {
            TableColumnInfo columnInfo = new TableColumnInfo();
            columnInfo.setColumnName(x.getColumnName());
            columnInfo.setDbType(x.getDataType());
            UserTableColumnComment colComment = colComments.stream().filter(y -> y.getColumnName().equals(x.getColumnName())).findFirst().orElse(null);
            String comments = null != colComment ? colComment.getComments() : null;
            columnInfo.setColumnComment(null != comments ? comments.trim() : null);
            boolean key = constraintPrimaryKeys.stream().anyMatch(y -> y.getColumnName().equals(x.getColumnName()));
            columnInfo.setKey(key);
            columnInfo.setNullAble("Y".equals(x.getNullable()));
            columnInfo.setAutoIncrement(false);
            columnInfos.put(x.getColumnName(), columnInfo);
        }
        tableInfo.setColumns(columnInfos);
        return tableInfo;
    }

    @Override
    public boolean exist(String tableName) {
        MysqlSchemaTable tableCondition = new MysqlSchemaTable();
        tableCondition.setTableName(tableName);
        Long count = tableCondition.count();
        return count == 1;
    }
}
