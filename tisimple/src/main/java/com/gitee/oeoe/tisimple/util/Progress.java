package com.gitee.oeoe.tisimple.util;

/**
 * 进度条
 *
 * @author zhu qiang
 * @since 1.8+ 2022/01/29
 */
public interface Progress {

    /**
     * 设置最大完成值
     *
     * @param max 完成值上限
     */
    void open(double max);

    /**
     * 完成后
     */
    void complete();

    /**
     * 一次完成或变化
     */
    void change();

    /**
     * 一次完成或变化
     *
     * @param degree 本次完成度
     */
    void change(double degree);

    /**
     * 一次完成或变化
     *
     * @param prefix 前缀显示
     * @param degree 本次完成度
     */
    void change(String prefix, double degree);
}
