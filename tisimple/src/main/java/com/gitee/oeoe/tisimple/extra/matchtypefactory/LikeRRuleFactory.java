package com.gitee.oeoe.tisimple.extra.matchtypefactory;

import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.exception.GenerateSqlException;
import com.gitee.oeoe.tisimple.exception.SqlInjectionException;
import com.gitee.oeoe.tisimple.extra.IFieldMatchRuleFactory;

/**
 * like 匹配规则，只对字符串有效，右前缀匹配，like xxx%
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class LikeRRuleFactory implements IFieldMatchRuleFactory {
    @Override
    public Object sql(StringBuilder sqlBuilder, String columnName, Object val) {
        if (val instanceof String) {
            String v = (String) val;
            try {
                TiSimpleCore.checkSqlInjection(v);
            } catch (SqlInjectionException e) {
                throw new GenerateSqlException(e.getMessage(), e);
            }
            sqlBuilder.append(columnName).append("like '").append(v).append("%'");
            return null;
        }
        //直接返回，sqlBuilder长度0
        return val;
    }
}
