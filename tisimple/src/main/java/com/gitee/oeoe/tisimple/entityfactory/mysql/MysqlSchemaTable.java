package com.gitee.oeoe.tisimple.entityfactory.mysql;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable("information_schema.tables")
public class MysqlSchemaTable extends TiEntity<MysqlSchemaTable> {
	@TiColumn("TABLE_CATALOG")
	private String tableCatalog;
	@TiColumn("TABLE_SCHEMA")
	private String tableSchema;
	@TiColumn("TABLE_NAME")
	private String tableName;
	@TiColumn("TABLE_TYPE")
	private String tableType;
	@TiColumn("ENGINE")
	private String engine;
	@TiColumn("VERSION")
	private Long version;
	@TiColumn("ROW_FORMAT")
	private String rowFormat;
	@TiColumn("TABLE_ROWS")
	private java.math.BigInteger tableRows;
	@TiColumn("AVG_ROW_LENGTH")
	private java.math.BigInteger avgRowLength;
	@TiColumn("DATA_LENGTH")
	private java.math.BigInteger dataLength;
	@TiColumn("MAX_DATA_LENGTH")
	private java.math.BigInteger maxDataLength;
	@TiColumn("INDEX_LENGTH")
	private java.math.BigInteger indexLength;
	@TiColumn("DATA_FREE")
	private java.math.BigInteger dataFree;
	@TiColumn("AUTO_INCREMENT")
	private java.math.BigInteger autoIncrement;
	@TiColumn("CREATE_TIME")
	private java.sql.Timestamp createTime;
	@TiColumn("UPDATE_TIME")
	private java.time.LocalDateTime updateTime;
	@TiColumn("CHECK_TIME")
	private String checkTime;
	@TiColumn("TABLE_COLLATION")
	private String tableCollation;
	@TiColumn("CHECKSUM")
	private String checksum;
	@TiColumn("CREATE_OPTIONS")
	private String createOptions;
	@TiColumn("TABLE_COMMENT")
	private String tableComment;

	public void setTableCatalog(String tableCatalog){this.tableCatalog=tableCatalog;}

	public String getTableCatalog(){return this.tableCatalog;}

	public void setTableSchema(String tableSchema){this.tableSchema=tableSchema;}

	public String getTableSchema(){return this.tableSchema;}

	public void setTableName(String tableName){this.tableName=tableName;}

	public String getTableName(){return this.tableName;}

	public void setTableType(String tableType){this.tableType=tableType;}

	public String getTableType(){return this.tableType;}

	public void setEngine(String engine){this.engine=engine;}

	public String getEngine(){return this.engine;}

	public void setVersion(Long version){this.version=version;}

	public Long getVersion(){return this.version;}

	public void setRowFormat(String rowFormat){this.rowFormat=rowFormat;}

	public String getRowFormat(){return this.rowFormat;}

	public void setTableRows(java.math.BigInteger tableRows){this.tableRows=tableRows;}

	public java.math.BigInteger getTableRows(){return this.tableRows;}

	public void setAvgRowLength(java.math.BigInteger avgRowLength){this.avgRowLength=avgRowLength;}

	public java.math.BigInteger getAvgRowLength(){return this.avgRowLength;}

	public void setDataLength(java.math.BigInteger dataLength){this.dataLength=dataLength;}

	public java.math.BigInteger getDataLength(){return this.dataLength;}

	public void setMaxDataLength(java.math.BigInteger maxDataLength){this.maxDataLength=maxDataLength;}

	public java.math.BigInteger getMaxDataLength(){return this.maxDataLength;}

	public void setIndexLength(java.math.BigInteger indexLength){this.indexLength=indexLength;}

	public java.math.BigInteger getIndexLength(){return this.indexLength;}

	public void setDataFree(java.math.BigInteger dataFree){this.dataFree=dataFree;}

	public java.math.BigInteger getDataFree(){return this.dataFree;}

	public void setAutoIncrement(java.math.BigInteger autoIncrement){this.autoIncrement=autoIncrement;}

	public java.math.BigInteger getAutoIncrement(){return this.autoIncrement;}

	public void setCreateTime(java.sql.Timestamp createTime){this.createTime=createTime;}

	public java.sql.Timestamp getCreateTime(){return this.createTime;}

	public void setUpdateTime(java.time.LocalDateTime updateTime){this.updateTime=updateTime;}

	public java.time.LocalDateTime getUpdateTime(){return this.updateTime;}

	public void setCheckTime(String checkTime){this.checkTime=checkTime;}

	public String getCheckTime(){return this.checkTime;}

	public void setTableCollation(String tableCollation){this.tableCollation=tableCollation;}

	public String getTableCollation(){return this.tableCollation;}

	public void setChecksum(String checksum){this.checksum=checksum;}

	public String getChecksum(){return this.checksum;}

	public void setCreateOptions(String createOptions){this.createOptions=createOptions;}

	public String getCreateOptions(){return this.createOptions;}

	public void setTableComment(String tableComment){this.tableComment=tableComment;}

	public String getTableComment(){return this.tableComment;}


	@Override
	public String toString() {
		return "MysqlSchemaTable{"+
			"tableCatalog="+tableCatalog+","+
			"tableSchema="+tableSchema+","+
			"tableName="+tableName+","+
			"tableType="+tableType+","+
			"engine="+engine+","+
			"version="+version+","+
			"rowFormat="+rowFormat+","+
			"tableRows="+tableRows+","+
			"avgRowLength="+avgRowLength+","+
			"dataLength="+dataLength+","+
			"maxDataLength="+maxDataLength+","+
			"indexLength="+indexLength+","+
			"dataFree="+dataFree+","+
			"autoIncrement="+autoIncrement+","+
			"createTime="+createTime+","+
			"updateTime="+updateTime+","+
			"checkTime="+checkTime+","+
			"tableCollation="+tableCollation+","+
			"checksum="+checksum+","+
			"createOptions="+createOptions+","+
			"tableComment="+tableComment+
		"}";
	}
}