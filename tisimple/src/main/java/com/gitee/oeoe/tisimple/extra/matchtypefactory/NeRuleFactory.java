package com.gitee.oeoe.tisimple.extra.matchtypefactory;

import com.gitee.oeoe.tisimple.extra.IFieldMatchRuleFactory;

/**
 * 不等于匹配规则 !=
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class NeRuleFactory implements IFieldMatchRuleFactory {
    @Override
    public Object sql(StringBuilder sqlBuilder, String columnName, Object val) {
        sqlBuilder.append(columnName).append("!=");
        return val;
    }
}
