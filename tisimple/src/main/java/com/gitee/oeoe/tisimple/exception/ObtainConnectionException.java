package com.gitee.oeoe.tisimple.exception;

/**
 * 获取连接异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class ObtainConnectionException extends RuntimeException {
    public ObtainConnectionException(String message) {
        super(message);
    }

    public ObtainConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
