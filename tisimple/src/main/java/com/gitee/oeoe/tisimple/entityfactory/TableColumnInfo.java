package com.gitee.oeoe.tisimple.entityfactory;

/**
 * 表字段信息
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/29
 */
public class TableColumnInfo {
    private String columnName;
    private String columnComment;
    private String dbType;
    private boolean isKey;
    private boolean nullAble;
    private boolean autoIncrement;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public boolean isKey() {
        return isKey;
    }

    public void setKey(boolean key) {
        isKey = key;
    }

    public boolean isNullAble() {
        return nullAble;
    }

    public void setNullAble(boolean nullAble) {
        this.nullAble = nullAble;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    @Override
    public String toString() {
        return "TableColumnInfo{" +
                "columnName='" + columnName + '\'' +
                ", columnComment='" + columnComment + '\'' +
                ", dbType='" + dbType + '\'' +
                ", isKey=" + isKey +
                ", nullAble=" + nullAble +
                ", autoIncrement=" + autoIncrement +
                '}';
    }
}
