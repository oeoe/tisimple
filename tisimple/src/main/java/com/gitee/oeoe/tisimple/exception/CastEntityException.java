package com.gitee.oeoe.tisimple.exception;

/**
 * ResultSet转换为entity异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class CastEntityException extends Exception {
    public CastEntityException(String message) {
        super(message);
    }

    public CastEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
