package com.gitee.oeoe.tisimple.exception;

/**
 * 事务异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/14
 */
public class TxException extends RuntimeException {
    public TxException(String message) {
        super(message);
    }

    public TxException(String message, Throwable cause) {
        super(message, cause);
    }
}
