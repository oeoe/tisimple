package com.gitee.oeoe.tisimple.transaction;

import com.gitee.oeoe.tisimple.ability.TiEntity;

import java.sql.Connection;

/**
 * 事务处理，主要处理事务提交，回滚，加入，连接释放
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/15
 */
public interface ITxManager extends AutoCloseable {


    /**
     * 得到代理连接，此连接无法执行close
     *
     * @return Connection
     */
    Connection getConnection();

    /**
     * 得到真实连接
     *
     * @return Connection
     */
    Connection getRealConnection();

    /**
     * 开启事务，初始化属性
     */
    void open();

    /**
     * 加入此事务
     *
     * @param entity entity
     * @return 当前事务管理器
     */
    ITxManager join(TiEntity<?> entity);

    /**
     * 事务是否有效
     * @return 是否有效，true-有效，否则false
     */
    boolean isValid();

    /**
     * 事务提交
     */
    void commit();

    /**
     * 事务回滚
     */
    void rollback();

    /**
     * 关闭资源
     */
    void close();
}
