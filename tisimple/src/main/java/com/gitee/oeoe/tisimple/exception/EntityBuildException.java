package com.gitee.oeoe.tisimple.exception;

/**
 * 实体类生成异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/5
 */
public class EntityBuildException extends RuntimeException {
    public EntityBuildException(String message) {
        super(message);
    }

    public EntityBuildException(String message, Throwable cause) {
        super(message, cause);
    }
}
