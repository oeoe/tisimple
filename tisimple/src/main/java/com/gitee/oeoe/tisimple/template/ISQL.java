package com.gitee.oeoe.tisimple.template;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * sql模板
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/19
 */
public interface ISQL {


    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @return 模版SQL
     */
    ISQL SqlNew(String originSql);

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param param     模版参数
     * @return 模版SQL
     */
    ISQL SqlNew(String originSql, Object param);

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param params    模版参数
     * @return 模版SQL
     */
    ISQL SqlNew(String originSql, Object... params);


    /**
     * 自定义添加参数到参数空间
     *
     * @param paramName  参数名
     * @param paramValue 参数值
     * @return 模版SQL
     */
    ISQL addParam(String paramName, Object paramValue);


    /**
     * 自定义添加参数到参数空间
     *
     * @param params 参数键值对
     * @return 模版SQL
     */
    ISQL addParams(Map<?, ?> params);

    /**
     * 获取参数空间的参数
     *
     * @param paramName 参数名
     * @return 参数值
     */
    Object getParam(String paramName);

    /**
     * 获取原生的sql
     *
     * @return 未解析的sql
     */
    String getOriginSql();

    /**
     * 获取解析后的sql
     *
     * @return 解析后的sql
     */
    String getParsedSql();

    /**
     * 获取将参数与sql组合后的参考sql
     *
     * @return 组合参数后的参考sql
     */
    String getReferSql();

    /**
     * 复制自身
     *
     * @return 新的模版SQL
     */
    ISQL copy();

    /**
     * 复制自身，替换未解析sql
     *
     * @param originSql 模版sql
     * @return 新的模版SQL
     */
    ISQL copy(String originSql);


    /**
     * 解析sql
     *
     * @return 解析后的sql
     */
    String parseSql();

    /**
     * 数据库是否支持自动生成主键
     *
     * @return 数据库是否支持自动生成主键
     */
    boolean supportGeneratorKey();

    /**
     * 数据库是否支持自动生成主键
     *
     * @return 模版SQL
     */
    ISQL supportGeneratorKey(boolean supports);

    /**
     * 组装参数
     *
     * @param statement 预编译语句
     * @return 模版SQL
     * @throws SQLException 设值异常
     */
    ISQL assemble(PreparedStatement statement) throws SQLException;

    /**
     * 注册模版函数
     *
     * @param funName 函数名
     * @param func    函数
     * @param <T>     函数类型
     */
    <T extends SqlFunc> void registerFunc(String funName, Class<T> func);
}
