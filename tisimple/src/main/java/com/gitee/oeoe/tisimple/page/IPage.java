package com.gitee.oeoe.tisimple.page;

import java.util.List;

/**
 * 分页结果
 *
 * @param <T> 实体类型
 * @author zhu qiang
 * @since 1.8+ 2022/1/11
 */
public interface IPage<T> {

    /**
     * 获取记录总数
     *
     * @return 记录总数
     */
    Long getTotal();

    /**
     * 获取分页大小
     *
     * @return 分页大小
     */
    Integer getPageSize();

    /**
     * 获取当前页编号
     *
     * @return 当前页编号
     */
    Integer getPageIndex();

    /**
     * 获取记录结果
     *
     * @return 记录结果
     */
    List<T> getData();
}
