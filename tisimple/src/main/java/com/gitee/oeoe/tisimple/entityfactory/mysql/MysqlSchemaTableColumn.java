package com.gitee.oeoe.tisimple.entityfactory.mysql;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable("information_schema.columns")
public class MysqlSchemaTableColumn extends TiEntity<MysqlSchemaTableColumn> {
	@TiColumn("TABLE_CATALOG")
	private String tableCatalog;
	@TiColumn("TABLE_SCHEMA")
	private String tableSchema;
	@TiColumn("TABLE_NAME")
	private String tableName;
	@TiColumn("COLUMN_NAME")
	private String columnName;
	@TiColumn("ORDINAL_POSITION")
	private Long ordinalPosition;
	@TiColumn("COLUMN_DEFAULT")
	private String columnDefault;
	@TiColumn("IS_NULLABLE")
	private String isNullable;
	@TiColumn("DATA_TYPE")
	private String dataType;
	@TiColumn("CHARACTER_MAXIMUM_LENGTH")
	private Long characterMaximumLength;
	@TiColumn("CHARACTER_OCTET_LENGTH")
	private Long characterOctetLength;
	@TiColumn("NUMERIC_PRECISION")
	private java.lang.Long numericPrecision;
	@TiColumn("NUMERIC_SCALE")
	private java.lang.Long numericScale;
	@TiColumn("DATETIME_PRECISION")
	private java.lang.Long datetimePrecision;
	@TiColumn("CHARACTER_SET_NAME")
	private String characterSetName;
	@TiColumn("COLLATION_NAME")
	private String collationName;
	@TiColumn("COLUMN_TYPE")
	private String columnType;
	@TiColumn("COLUMN_KEY")
	private String columnKey;
	@TiColumn("EXTRA")
	private String extra;
	@TiColumn("PRIVILEGES")
	private String privileges;
	@TiColumn("COLUMN_COMMENT")
	private String columnComment;
	@TiColumn("GENERATION_EXPRESSION")
	private String generationExpression;
	@TiColumn("SRS_ID")
	private String srsId;

	public void setTableCatalog(String tableCatalog){this.tableCatalog=tableCatalog;}

	public String getTableCatalog(){return this.tableCatalog;}

	public void setTableSchema(String tableSchema){this.tableSchema=tableSchema;}

	public String getTableSchema(){return this.tableSchema;}

	public void setTableName(String tableName){this.tableName=tableName;}

	public String getTableName(){return this.tableName;}

	public void setColumnName(String columnName){this.columnName=columnName;}

	public String getColumnName(){return this.columnName;}

	public void setOrdinalPosition(Long ordinalPosition){this.ordinalPosition=ordinalPosition;}

	public Long getOrdinalPosition(){return this.ordinalPosition;}

	public void setColumnDefault(String columnDefault){this.columnDefault=columnDefault;}

	public String getColumnDefault(){return this.columnDefault;}

	public void setIsNullable(String isNullable){this.isNullable=isNullable;}

	public String getIsNullable(){return this.isNullable;}

	public void setDataType(String dataType){this.dataType=dataType;}

	public String getDataType(){return this.dataType;}

	public void setCharacterMaximumLength(Long characterMaximumLength){this.characterMaximumLength=characterMaximumLength;}

	public Long getCharacterMaximumLength(){return this.characterMaximumLength;}

	public void setCharacterOctetLength(Long characterOctetLength){this.characterOctetLength=characterOctetLength;}

	public Long getCharacterOctetLength(){return this.characterOctetLength;}

	public void setNumericPrecision(java.lang.Long numericPrecision){this.numericPrecision=numericPrecision;}

	public java.lang.Long getNumericPrecision(){return this.numericPrecision;}

	public void setNumericScale(java.lang.Long numericScale){this.numericScale=numericScale;}

	public java.lang.Long getNumericScale(){return this.numericScale;}

	public void setDatetimePrecision(java.lang.Long datetimePrecision){this.datetimePrecision=datetimePrecision;}

	public java.lang.Long getDatetimePrecision(){return this.datetimePrecision;}

	public void setCharacterSetName(String characterSetName){this.characterSetName=characterSetName;}

	public String getCharacterSetName(){return this.characterSetName;}

	public void setCollationName(String collationName){this.collationName=collationName;}

	public String getCollationName(){return this.collationName;}

	public void setColumnType(String columnType){this.columnType=columnType;}

	public String getColumnType(){return this.columnType;}

	public void setColumnKey(String columnKey){this.columnKey=columnKey;}

	public String getColumnKey(){return this.columnKey;}

	public void setExtra(String extra){this.extra=extra;}

	public String getExtra(){return this.extra;}

	public void setPrivileges(String privileges){this.privileges=privileges;}

	public String getPrivileges(){return this.privileges;}

	public void setColumnComment(String columnComment){this.columnComment=columnComment;}

	public String getColumnComment(){return this.columnComment;}

	public void setGenerationExpression(String generationExpression){this.generationExpression=generationExpression;}

	public String getGenerationExpression(){return this.generationExpression;}

	public void setSrsId(String srsId){this.srsId=srsId;}

	public String getSrsId(){return this.srsId;}


	@Override
	public String toString() {
		return "MysqlSchemaTableColumn{"+
			"tableCatalog="+tableCatalog+","+
			"tableSchema="+tableSchema+","+
			"tableName="+tableName+","+
			"columnName="+columnName+","+
			"ordinalPosition="+ordinalPosition+","+
			"columnDefault="+columnDefault+","+
			"isNullable="+isNullable+","+
			"dataType="+dataType+","+
			"characterMaximumLength="+characterMaximumLength+","+
			"characterOctetLength="+characterOctetLength+","+
			"numericPrecision="+numericPrecision+","+
			"numericScale="+numericScale+","+
			"datetimePrecision="+datetimePrecision+","+
			"characterSetName="+characterSetName+","+
			"collationName="+collationName+","+
			"columnType="+columnType+","+
			"columnKey="+columnKey+","+
			"extra="+extra+","+
			"privileges="+privileges+","+
			"columnComment="+columnComment+","+
			"generationExpression="+generationExpression+","+
			"srsId="+srsId+
		"}";
	}
}