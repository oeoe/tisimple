package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.exception.DeleteException;
import com.gitee.oeoe.tisimple.exception.InsertException;
import com.gitee.oeoe.tisimple.exception.QueryException;
import com.gitee.oeoe.tisimple.exception.UpdateException;
import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.template.ISQL;

import java.util.List;

/**
 * 提供基础sql操作接口，赋予实体增删改查的基本能力，* 此接口中的方法使用前提条件是，实体上必须申明@TiTable，且必须指定表名，否则将不会有任何效果
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/1
 */

public interface IBaseAbility<T> {

    /**
     * 插入一条数据，如果提供了主键生成器，生成器调用后会将结果设置到实体对应的属性上
     *
     * @return 插入成功的数量，0-表示失败
     */
    int insert() throws InsertException;

    /**
     * 删除的数据，按所有不为空的字段匹配
     * @return 删除的数量
     */
    int delete() throws DeleteException;

    /**
     * 更新记录
     *
     * @param entity 更新的值
     * @return 更新的记录数量
     */
    int update(T entity) throws UpdateException;

    /**
     * 按给定的条件查询列表
     *
     * @return 匹配到的记录列表，必将返回一个list
     */
    List<T> query() throws QueryException;

    /**
     * 按给定的条件查询一个
     *
     * @return 若存在多个则会抛出查询异常，若不存在，则返回null
     */
    T onlyOne() throws QueryException;

    /**
     * 按给定的条件分页查询
     *
     * @return IPage，如果没有分页工厂则返回null
     */
    IPage<T> page() throws QueryException;

    /**
     * 按给定的条件统计数量
     *
     * @return 匹配到的记录数量
     */
    Long count() throws QueryException;

}
