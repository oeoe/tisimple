package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value="user_tab_comments")
public class UserTableComment extends TiEntity<UserTableComment> {
	@TiColumn(value="TABLE_NAME")
	private String tableName;
	@TiColumn(value="TABLE_TYPE")
	private String tableType;
	@TiColumn(value="COMMENTS")
	private String comments;
	@TiColumn(value="ORIGIN_CON_ID")
	private java.math.BigDecimal originConId;

	public void setTableName(String tableName){this.tableName=tableName;}

	public String getTableName(){return this.tableName;}

	public void setTableType(String tableType){this.tableType=tableType;}

	public String getTableType(){return this.tableType;}

	public void setComments(String comments){this.comments=comments;}

	public String getComments(){return this.comments;}

	public void setOriginConId(java.math.BigDecimal originConId){this.originConId=originConId;}

	public java.math.BigDecimal getOriginConId(){return this.originConId;}


	@Override
	public String toString() {
		return "UserTabComments{"+
			"tableName="+tableName+","+
			"tableType="+tableType+","+
			"comments="+comments+","+
			"originConId="+originConId+
		"}";
	}
}