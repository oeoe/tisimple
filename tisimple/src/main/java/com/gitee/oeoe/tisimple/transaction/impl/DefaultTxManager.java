package com.gitee.oeoe.tisimple.transaction.impl;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.exception.TxCommitException;
import com.gitee.oeoe.tisimple.exception.TxException;
import com.gitee.oeoe.tisimple.transaction.ITxManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 基于 Connection 的事务管理
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/15
 */
public class DefaultTxManager implements ITxManager {
    private static final Logger logger = LoggerFactory.getLogger(DefaultTxManager.class);

    private Connection connection;

    private Connection agentConnection;


    @Override
    public Connection getConnection() {
        return isValid() ? this.agentConnection : TiDBPoolManager.getConnection();
    }

    @Override
    public Connection getRealConnection() {
        return this.connection;
    }

    @Override
    public void open() {
        Connection connection = TiDBPoolManager.getConnection();
        try {
            connection.setReadOnly(false);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new TxException("开启事务失败", e);
        }
        this.connection = connection;
        this.agentConnection = agentConnection(connection);
        logger.debug("事务已开启");
    }

    @Override
    public ITxManager join(TiEntity<?> entity) {
        entity.setTxManager(this);
        return this;
    }

    @Override
    public boolean isValid() {
        try {
            return this.connection.isValid(3) && !this.connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void commit() {
        if (this.connection == null) {
            throw new TxCommitException("提交失败,连接无效");
        }
        try {
            boolean closed = this.connection.isClosed();
            if (closed) {
                throw new TxCommitException("事务提交失败,连接已关闭");
            }
            try {
                this.connection.commit();
            } catch (SQLException e) {
                throw new TxCommitException("事务提交失败", e);
            }
        } catch (SQLException e) {
            throw new TxCommitException("事务提交失败,连接异常", e);
        }
    }

    @Override
    public void rollback() {
        if (this.connection == null) {
            return;
        }
        try {
            boolean closed = this.connection.isClosed();
            if (closed) {
                logger.debug("the connection is closed，rollback action invalid");
            }
            if (!closed) {
                try {
                    this.connection.rollback();
                } catch (SQLException e) {
                    throw new TxException("事务回滚失败。", e);
                }
            }
        } catch (SQLException e) {//isClosed的异常
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        if (this.connection == null) {
            return;
        }
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("事务已关闭");
    }

    public Connection agentConnection(Connection connection) {
        AgentConnection handler = new AgentConnection(connection);
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(), new Class[]{Connection.class}, handler);
    }
}
