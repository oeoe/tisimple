package com.gitee.oeoe.tisimple.page;

import com.gitee.oeoe.tisimple.template.ISQL;

import java.util.List;

/**
 * 分页工厂，处理分页sql的生成以及结果的组装为Page
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/11
 */
public interface IPageFactory extends IPageParam {

    /**
     * 获取统计此分页条件西安的总记录数量的sql
     *
     * @param sql 模版sql对象
     * @return 模版sql对象
     */
    ISQL countSql(ISQL sql);

    /**
     * countSql 中返回的记录数量列名，比如select count(*)as max_row from user;中的 max_row
     *
     * @return 列名
     */
    String countSqlSelectName();

    /**
     * 获取分页sql
     *
     * @param sql 模版sql对象
     * @return 模版sql对象
     */
    ISQL pageSql(ISQL sql);

    /**
     * 组装成一个分页对象
     *
     * @param pageSize  页大小
     * @param pageIndex 当前页
     * @param <T>       分页对象类型
     * @param maxRows   此分页条件下的总记录数量
     * @param data      分页结果
     * @return 一个分页对象
     */
    <T> IPage<T> assemble(List<T> data, Long maxRows, int pageSize, int pageIndex);

    /**
     * 从参数空间中获取分页大小参数
     *
     * @param template 模版SQL
     * @return 分页大小
     */
    int getPageSize(ISQL template);

    /**
     * 从参数空间中获取当前分页编号参数
     *
     * @param template 模版SQL
     * @return 当前页索引
     */
    int getPageIndex(ISQL template);

    /**
     * 设置默认分页大小
     *
     * @param size 默认的分页大小
     */
    void setDefaultPageSize(int size);

    /**
     * 获取默认分页大小
     *
     * @return 默认分页大小
     */
    int getDefaultPageSize();
}
