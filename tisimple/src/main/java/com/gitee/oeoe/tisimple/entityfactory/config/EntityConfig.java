package com.gitee.oeoe.tisimple.entityfactory.config;

import com.gitee.oeoe.tisimple.exception.EntityBuildException;
import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.entityfactory.ITableInfoFactory;
import com.gitee.oeoe.tisimple.exception.UnPackResultSetException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;

/**
 * 每个实体的生成配置
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/16
 */
public class EntityConfig {
    private String className;//类名
    private String tableName;//表名
    private String packagePathName;//包路径名
    private String fileOutPath;//生成的文件的输出路径
    private String tableInfoFactoryClass;//提供额外的注解信息工厂
    private String selectOneSql;//查询sql

    public boolean isEmpty(Object obj) {
        if (null == obj) {
            return true;
        }
        if (obj instanceof String) {
            return ((String) obj).trim().length() < 1;
        }
        return false;
    }

    //默认处理
    private void fillDefault(YamlConfig config) {
        String defaultFileOutPath = config.getDefaultFileOutPath();
        String defaultPackagePathName = config.getDefaultPackagePathName();
        String defaultTableInfoFactory = config.getDefaultTableInfoFactoryClass();

        if (isEmpty(this.fileOutPath)) {
            this.fileOutPath = defaultFileOutPath;
        }
        if (isEmpty(this.packagePathName)) {
            this.packagePathName = defaultPackagePathName;
        }
        if (isEmpty(this.tableInfoFactoryClass)) {
            this.tableInfoFactoryClass = defaultTableInfoFactory;
        }
    }

    //后置处理
    public void postHandel(YamlConfig config) {
        //======================默认赋值
        fillDefault(config);
        //======================基本校验校验
        if (isEmpty(this.className)) {
            exceptionThrow("className");
        }
        if (isEmpty(this.tableName)) {
            exceptionThrow("tableName");
        }
        if (isEmpty(this.packagePathName)) {
            exceptionThrow("packagePathName");
        }
        if (isEmpty(this.fileOutPath)) {
            exceptionThrow("fileOutPath");
        }
        if (isEmpty(this.tableInfoFactoryClass)) {
            exceptionThrow("tableInfoFactoryClass");
        }
        if (isEmpty(this.selectOneSql)) {
            exceptionThrow("selectOneSql");
        }
        //======================fileOutPath校验
        String rootPath = config.getRootPath();
        if (isEmpty(rootPath)) {
            URL resource = Thread.currentThread().getContextClassLoader().getResource(".");
            assert resource != null;
            rootPath = resource.getFile();
        }
        String filePath = this.fileOutPath;
        if (filePath.startsWith("./") || filePath.startsWith(".\\\\")) {
            filePath = rootPath + File.separator + filePath.substring(2);
        }
        filePath = filePath.replaceAll("[/|\\\\]+", "\\" + File.separator);
        File file = new File(filePath);
        if (!file.exists()) {
            if (config.getAutoCreateFileOutPath()) {
                boolean mkdirs = file.mkdirs();
                if (!mkdirs) {
                    throw new EntityBuildException("配置项异常,fileOutPath 配置的路径不存在，且自动创建失败。" + filePath);
                }
            } else {
                throw new EntityBuildException("配置项异常,fileOutPath 配置的路径不存在。" + filePath);
            }
        }
        this.fileOutPath = filePath;
        //======================tableInfoFactory校验
        Class<?> infoClass;
        try {
            infoClass = Class.forName(this.tableInfoFactoryClass);
        } catch (ClassNotFoundException e) {
            throw new EntityBuildException("配置项异常,tableInfoFactoryClass 配置的表信息获取工厂类不存在。" + this.tableInfoFactoryClass);
        }
        if (!ITableInfoFactory.class.isAssignableFrom(infoClass)) {
            throw new EntityBuildException("配置项异常,tableInfoFactoryClass 配置的表信息获取工厂类的类型不正确,应为 ITableInfoFactory 的实现类。" + this.tableInfoFactoryClass);
        }
        //======================表存在校验
        try {
            ITableInfoFactory tableInfoFactory = (ITableInfoFactory) infoClass.newInstance();
            boolean exist = tableInfoFactory.exist(this.tableName);
            if (!exist) {
                throw new EntityBuildException("配置项异常,tableName, 配置的表不存在。" + this.getTableName());
            }

        } catch (InstantiationException | IllegalAccessException e) {
            throw new EntityBuildException("配置项异常，tableInfoFactoryClass,配置的表信息获取工厂类实例化失败。" + this.tableInfoFactoryClass, e);
        }

        //======================sql校验
        ISQL template = SqlFactory.sql(this.selectOneSql);
        try {
            Map<String, Object> one = TiSimpleCore.queryOne(template);
            if (one.isEmpty()) {
                throw new EntityBuildException("配置项异常,selectOneSql,配置查询sql执行未返回数据。" + this.selectOneSql);
            }
            for (String key : one.keySet()) {
                Object val = one.get(key);
                if (isEmpty(val)) {
                    throw new EntityBuildException("配置项异常,selectOneSql,sql执行返回的记录中列" + key + "存在空值,请更改sql以查回的记录不存在空值,或填充空值项。sql：" + this.selectOneSql + "\n返回的数据：" + one);
                }
            }

        } catch (SQLException | UnPackResultSetException e) {
            throw new EntityBuildException("配置项异常,selectOneSql, 配置的sql执行异常。" + this.selectOneSql, e);
        }
    }

    public void exceptionThrow(String field) {
        throw new EntityBuildException("配置项异常, " + field + "，不能为空");
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPackagePathName() {
        return packagePathName;
    }

    public void setPackagePathName(String packagePathName) {
        this.packagePathName = packagePathName;
    }

    public String getFileOutPath() {
        return fileOutPath;
    }

    public void setFileOutPath(String fileOutPath) {
        this.fileOutPath = fileOutPath;
    }

    public String getTableInfoFactoryClass() {
        return tableInfoFactoryClass;
    }

    public void setTableInfoFactoryClass(String tableInfoFactoryClass) {
        this.tableInfoFactoryClass = tableInfoFactoryClass;
    }

    public String getSelectOneSql() {
        return selectOneSql;
    }

    public void setSelectOneSql(String selectOneSql) {
        this.selectOneSql = selectOneSql;
    }

    @Override
    public String toString() {
        return "OneBuildConfig{" +
                "className='" + className + '\'' +
                ", tableName='" + tableName + '\'' +
                ", packagePathName='" + packagePathName + '\'' +
                ", fileOutPath='" + fileOutPath + '\'' +
                ", tableInfoFactoryClass='" + tableInfoFactoryClass + '\'' +
                ", defaultSelectOneSql='" + selectOneSql + '\'' +
                '}';
    }

}
