package com.gitee.oeoe.tisimple.entityfactory.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * 实体生成配置-yaml配置
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/16
 */
public class YamlConfig {
    private static final Logger logger = LoggerFactory.getLogger(YamlConfig.class);

    private String rootPath;//配置项目的绝对路径
    private String defaultPackagePathName;//全局配置包路径名
    private String defaultFileOutPath;//全局配置生成的文件的输出路径，可以是相对rootPath的路径，以点开头，否则rootpath将是当前class文件的项目路径
    private String defaultTableInfoFactoryClass;//全局配置提供额外的注解信息的工厂
    private Boolean autoCreateFileOutPath;//如果输出路径不存在，是否自动创建
    private List<EntityConfig> details;//每个实体的配置


    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getDefaultPackagePathName() {
        return defaultPackagePathName;
    }

    public void setDefaultPackagePathName(String defaultPackagePathName) {
        this.defaultPackagePathName = defaultPackagePathName;
    }

    public String getDefaultFileOutPath() {
        return defaultFileOutPath;
    }

    public void setDefaultFileOutPath(String defaultFileOutPath) {
        this.defaultFileOutPath = defaultFileOutPath;
    }

    public String getDefaultTableInfoFactoryClass() {
        return defaultTableInfoFactoryClass;
    }

    public void setDefaultTableInfoFactoryClass(String defaultTableInfoFactoryClass) {
        this.defaultTableInfoFactoryClass = defaultTableInfoFactoryClass;
    }

    public Boolean getAutoCreateFileOutPath() {
        return autoCreateFileOutPath;
    }

    public void setAutoCreateFileOutPath(Boolean autoCreateFileOutPath) {
        this.autoCreateFileOutPath = autoCreateFileOutPath;
    }

    public List<EntityConfig> getDetails() {
        return details;
    }

    public void setDetails(List<EntityConfig> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "BuildYamlConfig{" +
                "rootPath='" + rootPath + '\'' +
                ", defaultPackagePathName='" + defaultPackagePathName + '\'' +
                ", defaultFileOutPath='" + defaultFileOutPath + '\'' +
                ", defaultTableInfoFactoryClass='" + defaultTableInfoFactoryClass + '\'' +
                ", autoCreateOutPath=" + autoCreateFileOutPath +
                ", details=" + details +
                '}';
    }

    public static YamlConfig getInstance(String yaml) {
        if (null == yaml) {
            return null;
        }
        URL resource = Thread.currentThread().getContextClassLoader().getResource(yaml);
        if (null == resource) {
            logger.debug("in classpath no config file named " + yaml);
            return null;
        }
        Yaml parse = new Yaml(new Constructor(YamlConfig.class));
        try (FileReader fileReader = new FileReader(resource.getFile())) {
            YamlConfig config = parse.load(fileReader);
            //配置合并处理与校验
            List<EntityConfig> details = config.getDetails();
            logger.debug("=======================配置校验=============================");
            for (EntityConfig detail : details) {
                detail.postHandel(config);
            }
            logger.debug("=======================配置校验完成=============================");
            return config;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
