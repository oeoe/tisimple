package com.gitee.oeoe.tisimple.exception;

/**
 * 条件不满足异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class ConditionNotMetException extends RuntimeException {
    public ConditionNotMetException(String message) {
        super(message);
    }

    public ConditionNotMetException(String message, Throwable cause) {
        super(message, cause);
    }
}
