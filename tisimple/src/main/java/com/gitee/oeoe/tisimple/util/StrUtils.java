package com.gitee.oeoe.tisimple.util;

/**
 * 字符串处理工具
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/4
 */
public class StrUtils {
    /**
     * 将字符串转换为驼峰形式
     * 默认以字符串中_和-分隔
     *
     * @param str 待转换的字符串
     * @return 转换后的字符串
     */
    public static String camel(String str) {
        if (null == str || str.trim().length() < 1) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        String[] ss = str.split("[_|-]");
        for (String s : ss) {
            if (s.length() > 1) {
                String head = s.substring(0, 1);
                String tail = s.substring(1);
                builder.append(head.toUpperCase());
                builder.append(tail.toLowerCase());
            } else if (s.length() == 1) {
                builder.append(s.toUpperCase());
            }
        }
        if (builder.length() > 1) {
            String head = builder.substring(0, 1);
            String tail = builder.substring(1);
            return head.toLowerCase() + tail;
        } else if (builder.length() == 1) {
            return builder.toString().toLowerCase();
        }
        return "";
    }

    /**
     * 字符串首字母大写
     *
     * @param str 字符串
     * @return 首字母大写的字符串
     */
    public static String capitalize(String str) {
        if (null == str || str.trim().length() < 1) {
            return "";
        }
        if (str.trim().length() > 1) {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        } else {
            return str.toUpperCase();
        }
    }
}
