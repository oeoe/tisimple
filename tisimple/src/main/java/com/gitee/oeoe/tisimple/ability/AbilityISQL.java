package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;
import com.gitee.oeoe.tisimple.exception.ConditionNotMetException;
import com.gitee.oeoe.tisimple.exception.GenerateSqlException;
import com.gitee.oeoe.tisimple.extra.IFieldMatchRuleFactory;
import com.gitee.oeoe.tisimple.extra.IKeyGenerateFactory;
import com.gitee.oeoe.tisimple.extra.keygenertefactory.NoneFactory;
import com.gitee.oeoe.tisimple.extra.matchtypefactory.EqRuleFactory;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 生成基础功能的通用模版sql
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/25
 */
public class AbilityISQL {

    /**
     * 生成insert模版sql
     *
     * @param entity 实体
     * @return 模版sql
     */
    public ISQL insertISQL(TiEntity<?> entity) {
        String tableName = table(entity);
        String fieldWrapChar = wrapChar(entity);
        boolean autoIncrement=false;
        Map<String, Object> param = new HashMap<>();
        StringBuilder head = new StringBuilder();
        head.append("insert into ").append(tableName).append("(");
        StringBuilder tail = new StringBuilder("values (");
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                Object o = f.get(entity);
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                String fieldName = f.getName();
                String columnName = f.getName();
                if (null != alis) {
                    columnName = alis.value();
                    //value factory invoke
                    if (o == null) {
                        Class<? extends IKeyGenerateFactory<?>> keyFactory = alis.keyGenerateFactory();
                        if (keyFactory != NoneFactory.class) {
                            try {
                                IKeyGenerateFactory<?> keyGenerate = keyFactory.newInstance();
                                Object o1 = keyGenerate.key();
                                if (o1 != null && o1.getClass() == f.getType()) {
                                    o = o1;
                                    f.set(entity, o);
                                }
                            } catch (InstantiationException e) {
                                throw new GenerateSqlException("生成sql失败！", e);
                            }
                        }
                    }
                    autoIncrement = autoIncrement||alis.autoIncrement();
                    //null check
                    boolean nullAble = alis.nullAble();
                    if (!nullAble && !alis.autoIncrement()) {
                        if (null == o) {
                            throw new GenerateSqlException(entity.getClass().getTypeName() + "的属性 " + fieldName + " 非NULL约束未通过。");
                        }
                    }
                    //key check
                    boolean key = alis.key();
                    if (!nullAble &&key) {
                        if (null == o) {
                            throw new GenerateSqlException(entity.getClass().getTypeName() + "的属性 " + fieldName + " key不能为NULL。");
                        }
                    }
                }
                if (null == o) {
                    continue;
                }
                head.append(fieldWrapChar).append(columnName).append(fieldWrapChar).append(",");
                tail.append("{").append(fieldName).append("}").append(",");
                param.put(fieldName, o);
            } catch (IllegalAccessException e) {
                throw new GenerateSqlException("生成sql失败！", e);
            }
        }

        if (head.toString().endsWith("(")) {
            throw new ConditionNotMetException("insert sql 无法生成,由于当前的数据对象所有属性都为null！");
        }
        String sql = head.substring(0, head.length() - 1) + ") " + tail.substring(0, tail.length() - 1) + ")";
        return SqlFactory.sql(sql, param).supportGeneratorKey(autoIncrement);
    }

    /**
     * 生成insert模版sql
     *
     * @param entities 实体列表
     * @return 模版sql列表
     */
    public List<ISQL> insertISQL(List<? extends TiEntity<?>> entities) {
        LinkedList<ISQL> rs = new LinkedList<>();
        for (TiEntity<?> entity : entities) {
            ISQL isql = insertISQL(entity);
            rs.add(isql);
        }
        return rs;
    }

    /**
     * 生成delete模版sql
     *
     * @param condition 实体
     * @return 模版sql
     */
    public ISQL deleteISQL(TiEntity<?> condition) {
        String tableName = table(condition);
        String fieldWrapChar = wrapChar(condition);
        Map<String, Object> param = new HashMap<>();
        StringBuilder builder = new StringBuilder("delete from ").append(tableName).append(" where");
        Field[] fields = condition.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                Object o = f.get(condition);
                if (null == o) {
                    continue;
                }
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                String columnName = null != alis ? alis.value() : f.getName();
                String fieldName = f.getName();
                builder.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar);
                builder.append("=").append("{").append(fieldName).append("}").append(" and");
                param.put(fieldName, o);
            } catch (IllegalAccessException e) {
                throw new GenerateSqlException("生成sql失败！", e);
            }
        }
        String sql = builder.toString();
        if (sql.endsWith("where")) {
            sql = sql.substring(0, sql.length() - 5);
        }
        if (sql.endsWith("and")) {
            sql = sql.substring(0, sql.length() - 3);
        }
        return SqlFactory.sql(sql, param);
    }

    /**
     * 生成update模版sql
     *
     * @param condition 条件实体
     * @param entity    更新实体
     * @return 模版sql
     */
    public ISQL updateISQL(TiEntity<?> condition, TiEntity<?> entity) {
        String tableName = table(condition);
        String fieldWrapChar = wrapChar(condition);
        Map<String, Object> param = new HashMap<>();
        StringBuilder head = new StringBuilder("update ").append(tableName).append(" set");
        StringBuilder tail = new StringBuilder();
        Field[] fields = condition.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                String columnName = null != alis ? alis.value() : f.getName();
                Object o = f.get(entity);
                if (null != o) {
                    String keyName = "set_" + f.getName();
                    head.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append("=").append("{").append(keyName).append("}").append(" ,");
                    param.put(keyName, o);
                }
                Object c = f.get(condition);
                if (null != c) {
                    String keyName = "where_" + f.getName();
                    tail.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append("=").append("{").append(keyName).append("}").append(" and");
                    param.put(keyName, c);
                }
            } catch (IllegalAccessException e) {
                throw new GenerateSqlException("生成sql失败！", e);
            }
        }

        String sqlHead = head.toString();
        if (sqlHead.endsWith("set")) {
            throw new ConditionNotMetException("update sql 无法生成,由于需更新字段值都为null！");
        }
        String sql = null;
        if (sqlHead.endsWith(",")) {
            sql = sqlHead.substring(0, sqlHead.length() - 1);
        }
        String sqlTail = tail.toString();
        if (sqlTail.endsWith("and")) {
            sql = sql + " where " + sqlTail.substring(0, sqlTail.length() - 3);
        }
        return SqlFactory.sql(sql, param);
    }

    /**
     * 生成query模版sql
     *
     * @param condition 实体
     * @return 模版sql
     */
    public ISQL queryISQL(TiEntity<?> condition) {
        String tableName = table(condition);
        String fieldWrapChar = wrapChar(condition);
        Map<String, Object> param = new HashMap<>();
        StringBuilder head = new StringBuilder("select");
        StringBuilder tail = new StringBuilder();
        Field[] fields = condition.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                String columnName = null != alis ? alis.value() : f.getName();
                head.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append(" ,");
                Object o = f.get(condition);
                if (null == o) {
                    continue;
                }
                queryConditionSql(fieldWrapChar, param, tail, f, alis, columnName, o);
            } catch (IllegalAccessException e) {
                throw new GenerateSqlException("生成sql失败！", e);
            }
        }
        String sql = head.substring(0, head.length() - 1) + " from " + tableName;
        String sqlTail = tail.toString();
        if (sqlTail.endsWith("and")) {
            sql = sql + " where" + sqlTail.substring(0, sqlTail.length() - 3);
        }
        return SqlFactory.sql(sql, param);
    }

    /**
     * 生成count模版sql
     *
     * @param condition  条件实体
     * @param selectName sql中的数量别名
     * @return 模版sql
     */
    public ISQL countISQL(TiEntity<?> condition, String selectName) {
        String tableName = table(condition);
        String fieldWrapChar = wrapChar(condition);
        Map<String, Object> param = new HashMap<>();
        StringBuilder tail = new StringBuilder();
        Field[] fields = condition.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                Object o = f.get(condition);
                if (null == o) {
                    continue;
                }
                String columnName = null != alis ? alis.value() : f.getName();
                queryConditionSql(fieldWrapChar, param, tail, f, alis, columnName, o);
            } catch (IllegalAccessException e) {
                throw new GenerateSqlException("生成sql失败！", e);
            }
        }
        String sql = "select count(*) as " + selectName + " from " + tableName;
        String sqlTail = tail.toString();
        if (sqlTail.endsWith("and")) {
            sql = sql + " where" + sqlTail.substring(0, sqlTail.length() - 3);
        }
        return SqlFactory.sql(sql, param);
    }

    /**
     * 查询字段匹配规则生成
     *
     * @param fieldWrapChar 字段前后缀符
     * @param tail          sql条件片段
     * @param matchFactory  匹配规则工厂
     * @param value         当前字段值
     * @param columnName    当前字段对应的列名
     * @param fieldName     当前字段名
     * @throws GenerateSqlException 反射生成匹配工厂异常
     */
    private void querySqlFieldGenerate(String fieldWrapChar, StringBuilder tail, Class<? extends IFieldMatchRuleFactory> matchFactory, Object value, String columnName, String fieldName) throws GenerateSqlException {
        if (matchFactory == EqRuleFactory.class) {
            tail.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append("=").append("{").append(fieldName).append("}").append(" and");
            return;
        }
        IFieldMatchRuleFactory matchSqlFactory;
        try {
            matchSqlFactory = matchFactory.newInstance();
        } catch (Exception e) {
            throw new GenerateSqlException("生成sql失败！", e);
        }
        StringBuilder builder = new StringBuilder();
        Object newVal = matchSqlFactory.sql(builder, fieldWrapChar + columnName + fieldWrapChar, value);
        String sqlFragment = builder.toString().trim();
        if (sqlFragment.length() < 1) {
            tail.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append("=").append("{").append(fieldName).append("}").append(" and");
            return;
        }
        sqlFragment = sqlFragment.replaceAll("\\?", "");
        if (newVal != null) {
            tail.append(" ").append(sqlFragment).append("{").append(fieldName).append("}").append(" and");
        } else {
            tail.append(" ").append(sqlFragment).append(" and");
        }
    }

    /**
     * 查询条件生成
     *
     * @param fieldWrapChar 字段前后缀符
     * @param param         参数空间
     * @param tail          sql条件片段
     * @param f             属性
     * @param alis          字段注解
     * @param columnName    列名
     * @param o             属性值
     */
    private void queryConditionSql(String fieldWrapChar, Map<String, Object> param, StringBuilder tail, Field f, TiColumn alis, String columnName, Object o) {
        String fieldName = f.getName();
        if (null != alis) {
            querySqlFieldGenerate(fieldWrapChar, tail, alis.matchRuleFactory(), o, columnName, fieldName);
        } else {
            tail.append(" ").append(fieldWrapChar).append(columnName).append(fieldWrapChar).append("=").append("{").append(fieldName).append("}").append(" and");
        }
        param.put(fieldName, o);
    }


    /**
     * 获取实体的表名
     *
     * @param condition 实体
     * @return 表名
     */
    private String table(TiEntity<?> condition) {
        TiTable tableAnnotation = condition.getClass().getDeclaredAnnotation(TiTable.class);
        if (null == tableAnnotation || tableAnnotation.value().trim().length() < 1) {
            throw new ConditionNotMetException("当前实体对应的表名未知,请在实体类 " + condition.getClass().getTypeName() + " 上添加注解 @TiTable声明表名 ！");
        }
        return tableAnnotation.value().trim();
    }

    /**
     * 获取字段包裹字符
     *
     * @param condition 实体
     * @return 包裹字符
     */
    private String wrapChar(TiEntity<?> condition) {
        TiTable tableAnnotation = condition.getClass().getDeclaredAnnotation(TiTable.class);
        if (null == tableAnnotation || tableAnnotation.wrapChar().trim().length() < 1) {
            return "";
        }
        return tableAnnotation.wrapChar().trim();
    }
}
