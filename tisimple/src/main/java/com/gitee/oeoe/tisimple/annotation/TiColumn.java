package com.gitee.oeoe.tisimple.annotation;


import com.gitee.oeoe.tisimple.extra.IFieldMatchRuleFactory;
import com.gitee.oeoe.tisimple.extra.IKeyGenerateFactory;
import com.gitee.oeoe.tisimple.extra.keygenertefactory.NoneFactory;
import com.gitee.oeoe.tisimple.extra.matchtypefactory.EqRuleFactory;

import java.lang.annotation.*;

/**
 * 指定类属性对应的表列名，若不指定，将使用对象属性名与表字段名完全匹配
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TiColumn {
    /**
     * 指定表字段名
     *
     * @return 字段名
     */
    String value();

    /**
     * 指定此属性没有对应表字段，将不会被sql化处理，也不会被设值
     * 是否是非表字段，默认是表字段，true表示不是表字段
     *
     * @return true or false
     */
    boolean nonColumn() default false;

    /**
     * 表字段的注释
     *
     * @return 字段注释
     */
    String comment() default "";


    /**
     * 是否可为null，默认可为null
     *
     * @return true or false
     */
    boolean nullAble() default true;


    /**
     * 是否是表的键，主键，联合主键之一，默认不是键，且主键默认非null，nullAble优先
     *
     * @return true or false
     */
    boolean key() default false;


    /**
     * 唯一主键且自增，默认不是自增主键
     *
     * @return true or false
     */
    boolean autoIncrement() default false;

    /**
     * 主键生成策略，在进行插入的时候，自动赋值，默认不做任何处理
     *
     * @return 值生成工厂
     * @see IKeyGenerateFactory
     */
    Class<? extends IKeyGenerateFactory<?>> keyGenerateFactory() default NoneFactory.class;

    /**
     * 查询sql语句中，字段的匹配片段生成规则，默认匹配是 =
     *
     * @return 匹配规则工厂
     * @see IFieldMatchRuleFactory
     */
    Class<? extends IFieldMatchRuleFactory> matchRuleFactory() default EqRuleFactory.class;
}
