package com.gitee.oeoe.tisimple.template.fun;

import com.gitee.oeoe.tisimple.exception.SqlFunException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFunc;

import java.util.Collection;

/**
 * 循环遍历生成，p的类型必须是Collection类型，副参数有至少3个，第一个表示遍历拼接的前置字符，第二个为元素连接字符，第三个为后置字符，如果第四个参数不为空，将生成直接字符串
 * <p>
 * 模版语法，[foreach@list#where  name in (|,|)]，例如 select * from s_user [foreach@list#where  name in (|,|)]
 * 其中foreach表示调用此方法注册名，@与#间的list是参数空间中的参数名，是主参数，其必须为一个Collection类型，#后面是副参数块，参数块被|分割为3个副参数，
 * 第一个参数为 'where  name in ('
 * 第二个参数为'，'
 * 第三个参数为')'
 * 若list中的元素为[1,2,3]，[foreach@list#where  name in (|,|)]将返回类似如此的结构 where name in ({var1},{var2},{var3})，并将三个元素添加到参数空间
 * 若list中的元素为[1,2,3]，[foreach@list#where  name in (|,|)|true]将返回 where name in ('1','2','3')，第四个参数可以是任何字符
 * <p>
 * 将迭代list的元素，用第二个副参数拼接所有元素，在返回的时候第一个副参数将作为前缀，第三个副参数做为后缀
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/8
 */
public class ForeachFunc implements SqlFunc {
    @Override
    public String apply(ISQL template, Object p, Object[] param) {
        if (null == p) {
            return "";
        }
        if (p instanceof Collection && ((Collection<?>) p).isEmpty()) {
            return "";
        }
        if (param.length < 3) {
            throw new SqlFunException("参数个数不正确");
        }
        long nano = 0;
        Collection<?> p0;
        String p1 = (String) param[0];
        String p2 = (String) param[1];
        String p3 = (String) param[2];
        boolean p4 = true;
        if (param.length > 3) {
            String f = (String) param[3];
            if (null != f) {
                p4 = false;
            }
        } else {
            nano = System.nanoTime();
        }
        if (p instanceof Collection) {
            p0 = (Collection<?>) p;
            int size = p0.size();
            int index = 0;
            StringBuilder temp = new StringBuilder(p1);
            for (Object o : p0) {
                if (p4) {
                    String key = "fch_" + nano + "_" + index;
                    if (index++ < size - 1) {
                        temp.append("{").append(key).append("}").append(p2);
                    } else {
                        temp.append("{").append(key).append("}").append(p3);
                    }
                    template.addParam(key, o);
                } else {
                    if (index++ < size - 1) {
                        temp.append("'").append(o).append("'").append(p2);
                    } else {
                        temp.append("'").append(o).append("'").append(p3);
                    }
                }
            }
            return temp.toString();
        }
        throw new SqlFunException("主参数类型不正确,应为集合类型");
    }
}
