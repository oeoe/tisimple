package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value="user_col_comments")
public class UserTableColumnComment extends TiEntity<UserTableColumnComment> {
	@TiColumn(value="TABLE_NAME")
	private String tableName;
	@TiColumn(value="COLUMN_NAME")
	private String columnName;
	@TiColumn(value="COMMENTS")
	private String comments;
	@TiColumn(value="ORIGIN_CON_ID")
	private java.math.BigDecimal originConId;

	public void setTableName(String tableName){this.tableName=tableName;}

	public String getTableName(){return this.tableName;}

	public void setColumnName(String columnName){this.columnName=columnName;}

	public String getColumnName(){return this.columnName;}

	public void setComments(String comments){this.comments=comments;}

	public String getComments(){return this.comments;}

	public void setOriginConId(java.math.BigDecimal originConId){this.originConId=originConId;}

	public java.math.BigDecimal getOriginConId(){return this.originConId;}


	@Override
	public String toString() {
		return "UserColComments{"+
			"tableName="+tableName+","+
			"columnName="+columnName+","+
			"comments="+comments+","+
			"originConId="+originConId+
		"}";
	}
}