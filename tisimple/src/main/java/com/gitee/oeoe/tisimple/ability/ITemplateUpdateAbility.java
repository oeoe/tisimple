package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.exception.DeleteException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;

/**
 * 提供基于模版sql更新接口
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/9
 */
public interface ITemplateUpdateAbility {

    /**
     * 按给定的条件更新记录，实体本身的属性将放入到参数空间
     *
     * @param sql 使用模版语法的sql语句
     * @return 更新的数量
     * @throws DeleteException 更新中各个阶段出现异常
     */
    int update(String sql) throws DeleteException;

    /**
     * 按给定的条件更新记录
     *
     * @param template sql模版对象
     * @return 更新的数量
     * @throws DeleteException 更新中各个阶段出现异常
     */

    int update(ISQL template) throws DeleteException;
}