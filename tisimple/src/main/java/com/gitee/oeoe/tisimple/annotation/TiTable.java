package com.gitee.oeoe.tisimple.annotation;

import java.lang.annotation.*;

/**
 * 指定类对应的表名
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TiTable {
    /**
     * 指定表名
     *
     * @return 表名
     */
    String value();

    /**
     * 表的注释
     *
     * @return 注释
     */
    String comment() default "";

    /**
     * 指定字段包裹字符，例如mysql表有字段system，在sql语句中应包裹反引号，'`'，变成`system`
     * 默认空，即无需任何字符包裹
     *
     * @return 前后缀字符串
     */
    String wrapChar() default "";
}
