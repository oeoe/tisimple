package com.gitee.oeoe.tisimple.exception;

/**
 * 解析ResultSet异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class UnPackResultSetException extends Exception {
    public UnPackResultSetException(String message) {
        super(message);
    }

    public UnPackResultSetException(String message, Throwable cause) {
        super(message, cause);
    }
}
