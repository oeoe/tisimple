package com.gitee.oeoe.tisimple.entityfactory;

import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.entityfactory.config.EntityConfig;
import com.gitee.oeoe.tisimple.entityfactory.config.YamlConfig;
import com.gitee.oeoe.tisimple.exception.ConditionNotMetException;
import com.gitee.oeoe.tisimple.exception.EntityBuildException;
import com.gitee.oeoe.tisimple.util.StrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.List;


/**
 * 从table生成entity的工具
 *
 * @author zhu qiang
 * @see YamlConfig
 * @see EntityConfig
 * @since 1.8+ 2022/1/4
 */
public class EntityBuilderFactory {
    private static final Logger logger = LoggerFactory.getLogger(EntityBuilderFactory.class);

    public static EntityBuilder build() {
        return new EntityBuilder();
    }

    /**
     * 从yaml配置中生成实体
     */
    public static void buildFromYaml() {
        buildFromYaml("entity.yaml");//默认配置文件
    }

    /**
     * 从指定的yaml配置中生成实体
     *
     * @param yaml yaml配置文件路径名称
     */
    public static void buildFromYaml(String yaml) {
        YamlConfig instance = YamlConfig.getInstance(yaml);
        if (null == instance) {
            return;
        }
        List<EntityConfig> details = instance.getDetails();
        logger.debug("===================实体生成开始=======================");
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try (Connection connection = TiDBPoolManager.getConnection()) {
            for (int i = 0; i < details.size(); i++) {
                EntityConfig detail = details.get(i);
                String selectOneSql = detail.getSelectOneSql();
                String tableName = detail.getTableName();
                String className = detail.getClassName();
                String fileOutPath = detail.getFileOutPath();
                String packagePathName = detail.getPackagePathName();
                String tableInfoFactoryClass = detail.getTableInfoFactoryClass();
                Class<?> infoClassFactory = Class.forName(tableInfoFactoryClass);
                ITableInfoFactory infoFactoryInstance = (ITableInfoFactory) infoClassFactory.newInstance();
                preparedStatement = connection.prepareStatement(selectOneSql);
                resultSet = preparedStatement.executeQuery();
                EntityBuilderFactory.build()
                        .setPackName(packagePathName)
                        .setClassName(className)
                        .setTableName(tableName)
                        .setOutPath(fileOutPath)
                        .setInfoFactoryInstance(infoFactoryInstance)
                        .setResultSet(resultSet)
                        .generate();
                logger.debug("\n=======" + (i + 1) + "/" + details.size() + "=========" + className + " 实体生成成功======================\n");
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.debug("===================实体生成异常结束01=======================");
            e.printStackTrace();
        } catch (IOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            logger.debug("===================实体生成异常结束02=======================");
        } finally {
            try {
                if (null != resultSet) {
                    resultSet.close();
                }
                if (null != preparedStatement) {
                    preparedStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }


    public static class EntityBuilder {
        private String className;//类名
        private String tableName;//表名
        private String packName;//包路径名
        private String outPath;//生成的文件的输出路径
        private Class<? extends ITableInfoFactory> infoFactory;//提供额外的注解信息
        private ITableInfoFactory infoFactoryInstance;//提供额外的注解信息
        /**
         * 类似 select * from xxx limit 1  这样的sql查询结果，表中所有列必须有值。
         */
        private ResultSet rs;

        private EntityBuilder() {
        }


        public EntityBuilder setClassName(String className) {
            this.className = className;
            return this;
        }

        public EntityBuilder setTableName(String tableName) {
            this.tableName = tableName;
            return this;
        }

        public EntityBuilder setPackName(String packName) {
            this.packName = packName;
            return this;
        }

        public EntityBuilder setResultSet(ResultSet rs) {
            this.rs = rs;
            return this;
        }

        public EntityBuilder setOutPath(String outPath) {
            this.outPath = outPath;
            return this;
        }

        public EntityBuilder setInfoFactory(Class<? extends ITableInfoFactory> infoFactory) {
            this.infoFactory = infoFactory;
            return this;
        }

        public EntityBuilder setInfoFactoryInstance(ITableInfoFactory infoFactory) {
            this.infoFactoryInstance = infoFactory;
            return this;
        }

        public ITableInfoFactory getInfoFactoryInstance() {
            return infoFactoryInstance;
        }

        /**
         * 生成 java 文件
         *
         * @throws SQLException             resultSet 异常
         * @throws IOException              输出文件异常
         * @throws ConditionNotMetException 必要条件检查
         */
        public void generate() throws SQLException, IOException, ConditionNotMetException {
            paramCheck();
            StringBuilder data = this.generate(rs, className, tableName, packName);
            if (null == data) {
                throw new EntityBuildException("table:"+tableName+" 无生成数据");
            }
            String file = outPath + File.separator + className + ".java";
            try (FileOutputStream out = new FileOutputStream(file)) {
                out.write(data.toString().getBytes(StandardCharsets.UTF_8));
                out.flush();
            }
        }

        private void paramCheck() throws ConditionNotMetException {
            if (null == className) {
                throw new ConditionNotMetException("类名未指定");
            }
            if (null == tableName) {
                throw new ConditionNotMetException("表名未指定");
            }
            if (null == packName) {
                throw new ConditionNotMetException("包名未指定");
            }
            if (null == outPath) {
                throw new ConditionNotMetException("文件输出路径未指定");
            }
            if (!new File(outPath).exists()) {
                throw new ConditionNotMetException("文件输出路径不存在," + outPath);
            }
            if (null == rs) {
                throw new ConditionNotMetException("ResultSet 未指定");
            }
        }

        /**
         * 生成 实体对象
         *
         * @param rs        查询结果，select * from xxx limit 1;这样的语句的结果，并且需要每个列值不为空。
         * @param className 类名
         * @param tableName 表名
         * @param packName  全限定包名
         * @return 生成的文字符串形式的实体
         * @throws SQLException 查询过程中出现异常
         */
        public StringBuilder generate(ResultSet rs, String className, String tableName, String packName) throws SQLException {
            StringBuilder builderField = new StringBuilder();
            StringBuilder builderGetterSetter = new StringBuilder();
            StringBuilder builderToString = new StringBuilder();
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount() + 1;
            if (rs.next()) {
                TableInfo tableInfo = null;
                if (infoFactoryInstance != null) {
                    tableInfo = infoFactoryInstance.info(tableName);
                } else {
                    if (infoFactory != null) {
                        try {
                            ITableInfoFactory tableInfoFactory = infoFactory.newInstance();
                            tableInfo = tableInfoFactory.info(tableName);
                        } catch (InstantiationException | IllegalAccessException e) {
                            throw new ConditionNotMetException("创建表信息工厂失败", e);
                        }
                    }
                }
                String tableComment = null == tableInfo ? null : tableInfo.getTableComment();

                builderToString.append("\t@Override\n").append("\tpublic String toString() {\n\t\treturn \"").append(className).append("{\"+\n");
                for (int i = 1; i < columnCount; i++) {
                    String name = metaData.getColumnLabel(i);
                    TableColumnInfo columnInfo = null == tableInfo ? null : tableInfo.getColumns().get(name);
                    String columnComment = null == columnInfo ? null : columnInfo.getColumnComment();
                    boolean isKey = null != columnInfo && columnInfo.isKey();
                    boolean nullAble = null == columnInfo || columnInfo.isNullAble();
                    boolean autoIncrement = null != columnInfo && columnInfo.isAutoIncrement();

                    String fieldName = StrUtils.camel(name);
                    Object o = rs.getObject(name);
                    String type = null == o ? "java.lang.String" : o.getClass().getTypeName();
                    builderField.append("\t@TiColumn(value=\"").append(name).append("\"");
                    if (columnComment != null) {
                        builderField.append(",comment=\"").append(columnComment).append("\"");
                        if (!nullAble) {
                            builderField.append(",nullAble=").append("false");
                        }
                        if (isKey) {
                            builderField.append(",key=").append("true");
                        }
                        if (autoIncrement) {
                            builderField.append(",autoIncrement=").append("true");
                        }
                    }
                    builderField.append(")\n");
                    builderField.append("\tprivate ").append(type).append(" ").append(fieldName).append(";\n");
                    //setter
                    builderGetterSetter.append("\tpublic void set").append(StrUtils.capitalize(fieldName)).append("(");
                    builderGetterSetter.append(type).append(" ").append(fieldName);
                    builderGetterSetter.append("){");
                    builderGetterSetter.append("this.").append(fieldName).append("=").append(fieldName);
                    builderGetterSetter.append(";}\n\n");
                    //getter
                    builderGetterSetter.append("\tpublic ").append(type).append(" get").append(StrUtils.capitalize(fieldName)).append("(){");
                    builderGetterSetter.append("return this.").append(fieldName);
                    builderGetterSetter.append(";}\n\n");
                    //toString
                    builderToString.append("\t\t\t\"").append(fieldName).append("=\"+").append(fieldName).append(i > columnCount - 2 ? "" : "+\",\"").append("+\n");
                }
                builderToString.append("\t\t\"}\";").append("\n\t}");
                //class

                StringBuilder builderClass = new StringBuilder();
                builderClass.append("package ").append(packName).append(";\n\n");
                builderClass.append("import com.gitee.oeoe.tisimple.ability.TiEntity;\nimport com.gitee.oeoe.tisimple.annotation.TiColumn;\nimport com.gitee.oeoe.tisimple.annotation.TiTable;\n\n");
                builderClass.append("@TiTable(value=\"").append(tableName).append("\"");
                if (tableComment != null) {
                    builderClass.append(",comment=\"").append(tableComment).append("\"");
                }
                builderClass.append(")\n");
                builderClass.append("public class ").append(className).append(" extends TiEntity<").append(className).append("> {\n");
                builderClass.append(builderField);
                builderClass.append("\n");
                builderClass.append(builderGetterSetter);
                builderClass.append("\n");
                builderClass.append(builderToString);
                builderClass.append("\n}");
                return builderClass;
            }
            return null;
        }
    }
}
