package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.core.TiDBPoolManager;
import com.gitee.oeoe.tisimple.core.TiGlobalConfig;
import com.gitee.oeoe.tisimple.core.TiSimpleCore;
import com.gitee.oeoe.tisimple.exception.*;
import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.page.IPageFactory;
import com.gitee.oeoe.tisimple.page.IPageParam;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;
import com.gitee.oeoe.tisimple.transaction.ITxManager;
import com.gitee.oeoe.tisimple.transaction.TX;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 为实体提供操作表的进阶能力
 * <p>
 * Trustworthy
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/11
 */
public abstract class TiEntity<T extends TiEntity<T>> extends AbilityISQL implements Serializable, IBaseAbility<T>, ITemplateQueryAbility<T>, ITemplateDeleteAbility, ITemplateUpdateAbility, ITemplateInsertAbility<T>, IPageParam {

    /**
     * 当前实体的分页工厂
     */
    private transient IPageFactory pageFactory;

    /**
     * 当前实体的事务管理器
     */
    private transient ITxManager txManager;

    /**
     * 当前实体的私有数据库连接
     */
    private transient Connection connection;

    /**
     * 插入的时候返回的东西，一般是返回的数据库生成的主键
     */
    private transient Object returnKey;


    public Object getReturnKey() {
        return returnKey;
    }

    public void setReturnKey(Object returnKey) {
        this.returnKey = returnKey;
    }

    public IPageFactory getPageFactory() {
        if (null == this.pageFactory) {
            this.pageFactory = TiGlobalConfig.getGlobalPageFactory();
        }
        return this.pageFactory;
    }

    public void setPageFactory(IPageFactory pageFactory) {
        this.pageFactory = pageFactory;
    }

    public void setTxManager(ITxManager tx) {
        this.connection = tx.getConnection();
        this.txManager = tx;
    }

    public ITxManager getTxManager() {
        return txManager;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * 获取当前对象的连接
     *
     * @return Connection 一条连接
     * @throws ObtainConnectionException 获取连接异常
     */
    public Connection getConnection() {
        try {
            if (null == connection || !connection.isValid(3)) {
                this.connection = TiDBPoolManager.getConnection();
            }
            return this.connection;
        } catch (Exception e) {
            throw new ObtainConnectionException("获取连接失败", e);
        }
    }

    @Override
    public int insert() throws InsertException {
        ISQL template = insertISQL(this);
        try {
            TiSimpleCore.InsertResult result = TiSimpleCore.insert(getConnection(), template);
            if (result.getEffectRows() > -1) {
                this.setReturnKey(result.getReturnKey());
                for (Field field : this.getClass().getDeclaredFields()) {
                    TiColumn annotation = field.getDeclaredAnnotation(TiColumn.class);
                    if (null != annotation) {
                        boolean set = annotation.autoIncrement() && annotation.key() && !annotation.nullAble();
                        if (set) {
                            field.setAccessible(true);
                            try {
                                field.set(this, result.getReturnKey());
                            } catch (IllegalAccessException e) {
                                throw new InsertException("设置自增主键失败", e);
                            }
                        }
                    }
                }
            }
            return result.getEffectRows();
        } catch (SQLException e) {
            throw new InsertException("插入异常", e);
        }
    }


    @Override
    public int delete() throws DeleteException {
        ISQL template = deleteISQL(this);
        try {
            return TiSimpleCore.updateOrDelete(getConnection(), template);
        } catch (SQLException e) {
            throw new InsertException("删除异常", e);
        }
    }

    @Override
    public int update(T entity) throws UpdateException {
        ISQL template = updateISQL(this, entity);
        try {
            return TiSimpleCore.updateOrDelete(getConnection(), template);
        } catch (SQLException e) {
            throw new InsertException("更新异常", e);
        }
    }

    @Override
    public IPage<T> page() throws QueryException {
        IPageFactory pageFactory = getPageFactory();
        if (null == pageFactory) {
            return null;
        }
        ISQL template = queryISQL(this);
        if (null == template) {
            return null;
        }
        return page(template);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> query() throws QueryException {
        ISQL template = queryISQL(this);
        if (null == template) {
            return new ArrayList<>();
        }
        try {
            List<Map<String, Object>> ls = TiSimpleCore.queryList(getConnection(), template);
            return TiSimpleCore.toEntityList((Class<T>) this.getClass(), ls);
        } catch (Exception e) {
            throw new InsertException("查询异常", e);
        }
    }

    @Override
    public T onlyOne() throws QueryException {
        List<T> list = query();
        if (list.size() > 1) {
            throw new QueryException("满足该条件的记录不唯一");
        }
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public Long count() throws QueryException {
        String selectName = "num";
        ISQL sqlTemplate = countISQL(this, selectName);
        if (null == sqlTemplate) {
            return 0L;
        }
        try {
            Map<String, Object> map = TiSimpleCore.queryOne(getConnection(), sqlTemplate);
            Object o = map.get(selectName);
            if (null == o) {
                o=map.get(selectName.toUpperCase());
            }
            if (null != o) {
                return Long.parseLong(String.valueOf(o));
            }
            return null;
        } catch (Exception e) {
            throw new QueryException("查询异常", e);
        }
    }

    @Override
    public List<Map<String, Object>> queryMap(String sqlTemplate) throws QueryException {
        return this.queryMap(SqlFactory.sql(sqlTemplate, this));
    }

    @Override
    public List<Map<String, Object>> queryMap(ISQL sqlTemplate) throws QueryException {
        try (Connection conn = this.getConnection()) {
            return TiSimpleCore.queryList(conn, sqlTemplate);
        } catch (Exception e) {
            throw new QueryException(e.getMessage(), e);
        }
    }

    @Override
    public List<T> queryList(String sqlTemplate) throws QueryException {
        return this.queryList(SqlFactory.sql(sqlTemplate, this));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> queryList(ISQL sqlTemplate) throws QueryException {
        try {
            List<Map<String, Object>> maps = TiSimpleCore.queryList(getConnection(), sqlTemplate);
            return (List<T>) TiSimpleCore.toEntityList(this.getClass(), maps);
        } catch (Exception e) {
            throw new QueryException(e.getMessage(), e);
        }
    }

    @Override
    public T queryOne(String sqlTemplate) throws QueryException {
        return this.queryOne(SqlFactory.sql(sqlTemplate, this));
    }

    @SuppressWarnings("unchecked")
    @Override
    public T queryOne(ISQL sqlTemplate) throws QueryException {
        try {
            Map<String, Object> map = TiSimpleCore.queryOne(getConnection(), sqlTemplate);
            return (T) TiSimpleCore.toEntityOne(this.getClass(), map);
        } catch (Exception e) {
            throw new QueryException(e.getMessage(), e);
        }
    }

    @Override
    public Long queryCount(String sqlTemplate, String selectName) throws QueryException {
        return this.queryCount(SqlFactory.sql(sqlTemplate, this), selectName);
    }

    @Override
    public Long queryCount(ISQL sqlTemplate, String selectName) throws QueryException {
        try {
            Map<String, Object> map = TiSimpleCore.queryOne(getConnection(), sqlTemplate);
            Object o = map.get(selectName);
            if (null == o) {
                o=map.get(selectName.toUpperCase());
            }
            if (null != o) {
                return Long.parseLong(String.valueOf(o));
            }
            return null;
        } catch (Exception e) {
            throw new QueryException("查询异常", e);
        }
    }

    @Override
    public IPage<T> page(String sql) throws QueryException {
        return page(SqlFactory.sql(sql, this));
    }

    @Override
    public IPage<T> page(ISQL sqlTemplate) throws QueryException {
        IPageFactory pageFactory = this.getPageFactory();
        if (null == pageFactory) {
            return null;
        }
        ISQL countSql = pageFactory.countSql(sqlTemplate);
        String selectName = pageFactory.countSqlSelectName();
        if (selectName == null) {
            return null;
        }
        int pageIndex = pageFactory.getPageIndex(sqlTemplate);
        int pageSize = pageFactory.getPageSize(sqlTemplate);

        Long maxRowCount = this.queryCount(countSql, selectName);
        if (maxRowCount == null || maxRowCount == 0) {
            return pageFactory.assemble(new ArrayList<>(), maxRowCount, pageSize, pageIndex);
        }
        ISQL pageSql = pageFactory.pageSql(sqlTemplate);
        List<T> data = this.queryList(pageSql);
        return pageFactory.assemble(data, maxRowCount, pageSize, pageIndex);
    }

    @Override
    public int delete(String sql) throws DeleteException {
        return this.delete(SqlFactory.sql(sql, this));
    }

    @Override
    public int delete(ISQL template) throws DeleteException {
        try {
            return TiSimpleCore.updateOrDelete(getConnection(), template);
        } catch (Exception e) {
            throw new QueryException("删除异常", e);
        }
    }

    @Override
    public int update(String sql) throws DeleteException {
        return this.update(SqlFactory.sql(sql, this));
    }

    @Override
    public int update(ISQL template) throws DeleteException {
        try {
            return TiSimpleCore.updateOrDelete(getConnection(), template);
        } catch (Exception e) {
            throw new QueryException("更新异常", e);
        }
    }

    @Override
    public Integer pageSize() {
        IPageFactory pageFactory = getPageFactory();
        if (null == pageFactory) {
            return null;
        }
        return pageFactory.pageSize();
    }

    @Override
    public Integer pageIndex() {
        IPageFactory pageFactory = getPageFactory();
        if (null == pageFactory) {
            return null;
        }
        return pageFactory.pageIndex();
    }


    @Override
    public void pageSize(Integer size) {
        IPageFactory pageFactory = getPageFactory();
        if (null != pageFactory) {
            pageFactory.pageSize(size);
        }
    }

    @Override
    public void pageIndex(Integer size) {
        IPageFactory pageFactory = getPageFactory();
        if (null != pageFactory) {
            pageFactory.pageIndex(size);
        }

    }

    /**
     * 新增
     *
     * @param sql 使用模版语法的sql语句
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    @Override
    public int insert(String sql) throws InsertException {
        return insert(SqlFactory.sql(sql, this));
    }

    /**
     * 新增
     *
     * @param template sql模版对象
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    @Override
    public int insert(ISQL template) throws InsertException {
        try {
            TiSimpleCore.InsertResult insert = TiSimpleCore.insert(getConnection(), template);
            return insert.getEffectRows();
        } catch (Exception e) {
            throw new InsertException("新增异常", e);
        }
    }


    /**
     * 新增-多记录-高级-批量模式，主键若指定了生成器将自动赋值，或指定为自增，也会自动赋值
     * <p>
     * 将单独成一个事务进行批量插入，为了在底层插入上保证事务特性，上层应用应该将大数据拆分为小块调用此方法，
     * 如果数据量太大可能会导致无法插入，并一直阻塞。
     * 建议单次插入不超过10000条
     *
     * @param entities 列表对象
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    @Override
    public int insert(List<T> entities) throws InsertException {
        List<ISQL> templates = insertISQL(entities);
        ITxManager txManager = TX.open().join(this);
        try (ITxManager tx = txManager) {
            TiSimpleCore.InsertResult[] result = TiSimpleCore.insert(getConnection(), templates);
            if (result.length != entities.size()) {
                tx.rollback();//存在没有插入成功的，全部回滚
                return -1;
            }
            for (int i = 0; i < entities.size(); i++) {
                entities.get(i).setReturnKey(result[i].getReturnKey());
            }
            for (Field field : this.getClass().getDeclaredFields()) {
                TiColumn annotation = field.getDeclaredAnnotation(TiColumn.class);
                if (null != annotation) {
                    boolean set = annotation.autoIncrement() && annotation.key() && !annotation.nullAble();
                    if (set) {
                        field.setAccessible(true);
                        try {
                            for (int i = 0; i < result.length; i++) {
                                int ef = result[i].getEffectRows();
                                Object id = result[i].getReturnKey();
                                if (ef > 0 && id !=null) {
                                    TiEntity<T> en = entities.get(i);
                                    field.set(en, id);
                                }
                            }
                        } catch (IllegalAccessException e) {
                            throw new InsertException("设置自增主键失败", e);
                        }
                    }
                }
            }
            tx.commit();
            int success = 0;
            for (TiSimpleCore.InsertResult ints : result) {
                success += ints.getEffectRows();
            }
            return success;
        } catch (Exception e) {
            txManager.rollback();
            throw new InsertException("批量插入失败", e);
        }
    }
}
