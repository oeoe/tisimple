package com.gitee.oeoe.tisimple.exception;

/**
 * 包装异常类-插入异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class InsertException extends RuntimeException {
    public InsertException(String message) {
        super(message);
    }

    public InsertException(String message, Throwable cause) {
        super(message, cause);
    }
}
