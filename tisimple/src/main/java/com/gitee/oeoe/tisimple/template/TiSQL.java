package com.gitee.oeoe.tisimple.template;

import com.gitee.oeoe.tisimple.exception.SqlFunException;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * sql 模板引擎
 * <p>
 * 模版函数调用规则：一个模版函数调用以[开头，以]结束，紧跟[之后的funName，表示要调用的模版方法，@与#之间的表示主参数，是参数空间中的属性，#之后的以|分隔为副参数
 *
 * @author zhu qiang
 * @see SqlFunc
 * @since 1.8+ 2022/1/23
 */
public class TiSQL implements ISQL {

    private static final String vav = "_\\$@\\$_";

    private static final String rgxQ = "\\?";

    private static final Pattern compileQ = Pattern.compile(rgxQ);

    private static final String rgxP = "^\\{(.*?)}$";

    private static final Pattern compileP = Pattern.compile(rgxP);

    private static final String rgxM = "\\{(.*?)}";

    private static final Pattern compileM = Pattern.compile(rgxM);

    private static final String rgxN = "\\{#(.*?)}";

    private static final Pattern compileN = Pattern.compile(rgxN);

    private static final Pattern funPattern = Pattern.compile("\\[(.+?)@(.+?)#(.*)]");//foreach@name@(|，|)

    //模版参数方法空间
    private static final Map<String, SqlFunc> fun = new LinkedHashMap<>();

    //序列参数空间
    private final Map<Integer, Object> pv = new LinkedHashMap<>();

    //参数空间
    private final Map<Object, Object> nv = new HashMap<>();

    //解析后的sql
    private String parsedSql;

    //未解析的sql
    private String originSql;

    //未解析
    private boolean noAnalysis = true;

    //数据库知否支持自动生成key
    private boolean supportGeneratorKey = false;

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @return 模版SQL
     */
    @Override
    public ISQL SqlNew(String originSql) {
        TiSQL s = new TiSQL();
        s.originSql = originSql;
        return s;
    }

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param param     模版参数
     * @return 模版SQL
     */
    @Override
    public ISQL SqlNew(String originSql, Object param) {
        ISQL s = SqlNew(originSql);
        if (null == param) {
            return s;
        }
        if (param instanceof Map) {
            s.addParams((Map<?, ?>) param);
            return s;
        }
        if (param instanceof Collection<?>) {
            s.addParam("list", param);
            return s;
        }
        if (param instanceof Object[]) {
            Object[] p = (Object[]) param;
            for (int i = 0; i < p.length; i++) {
                s.addParam(String.valueOf(i), p[i]);
            }
            return s;
        }
        Class<?> pCls = param.getClass();
        if (CharSequence.class.isAssignableFrom(pCls) || Number.class.isAssignableFrom(pCls) || Boolean.class.isAssignableFrom(pCls)) {
            s.addParam("0", param);
            return s;
        }
        Field[] fields = param.getClass().getDeclaredFields();
        for (Field field : fields) {
            String name = field.getName();
            field.setAccessible(true);
            try {
                Object o = field.get(param);
                s.addParam(name, o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return s;
    }

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param params    模版参数
     * @return 模版SQL
     */
    @Override
    public ISQL SqlNew(String originSql, Object... params) {
        return SqlNew(originSql, (Object) params);
    }

    /**
     * 自定义添加参数到参数空间
     *
     * @param paramName  参数名
     * @param paramValue 参数值
     * @return 模版SQL
     */
    @Override
    public ISQL addParam(String paramName, Object paramValue) {
        if (null == paramValue) {
            return this;
        }
        this.nv.put(paramName, paramValue);
        return this;
    }

    /**
     * 自定义添加参数到参数空间
     *
     * @param params 参数键值对
     * @return 模版SQL
     */
    @Override
    public ISQL addParams(Map<?, ?> params) {
        if (null == params || params.isEmpty()) {
            return this;
        }
        this.nv.putAll(params);
        return this;
    }

    /**
     * 获取参数空间的参数
     *
     * @param paramName 参数名
     * @return 参数值
     */
    @Override
    public Object getParam(String paramName) {
        return this.nv.get(paramName);
    }

    /**
     * 获取原生的sql
     *
     * @return 未解析的sql
     */
    @Override
    public String getOriginSql() {
        return this.originSql;
    }

    /**
     * 获取解析后的sql
     *
     * @return 解析后的sql
     */
    @Override
    public String getParsedSql() {
        return this.noAnalysis ? parseSql() : parsedSql;
    }

    /**
     * 获取将参数与sql组合后的参考sql
     *
     * @return 组合参数后的参考sql
     */
    @Override
    public String getReferSql() {
        String sql = noAnalysis ? getParsedSql() : parsedSql;
        for (Integer index : pv.keySet()) {
            sql = sql.replaceFirst("\\?", "'" + pv.get(index) + "'");
        }
        return sql;
    }

    /**
     * 复制自身
     *
     * @return 新的模版SQL
     */
    @Override
    public ISQL copy() {
        TiSQL tp = new TiSQL();
        tp.parsedSql = this.parsedSql;
        tp.originSql = this.originSql;
        tp.noAnalysis = this.noAnalysis;
        tp.nv.putAll(this.nv);
        tp.pv.putAll(this.pv);
        return tp;
    }

    /**
     * 复制自身，替换未解析sql
     *
     * @param originSql 模版sql
     * @return 新的模版SQL
     */
    @Override
    public ISQL copy(String originSql) {
        TiSQL tp = new TiSQL();
        tp.originSql = originSql;
        tp.nv.putAll(this.nv);
        tp.pv.putAll(this.pv);
        return tp;
    }

    /**
     * 解析模版sql
     *
     * @return 解析后的sql
     */
    @Override
    public String parseSql() {
        this.pv.clear();//清空序列空间
        boolean isProc = false;
        Map<Object, Object> nv = this.nv;
        Map<Integer, Object> pv = this.pv;
        String tempSql = funAnalysis(this, this.originSql, this.nv);
        Matcher matcher;
        if ((matcher = compileP.matcher(tempSql)).find()) {
            tempSql = matcher.group(1).trim();
            isProc = true;
        }
        boolean has = true;
        int order = 0;
        int seq = 0;
        while (has) {
            if (has = (matcher = compileN.matcher(tempSql)).find()) {
                String key = matcher.group(1).trim();
                String vl = null == nv.get(key) ? "null" : String.valueOf(nv.get(key));
                tempSql = tempSql.replaceFirst(rgxN, vl);
                order++;
            } else if (has = (matcher = compileM.matcher(tempSql)).find()) {
                tempSql = tempSql.replaceFirst(rgxM, vav);
                String key = matcher.group(1).trim();
                pv.put(order++, null == nv.get(key) ? "null" : nv.get(key));
            } else if (has = (compileQ.matcher(tempSql)).find()) {
                tempSql = tempSql.replaceFirst(rgxQ, vav);
                String key = String.valueOf(seq++);
                pv.put(order++, null == nv.get(key) ? "null" : nv.get(key));
            }
        }

        tempSql = tempSql.replaceAll(vav, "?");
        this.parsedSql = isProc ? "{" + tempSql + "}" : tempSql;
        this.noAnalysis = false;
        return this.parsedSql;
    }

    @Override
    public boolean supportGeneratorKey() {
        return this.supportGeneratorKey;
    }

    @Override
    public ISQL supportGeneratorKey(boolean supports) {
        this.supportGeneratorKey=supports;
        return this;
    }

    /**
     * 组装参数
     *
     * @param statement 预编译语句
     * @return 模版SQL
     * @throws SQLException 设值异常
     */
    @Override
    public ISQL assemble(PreparedStatement statement) throws SQLException {
        for (Integer index : pv.keySet()) {
            statement.setObject(index + 1, pv.get(index));
        }
        return this;
    }

    /**
     * 注册模版函数
     *
     * @param funName 函数名
     * @param func    函数
     */
    @Override
    public <T extends SqlFunc> void registerFunc(String funName, Class<T> func) {
        try {
            fun.put(funName, func.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw new SqlFunException("注册模版方法失败", e);
        }
    }

    /**
     * 模版函数解析
     *
     * @param template 模版SQL
     * @param sql      sql
     * @param param    实际参数
     * @return 计算表达式后的值
     */
    private String funAnalysis(ISQL template, String sql, Map<Object, Object> param) {
        String tempSql = sql;
        int ed;
        while (tempSql.contains("[")) {
            if ((ed = tempSql.indexOf("]")) > -1) {
                int index3;
                if ((index3 = tempSql.lastIndexOf("[", ed)) > -1) {
                    String content = tempSql.substring(index3, ed + 1);
                    String gStr = funInvoke(template, content, param);
                    tempSql = tempSql.substring(0, index3) + gStr + tempSql.substring(ed + 1);
                }
            }
        }
        return tempSql;
    }

    /**
     * 模版函数分析调用
     *
     * @param template   模版SQL
     * @param expression 函数表达式
     * @param param      实际参数
     * @return 分析后的表达式
     */
    private String funInvoke(ISQL template, String expression, Map<Object, Object> param) {
        Matcher matcher;
        if ((matcher = funPattern.matcher(expression)).matches()) {
            String funName = matcher.group(1);
            String key = matcher.group(2);
            String pm = matcher.group(3);
            Object val = param.get(key);
            String[] pms = pm.split("\\|");
            SqlFunc sqlFunc = fun.get(funName);
            if (null == sqlFunc) {
                throw new SqlFunException(funName + " 未注册");
            }
            return sqlFunc.apply(template, val, pms);
        }
        return expression;
    }

    @Override
    public boolean equals(Object o) {
        if (noAnalysis) {
            parseSql();
        }
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TiSQL tiSQL = (TiSQL) o;
        if (tiSQL.noAnalysis) {
            tiSQL.parseSql();
        }
        return parsedSql.equals(tiSQL.parsedSql)&&supportGeneratorKey==tiSQL.supportGeneratorKey;
    }

    @Override
    public int hashCode() {
        if (noAnalysis) {
            parseSql();
        }
        return Objects.hash(parsedSql);
    }
}
