package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.exception.DeleteException;
import com.gitee.oeoe.tisimple.template.ISQL;

/**
 * 提供基于模版sql删除接口
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/9
 */
public interface ITemplateDeleteAbility {

    /**
     * 按给定的条件删除记录，实体本身的属性将放入到参数空间
     *
     * @param sql 使用模版语法的sql语句
     * @return 删除的数量
     * @throws DeleteException 删除中各个阶段出现异常
     */
    int delete(String sql) throws DeleteException;

    /**
     * 按给定的条件删除记录
     *
     * @param template sql模版对象
     * @return 删除的数量
     * @throws DeleteException 删除中各个阶段出现异常
     */

    int delete(ISQL template) throws DeleteException;
}