package com.gitee.oeoe.tisimple.template.fun;

import com.gitee.oeoe.tisimple.exception.SqlFunException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFunc;

import java.util.Collection;

/**
 * 三元判空，如果p为null，或p为字符串时为空，则返回第一个参数，否则返回第二参数，且所有副参数的类型必须为字符串
 * 模版语法，[null@name#and a=1|and a={name}]，例如 select*from xx where 1=1[null@name#and a=1|and a={name}]
 * <p>
 * 其中的null表示此方法注册名，@与#间的name是参数空间中的参数名，是主参数，#后面是副参数块，并用|分割为2个副参数，第一个副参数为 'and a=1'
 * 第二个副参数为 'and a={name}'
 * 它们都将被当成字符串处理。
 * <p>
 * 如果参数空间中name对应的对象为空或为null，或长度为0，则返回第一个副参数，否则返回第二个副参数。
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/8
 */
public class TernaryEmptyFunc implements SqlFunc {
    @Override
    public String apply(ISQL template, Object p, Object[] param) {
        if (param.length < 2) {
            throw new SqlFunException("参数个数不正确");
        }
        if (null == p) {
            return null == param[0] ? "" : (String) param[0];
        }
        if (p instanceof String) {
            if (((String) p).trim().length() < 1) {
                return (String) param[0];
            }
        }
        if (p instanceof Collection) {
            if (((Collection<?>) p).isEmpty()) {
                return (String) param[0];
            }
        }
        return (String) param[1];
    }
}
