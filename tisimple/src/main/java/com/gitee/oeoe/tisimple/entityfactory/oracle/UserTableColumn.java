package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value = "user_tab_columns")
public class UserTableColumn extends TiEntity<UserTableColumn> {
    @TiColumn(value = "TABLE_NAME")
    private String tableName;
    @TiColumn(value = "COLUMN_NAME")
    private String columnName;
    @TiColumn(value = "DATA_TYPE")
    private String dataType;
    @TiColumn(value = "DATA_TYPE_MOD")
    private String dataTypeMod;
    @TiColumn(value = "DATA_TYPE_OWNER")
    private String dataTypeOwner;
    @TiColumn(value = "DATA_LENGTH")
    private java.math.BigDecimal dataLength;
    @TiColumn(value = "DATA_PRECISION")
    private java.math.BigDecimal dataPrecision;
    @TiColumn(value = "DATA_SCALE")
    private java.math.BigDecimal dataScale;
    @TiColumn(value = "NULLABLE")
    private String nullable;
    @TiColumn(value = "COLUMN_ID")
    private java.math.BigDecimal columnId;
    @TiColumn(value = "DEFAULT_LENGTH")
    private java.math.BigDecimal defaultLength;
    @TiColumn(value = "DATA_DEFAULT")
    private String dataDefault;
    @TiColumn(value = "NUM_DISTINCT")
    private String numDistinct;
    @TiColumn(value = "LOW_VALUE")
    private String lowValue;
    @TiColumn(value = "HIGH_VALUE")
    private String highValue;
    @TiColumn(value = "DENSITY")
    private String density;
    @TiColumn(value = "NUM_NULLS")
    private String numNulls;
    @TiColumn(value = "NUM_BUCKETS")
    private String numBuckets;
    @TiColumn(value = "LAST_ANALYZED")
    private String lastAnalyzed;
    @TiColumn(value = "SAMPLE_SIZE")
    private String sampleSize;
    @TiColumn(value = "CHARACTER_SET_NAME")
    private String characterSetName;
    @TiColumn(value = "CHAR_COL_DECL_LENGTH")
    private java.math.BigDecimal charColDeclLength;
    @TiColumn(value = "GLOBAL_STATS")
    private String globalStats;
    @TiColumn(value = "USER_STATS")
    private String userStats;
    @TiColumn(value = "AVG_COL_LEN")
    private String avgColLen;
    @TiColumn(value = "CHAR_LENGTH")
    private java.math.BigDecimal charLength;
    @TiColumn(value = "CHAR_USED")
    private String charUsed;
    @TiColumn(value = "V80_FMT_IMAGE")
    private String v80FmtImage;
    @TiColumn(value = "DATA_UPGRADED")
    private String dataUpgraded;
    @TiColumn(value = "HISTOGRAM")
    private String histogram;
    @TiColumn(value = "DEFAULT_ON_NULL")
    private String defaultOnNull;
    @TiColumn(value = "IDENTITY_COLUMN")
    private String identityColumn;
    @TiColumn(value = "EVALUATION_EDITION")
    private String evaluationEdition;
    @TiColumn(value = "UNUSABLE_BEFORE")
    private String unusableBefore;
    @TiColumn(value = "UNUSABLE_BEGINNING")
    private String unusableBeginning;
    @TiColumn(value = "COLLATION")
    private String collation;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataType() {
        return this.dataType;
    }

    public void setDataTypeMod(String dataTypeMod) {
        this.dataTypeMod = dataTypeMod;
    }

    public String getDataTypeMod() {
        return this.dataTypeMod;
    }

    public void setDataTypeOwner(String dataTypeOwner) {
        this.dataTypeOwner = dataTypeOwner;
    }

    public String getDataTypeOwner() {
        return this.dataTypeOwner;
    }

    public void setDataLength(java.math.BigDecimal dataLength) {
        this.dataLength = dataLength;
    }

    public java.math.BigDecimal getDataLength() {
        return this.dataLength;
    }

    public void setDataPrecision(java.math.BigDecimal dataPrecision) {
        this.dataPrecision = dataPrecision;
    }

    public java.math.BigDecimal getDataPrecision() {
        return this.dataPrecision;
    }

    public void setDataScale(java.math.BigDecimal dataScale) {
        this.dataScale = dataScale;
    }

    public java.math.BigDecimal getDataScale() {
        return this.dataScale;
    }

    public void setNullable(String nullable) {
        this.nullable = nullable;
    }

    public String getNullable() {
        return this.nullable;
    }

    public void setColumnId(java.math.BigDecimal columnId) {
        this.columnId = columnId;
    }

    public java.math.BigDecimal getColumnId() {
        return this.columnId;
    }

    public void setDefaultLength(java.math.BigDecimal defaultLength) {
        this.defaultLength = defaultLength;
    }

    public java.math.BigDecimal getDefaultLength() {
        return this.defaultLength;
    }

    public void setDataDefault(String dataDefault) {
        this.dataDefault = dataDefault;
    }

    public String getDataDefault() {
        return this.dataDefault;
    }

    public void setNumDistinct(String numDistinct) {
        this.numDistinct = numDistinct;
    }

    public String getNumDistinct() {
        return this.numDistinct;
    }

    public void setLowValue(String lowValue) {
        this.lowValue = lowValue;
    }

    public String getLowValue() {
        return this.lowValue;
    }

    public void setHighValue(String highValue) {
        this.highValue = highValue;
    }

    public String getHighValue() {
        return this.highValue;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getDensity() {
        return this.density;
    }

    public void setNumNulls(String numNulls) {
        this.numNulls = numNulls;
    }

    public String getNumNulls() {
        return this.numNulls;
    }

    public void setNumBuckets(String numBuckets) {
        this.numBuckets = numBuckets;
    }

    public String getNumBuckets() {
        return this.numBuckets;
    }

    public void setLastAnalyzed(String lastAnalyzed) {
        this.lastAnalyzed = lastAnalyzed;
    }

    public String getLastAnalyzed() {
        return this.lastAnalyzed;
    }

    public void setSampleSize(String sampleSize) {
        this.sampleSize = sampleSize;
    }

    public String getSampleSize() {
        return this.sampleSize;
    }

    public void setCharacterSetName(String characterSetName) {
        this.characterSetName = characterSetName;
    }

    public String getCharacterSetName() {
        return this.characterSetName;
    }

    public void setCharColDeclLength(java.math.BigDecimal charColDeclLength) {
        this.charColDeclLength = charColDeclLength;
    }

    public java.math.BigDecimal getCharColDeclLength() {
        return this.charColDeclLength;
    }

    public void setGlobalStats(String globalStats) {
        this.globalStats = globalStats;
    }

    public String getGlobalStats() {
        return this.globalStats;
    }

    public void setUserStats(String userStats) {
        this.userStats = userStats;
    }

    public String getUserStats() {
        return this.userStats;
    }

    public void setAvgColLen(String avgColLen) {
        this.avgColLen = avgColLen;
    }

    public String getAvgColLen() {
        return this.avgColLen;
    }

    public void setCharLength(java.math.BigDecimal charLength) {
        this.charLength = charLength;
    }

    public java.math.BigDecimal getCharLength() {
        return this.charLength;
    }

    public void setCharUsed(String charUsed) {
        this.charUsed = charUsed;
    }

    public String getCharUsed() {
        return this.charUsed;
    }

    public void setV80FmtImage(String v80FmtImage) {
        this.v80FmtImage = v80FmtImage;
    }

    public String getV80FmtImage() {
        return this.v80FmtImage;
    }

    public void setDataUpgraded(String dataUpgraded) {
        this.dataUpgraded = dataUpgraded;
    }

    public String getDataUpgraded() {
        return this.dataUpgraded;
    }

    public void setHistogram(String histogram) {
        this.histogram = histogram;
    }

    public String getHistogram() {
        return this.histogram;
    }

    public void setDefaultOnNull(String defaultOnNull) {
        this.defaultOnNull = defaultOnNull;
    }

    public String getDefaultOnNull() {
        return this.defaultOnNull;
    }

    public void setIdentityColumn(String identityColumn) {
        this.identityColumn = identityColumn;
    }

    public String getIdentityColumn() {
        return this.identityColumn;
    }

    public void setEvaluationEdition(String evaluationEdition) {
        this.evaluationEdition = evaluationEdition;
    }

    public String getEvaluationEdition() {
        return this.evaluationEdition;
    }

    public void setUnusableBefore(String unusableBefore) {
        this.unusableBefore = unusableBefore;
    }

    public String getUnusableBefore() {
        return this.unusableBefore;
    }

    public void setUnusableBeginning(String unusableBeginning) {
        this.unusableBeginning = unusableBeginning;
    }

    public String getUnusableBeginning() {
        return this.unusableBeginning;
    }

    public void setCollation(String collation) {
        this.collation = collation;
    }

    public String getCollation() {
        return this.collation;
    }


    @Override
    public String toString() {
        return "UserTabColumns{" +
                "tableName=" + tableName + "," +
                "columnName=" + columnName + "," +
                "dataType=" + dataType + "," +
                "dataTypeMod=" + dataTypeMod + "," +
                "dataTypeOwner=" + dataTypeOwner + "," +
                "dataLength=" + dataLength + "," +
                "dataPrecision=" + dataPrecision + "," +
                "dataScale=" + dataScale + "," +
                "nullable=" + nullable + "," +
                "columnId=" + columnId + "," +
                "defaultLength=" + defaultLength + "," +
                "dataDefault=" + dataDefault + "," +
                "numDistinct=" + numDistinct + "," +
                "lowValue=" + lowValue + "," +
                "highValue=" + highValue + "," +
                "density=" + density + "," +
                "numNulls=" + numNulls + "," +
                "numBuckets=" + numBuckets + "," +
                "lastAnalyzed=" + lastAnalyzed + "," +
                "sampleSize=" + sampleSize + "," +
                "characterSetName=" + characterSetName + "," +
                "charColDeclLength=" + charColDeclLength + "," +
                "globalStats=" + globalStats + "," +
                "userStats=" + userStats + "," +
                "avgColLen=" + avgColLen + "," +
                "charLength=" + charLength + "," +
                "charUsed=" + charUsed + "," +
                "v80FmtImage=" + v80FmtImage + "," +
                "dataUpgraded=" + dataUpgraded + "," +
                "histogram=" + histogram + "," +
                "defaultOnNull=" + defaultOnNull + "," +
                "identityColumn=" + identityColumn + "," +
                "evaluationEdition=" + evaluationEdition + "," +
                "unusableBefore=" + unusableBefore + "," +
                "unusableBeginning=" + unusableBeginning + "," +
                "collation=" + collation +
                "}";
    }
}