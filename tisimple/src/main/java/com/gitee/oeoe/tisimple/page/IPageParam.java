package com.gitee.oeoe.tisimple.page;

/**
 * 工厂，实体间的分页参数桥梁
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/11
 */
public interface IPageParam {

    /**
     * 获取分页大小
     *
     * @return 分页大小
     */
    Integer pageSize();

    /**
     * 获取当前分页编号，分页从1开始
     *
     * @return 分页编号
     */
    Integer pageIndex();

    /**
     * 设置分页大小
     *
     * @param size 分页大小
     */
    void pageSize(Integer size);

    /**
     * 设置当前页编号
     *
     * @param page 当前页
     */
    void pageIndex(Integer page);

}
