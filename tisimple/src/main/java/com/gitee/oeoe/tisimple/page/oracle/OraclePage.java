package com.gitee.oeoe.tisimple.page.oracle;


import com.gitee.oeoe.tisimple.page.IPage;

import java.util.List;

/**
 * oracle 数据库 分页结果
 *
 * @param <T> 实体类型
 * @author zhu qiang
 * @see OraclePageFactory
 * @since 1.8+ 2022/2/7
 */
public final class OraclePage<T> implements IPage<T> {
    private final Long total;
    private final Integer pageSize;
    private final Integer currentPage;
    private final List<T> data;

    public OraclePage(Long total, Integer pageSize, Integer currentPage, List<T> data) {
        this.total = total;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.data = data;
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public Integer getPageSize() {
        return this.pageSize;
    }

    @Override
    public Integer getPageIndex() {
        return this.currentPage;
    }

    public Integer pageSize() {
        return pageSize;
    }

    public Integer currentPage() {
        return currentPage;
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "OraclePage{" +
                "pageSize=" + pageSize +
                ", currentPage=" + currentPage +
                ", data=" + data +
                '}';
    }
}
