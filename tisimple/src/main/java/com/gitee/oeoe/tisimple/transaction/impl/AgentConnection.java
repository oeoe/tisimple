package com.gitee.oeoe.tisimple.transaction.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
/**
 *  代理连接，将关闭连接的方法委托到事务管理中
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/15
 */
public class AgentConnection implements InvocationHandler {

    private final Connection connection;

    public AgentConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("close")) {
            return null;
        }
        return method.invoke(connection, args);
    }
}
