package com.gitee.oeoe.tisimple.exception;

/**
 * SQL的参数异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class ParamSqlException extends Exception {
    public ParamSqlException(String message) {
        super(message);
    }

    public ParamSqlException(String message, Throwable cause) {
        super(message, cause);
    }
}
