package com.gitee.oeoe.tisimple.template;

import com.gitee.oeoe.tisimple.exception.SqlTemplateException;
import com.gitee.oeoe.tisimple.template.fun.ForeachFunc;
import com.gitee.oeoe.tisimple.template.fun.TernaryEmptyFunc;

/**
 * sql 模板引擎
 * 模版函数调用规则：一个模版函数调用以[开头，以]结束，紧跟[之后的funName，表示要调用的模版方法，@与#之间的表示主参数，是参数空间中的属性，#之后的以|分隔为副参数
 *
 * @author zhu qiang
 * @see SqlFunc
 * @since 1.8+ 2022/1/8
 */
public class SqlFactory {
    private static ISQL originEngine;//默认模版

    static {
        use(TiSQL.class);
        registerFunc("null", TernaryEmptyFunc.class);//空判断三元表达式
        registerFunc("foreach", ForeachFunc.class);//循环迭代表达式
    }

    /**
     * 设置模版引擎
     *
     * @param sqlCls 模版引擎类
     */
    public static void use(Class<? extends ISQL> sqlCls) {
        try {
            originEngine = sqlCls.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new SqlTemplateException("创建SQL模版失败。", e);
        }
    }

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @return 模版SQL
     */
    public static ISQL sql(String originSql) {
        return originEngine.SqlNew(originSql);
    }

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param param     模版参数
     * @return 模版SQL
     */
    public static ISQL sql(String originSql, Object param) {
        return originEngine.SqlNew(originSql, param);
    }

    /**
     * 创建一个新的SQL模版
     *
     * @param originSql sql语句
     * @param params    模版参数
     * @return 模版SQL
     */
    public static ISQL sql(String originSql, Object... params) {
        return originEngine.SqlNew(originSql, params);
    }

    /**
     * 注册模版函数
     *
     * @param funName 函数名
     * @param func    函数
     * @param <T>     函数类型
     */
    public static <T extends SqlFunc> void registerFunc(String funName, Class<T> func) {
        originEngine.registerFunc(funName, func);
    }
}
