package com.gitee.oeoe.tisimple.exception;

/**
 * 包装异常类-查询异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class QueryException extends RuntimeException {
    public QueryException(String message) {
        super(message);
    }

    public QueryException(String message, Throwable cause) {
        super(message, cause);
    }
}
