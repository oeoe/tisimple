package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.annotation.TiTable;

@TiTable(value="user_tables")
public class UserTable extends TiEntity<UserTable> {
	@TiColumn(value="TABLE_NAME")
	private String tableName;
	@TiColumn(value="TABLESPACE_NAME")
	private String tablespaceName;
	@TiColumn(value="CLUSTER_NAME")
	private String clusterName;
	@TiColumn(value="IOT_NAME")
	private String iotName;
	@TiColumn(value="STATUS")
	private String status;
	@TiColumn(value="PCT_FREE")
	private java.math.BigDecimal pctFree;
	@TiColumn(value="PCT_USED")
	private String pctUsed;
	@TiColumn(value="INI_TRANS")
	private java.math.BigDecimal iniTrans;
	@TiColumn(value="MAX_TRANS")
	private java.math.BigDecimal maxTrans;
	@TiColumn(value="INITIAL_EXTENT")
	private java.math.BigDecimal initialExtent;
	@TiColumn(value="NEXT_EXTENT")
	private java.math.BigDecimal nextExtent;
	@TiColumn(value="MIN_EXTENTS")
	private java.math.BigDecimal minExtents;
	@TiColumn(value="MAX_EXTENTS")
	private java.math.BigDecimal maxExtents;
	@TiColumn(value="PCT_INCREASE")
	private String pctIncrease;
	@TiColumn(value="FREELISTS")
	private String freelists;
	@TiColumn(value="FREELIST_GROUPS")
	private String freelistGroups;
	@TiColumn(value="LOGGING")
	private String logging;
	@TiColumn(value="BACKED_UP")
	private String backedUp;
	@TiColumn(value="NUM_ROWS")
	private String numRows;
	@TiColumn(value="BLOCKS")
	private String blocks;
	@TiColumn(value="EMPTY_BLOCKS")
	private String emptyBlocks;
	@TiColumn(value="AVG_SPACE")
	private String avgSpace;
	@TiColumn(value="CHAIN_CNT")
	private String chainCnt;
	@TiColumn(value="AVG_ROW_LEN")
	private String avgRowLen;
	@TiColumn(value="AVG_SPACE_FREELIST_BLOCKS")
	private String avgSpaceFreelistBlocks;
	@TiColumn(value="NUM_FREELIST_BLOCKS")
	private String numFreelistBlocks;
	@TiColumn(value="DEGREE")
	private String degree;
	@TiColumn(value="INSTANCES")
	private String instances;
	@TiColumn(value="CACHE")
	private String cache;
	@TiColumn(value="TABLE_LOCK")
	private String tableLock;
	@TiColumn(value="SAMPLE_SIZE")
	private String sampleSize;
	@TiColumn(value="LAST_ANALYZED")
	private String lastAnalyzed;
	@TiColumn(value="PARTITIONED")
	private String partitioned;
	@TiColumn(value="IOT_TYPE")
	private String iotType;
	@TiColumn(value="TEMPORARY")
	private String temporary;
	@TiColumn(value="SECONDARY")
	private String secondary;
	@TiColumn(value="NESTED")
	private String nested;
	@TiColumn(value="BUFFER_POOL")
	private String bufferPool;
	@TiColumn(value="FLASH_CACHE")
	private String flashCache;
	@TiColumn(value="CELL_FLASH_CACHE")
	private String cellFlashCache;
	@TiColumn(value="ROW_MOVEMENT")
	private String rowMovement;
	@TiColumn(value="GLOBAL_STATS")
	private String globalStats;
	@TiColumn(value="USER_STATS")
	private String userStats;
	@TiColumn(value="DURATION")
	private String duration;
	@TiColumn(value="SKIP_CORRUPT")
	private String skipCorrupt;
	@TiColumn(value="MONITORING")
	private String monitoring;
	@TiColumn(value="CLUSTER_OWNER")
	private String clusterOwner;
	@TiColumn(value="DEPENDENCIES")
	private String dependencies;
	@TiColumn(value="COMPRESSION")
	private String compression;
	@TiColumn(value="COMPRESS_FOR")
	private String compressFor;
	@TiColumn(value="DROPPED")
	private String dropped;
	@TiColumn(value="READ_ONLY")
	private String readOnly;
	@TiColumn(value="SEGMENT_CREATED")
	private String segmentCreated;
	@TiColumn(value="RESULT_CACHE")
	private String resultCache;
	@TiColumn(value="CLUSTERING")
	private String clustering;
	@TiColumn(value="ACTIVITY_TRACKING")
	private String activityTracking;
	@TiColumn(value="DML_TIMESTAMP")
	private String dmlTimestamp;
	@TiColumn(value="HAS_IDENTITY")
	private String hasIdentity;
	@TiColumn(value="CONTAINER_DATA")
	private String containerData;
	@TiColumn(value="INMEMORY")
	private String inmemory;
	@TiColumn(value="INMEMORY_PRIORITY")
	private String inmemoryPriority;
	@TiColumn(value="INMEMORY_DISTRIBUTE")
	private String inmemoryDistribute;
	@TiColumn(value="INMEMORY_COMPRESSION")
	private String inmemoryCompression;
	@TiColumn(value="INMEMORY_DUPLICATE")
	private String inmemoryDuplicate;
	@TiColumn(value="DEFAULT_COLLATION")
	private String defaultCollation;
	@TiColumn(value="DUPLICATED")
	private String duplicated;
	@TiColumn(value="SHARDED")
	private String sharded;
	@TiColumn(value="EXTERNALLY_SHARDED")
	private String externallySharded;
	@TiColumn(value="EXTERNALLY_DUPLICATED")
	private String externallyDuplicated;
	@TiColumn(value="EXTERNAL")
	private String external;
	@TiColumn(value="HYBRID")
	private String hybrid;
	@TiColumn(value="CELLMEMORY")
	private String cellmemory;
	@TiColumn(value="CONTAINERS_DEFAULT")
	private String containersDefault;
	@TiColumn(value="CONTAINER_MAP")
	private String containerMap;
	@TiColumn(value="EXTENDED_DATA_LINK")
	private String extendedDataLink;
	@TiColumn(value="EXTENDED_DATA_LINK_MAP")
	private String extendedDataLinkMap;
	@TiColumn(value="INMEMORY_SERVICE")
	private String inmemoryService;
	@TiColumn(value="INMEMORY_SERVICE_NAME")
	private String inmemoryServiceName;
	@TiColumn(value="CONTAINER_MAP_OBJECT")
	private String containerMapObject;
	@TiColumn(value="MEMOPTIMIZE_READ")
	private String memoptimizeRead;
	@TiColumn(value="MEMOPTIMIZE_WRITE")
	private String memoptimizeWrite;
	@TiColumn(value="HAS_SENSITIVE_COLUMN")
	private String hasSensitiveColumn;
	@TiColumn(value="ADMIT_NULL")
	private String admitNull;
	@TiColumn(value="DATA_LINK_DML_ENABLED")
	private String dataLinkDmlEnabled;
	@TiColumn(value="LOGICAL_REPLICATION")
	private String logicalReplication;

	public void setTableName(String tableName){this.tableName=tableName;}

	public String getTableName(){return this.tableName;}

	public void setTablespaceName(String tablespaceName){this.tablespaceName=tablespaceName;}

	public String getTablespaceName(){return this.tablespaceName;}

	public void setClusterName(String clusterName){this.clusterName=clusterName;}

	public String getClusterName(){return this.clusterName;}

	public void setIotName(String iotName){this.iotName=iotName;}

	public String getIotName(){return this.iotName;}

	public void setStatus(String status){this.status=status;}

	public String getStatus(){return this.status;}

	public void setPctFree(java.math.BigDecimal pctFree){this.pctFree=pctFree;}

	public java.math.BigDecimal getPctFree(){return this.pctFree;}

	public void setPctUsed(String pctUsed){this.pctUsed=pctUsed;}

	public String getPctUsed(){return this.pctUsed;}

	public void setIniTrans(java.math.BigDecimal iniTrans){this.iniTrans=iniTrans;}

	public java.math.BigDecimal getIniTrans(){return this.iniTrans;}

	public void setMaxTrans(java.math.BigDecimal maxTrans){this.maxTrans=maxTrans;}

	public java.math.BigDecimal getMaxTrans(){return this.maxTrans;}

	public void setInitialExtent(java.math.BigDecimal initialExtent){this.initialExtent=initialExtent;}

	public java.math.BigDecimal getInitialExtent(){return this.initialExtent;}

	public void setNextExtent(java.math.BigDecimal nextExtent){this.nextExtent=nextExtent;}

	public java.math.BigDecimal getNextExtent(){return this.nextExtent;}

	public void setMinExtents(java.math.BigDecimal minExtents){this.minExtents=minExtents;}

	public java.math.BigDecimal getMinExtents(){return this.minExtents;}

	public void setMaxExtents(java.math.BigDecimal maxExtents){this.maxExtents=maxExtents;}

	public java.math.BigDecimal getMaxExtents(){return this.maxExtents;}

	public void setPctIncrease(String pctIncrease){this.pctIncrease=pctIncrease;}

	public String getPctIncrease(){return this.pctIncrease;}

	public void setFreelists(String freelists){this.freelists=freelists;}

	public String getFreelists(){return this.freelists;}

	public void setFreelistGroups(String freelistGroups){this.freelistGroups=freelistGroups;}

	public String getFreelistGroups(){return this.freelistGroups;}

	public void setLogging(String logging){this.logging=logging;}

	public String getLogging(){return this.logging;}

	public void setBackedUp(String backedUp){this.backedUp=backedUp;}

	public String getBackedUp(){return this.backedUp;}

	public void setNumRows(String numRows){this.numRows=numRows;}

	public String getNumRows(){return this.numRows;}

	public void setBlocks(String blocks){this.blocks=blocks;}

	public String getBlocks(){return this.blocks;}

	public void setEmptyBlocks(String emptyBlocks){this.emptyBlocks=emptyBlocks;}

	public String getEmptyBlocks(){return this.emptyBlocks;}

	public void setAvgSpace(String avgSpace){this.avgSpace=avgSpace;}

	public String getAvgSpace(){return this.avgSpace;}

	public void setChainCnt(String chainCnt){this.chainCnt=chainCnt;}

	public String getChainCnt(){return this.chainCnt;}

	public void setAvgRowLen(String avgRowLen){this.avgRowLen=avgRowLen;}

	public String getAvgRowLen(){return this.avgRowLen;}

	public void setAvgSpaceFreelistBlocks(String avgSpaceFreelistBlocks){this.avgSpaceFreelistBlocks=avgSpaceFreelistBlocks;}

	public String getAvgSpaceFreelistBlocks(){return this.avgSpaceFreelistBlocks;}

	public void setNumFreelistBlocks(String numFreelistBlocks){this.numFreelistBlocks=numFreelistBlocks;}

	public String getNumFreelistBlocks(){return this.numFreelistBlocks;}

	public void setDegree(String degree){this.degree=degree;}

	public String getDegree(){return this.degree;}

	public void setInstances(String instances){this.instances=instances;}

	public String getInstances(){return this.instances;}

	public void setCache(String cache){this.cache=cache;}

	public String getCache(){return this.cache;}

	public void setTableLock(String tableLock){this.tableLock=tableLock;}

	public String getTableLock(){return this.tableLock;}

	public void setSampleSize(String sampleSize){this.sampleSize=sampleSize;}

	public String getSampleSize(){return this.sampleSize;}

	public void setLastAnalyzed(String lastAnalyzed){this.lastAnalyzed=lastAnalyzed;}

	public String getLastAnalyzed(){return this.lastAnalyzed;}

	public void setPartitioned(String partitioned){this.partitioned=partitioned;}

	public String getPartitioned(){return this.partitioned;}

	public void setIotType(String iotType){this.iotType=iotType;}

	public String getIotType(){return this.iotType;}

	public void setTemporary(String temporary){this.temporary=temporary;}

	public String getTemporary(){return this.temporary;}

	public void setSecondary(String secondary){this.secondary=secondary;}

	public String getSecondary(){return this.secondary;}

	public void setNested(String nested){this.nested=nested;}

	public String getNested(){return this.nested;}

	public void setBufferPool(String bufferPool){this.bufferPool=bufferPool;}

	public String getBufferPool(){return this.bufferPool;}

	public void setFlashCache(String flashCache){this.flashCache=flashCache;}

	public String getFlashCache(){return this.flashCache;}

	public void setCellFlashCache(String cellFlashCache){this.cellFlashCache=cellFlashCache;}

	public String getCellFlashCache(){return this.cellFlashCache;}

	public void setRowMovement(String rowMovement){this.rowMovement=rowMovement;}

	public String getRowMovement(){return this.rowMovement;}

	public void setGlobalStats(String globalStats){this.globalStats=globalStats;}

	public String getGlobalStats(){return this.globalStats;}

	public void setUserStats(String userStats){this.userStats=userStats;}

	public String getUserStats(){return this.userStats;}

	public void setDuration(String duration){this.duration=duration;}

	public String getDuration(){return this.duration;}

	public void setSkipCorrupt(String skipCorrupt){this.skipCorrupt=skipCorrupt;}

	public String getSkipCorrupt(){return this.skipCorrupt;}

	public void setMonitoring(String monitoring){this.monitoring=monitoring;}

	public String getMonitoring(){return this.monitoring;}

	public void setClusterOwner(String clusterOwner){this.clusterOwner=clusterOwner;}

	public String getClusterOwner(){return this.clusterOwner;}

	public void setDependencies(String dependencies){this.dependencies=dependencies;}

	public String getDependencies(){return this.dependencies;}

	public void setCompression(String compression){this.compression=compression;}

	public String getCompression(){return this.compression;}

	public void setCompressFor(String compressFor){this.compressFor=compressFor;}

	public String getCompressFor(){return this.compressFor;}

	public void setDropped(String dropped){this.dropped=dropped;}

	public String getDropped(){return this.dropped;}

	public void setReadOnly(String readOnly){this.readOnly=readOnly;}

	public String getReadOnly(){return this.readOnly;}

	public void setSegmentCreated(String segmentCreated){this.segmentCreated=segmentCreated;}

	public String getSegmentCreated(){return this.segmentCreated;}

	public void setResultCache(String resultCache){this.resultCache=resultCache;}

	public String getResultCache(){return this.resultCache;}

	public void setClustering(String clustering){this.clustering=clustering;}

	public String getClustering(){return this.clustering;}

	public void setActivityTracking(String activityTracking){this.activityTracking=activityTracking;}

	public String getActivityTracking(){return this.activityTracking;}

	public void setDmlTimestamp(String dmlTimestamp){this.dmlTimestamp=dmlTimestamp;}

	public String getDmlTimestamp(){return this.dmlTimestamp;}

	public void setHasIdentity(String hasIdentity){this.hasIdentity=hasIdentity;}

	public String getHasIdentity(){return this.hasIdentity;}

	public void setContainerData(String containerData){this.containerData=containerData;}

	public String getContainerData(){return this.containerData;}

	public void setInmemory(String inmemory){this.inmemory=inmemory;}

	public String getInmemory(){return this.inmemory;}

	public void setInmemoryPriority(String inmemoryPriority){this.inmemoryPriority=inmemoryPriority;}

	public String getInmemoryPriority(){return this.inmemoryPriority;}

	public void setInmemoryDistribute(String inmemoryDistribute){this.inmemoryDistribute=inmemoryDistribute;}

	public String getInmemoryDistribute(){return this.inmemoryDistribute;}

	public void setInmemoryCompression(String inmemoryCompression){this.inmemoryCompression=inmemoryCompression;}

	public String getInmemoryCompression(){return this.inmemoryCompression;}

	public void setInmemoryDuplicate(String inmemoryDuplicate){this.inmemoryDuplicate=inmemoryDuplicate;}

	public String getInmemoryDuplicate(){return this.inmemoryDuplicate;}

	public void setDefaultCollation(String defaultCollation){this.defaultCollation=defaultCollation;}

	public String getDefaultCollation(){return this.defaultCollation;}

	public void setDuplicated(String duplicated){this.duplicated=duplicated;}

	public String getDuplicated(){return this.duplicated;}

	public void setSharded(String sharded){this.sharded=sharded;}

	public String getSharded(){return this.sharded;}

	public void setExternallySharded(String externallySharded){this.externallySharded=externallySharded;}

	public String getExternallySharded(){return this.externallySharded;}

	public void setExternallyDuplicated(String externallyDuplicated){this.externallyDuplicated=externallyDuplicated;}

	public String getExternallyDuplicated(){return this.externallyDuplicated;}

	public void setExternal(String external){this.external=external;}

	public String getExternal(){return this.external;}

	public void setHybrid(String hybrid){this.hybrid=hybrid;}

	public String getHybrid(){return this.hybrid;}

	public void setCellmemory(String cellmemory){this.cellmemory=cellmemory;}

	public String getCellmemory(){return this.cellmemory;}

	public void setContainersDefault(String containersDefault){this.containersDefault=containersDefault;}

	public String getContainersDefault(){return this.containersDefault;}

	public void setContainerMap(String containerMap){this.containerMap=containerMap;}

	public String getContainerMap(){return this.containerMap;}

	public void setExtendedDataLink(String extendedDataLink){this.extendedDataLink=extendedDataLink;}

	public String getExtendedDataLink(){return this.extendedDataLink;}

	public void setExtendedDataLinkMap(String extendedDataLinkMap){this.extendedDataLinkMap=extendedDataLinkMap;}

	public String getExtendedDataLinkMap(){return this.extendedDataLinkMap;}

	public void setInmemoryService(String inmemoryService){this.inmemoryService=inmemoryService;}

	public String getInmemoryService(){return this.inmemoryService;}

	public void setInmemoryServiceName(String inmemoryServiceName){this.inmemoryServiceName=inmemoryServiceName;}

	public String getInmemoryServiceName(){return this.inmemoryServiceName;}

	public void setContainerMapObject(String containerMapObject){this.containerMapObject=containerMapObject;}

	public String getContainerMapObject(){return this.containerMapObject;}

	public void setMemoptimizeRead(String memoptimizeRead){this.memoptimizeRead=memoptimizeRead;}

	public String getMemoptimizeRead(){return this.memoptimizeRead;}

	public void setMemoptimizeWrite(String memoptimizeWrite){this.memoptimizeWrite=memoptimizeWrite;}

	public String getMemoptimizeWrite(){return this.memoptimizeWrite;}

	public void setHasSensitiveColumn(String hasSensitiveColumn){this.hasSensitiveColumn=hasSensitiveColumn;}

	public String getHasSensitiveColumn(){return this.hasSensitiveColumn;}

	public void setAdmitNull(String admitNull){this.admitNull=admitNull;}

	public String getAdmitNull(){return this.admitNull;}

	public void setDataLinkDmlEnabled(String dataLinkDmlEnabled){this.dataLinkDmlEnabled=dataLinkDmlEnabled;}

	public String getDataLinkDmlEnabled(){return this.dataLinkDmlEnabled;}

	public void setLogicalReplication(String logicalReplication){this.logicalReplication=logicalReplication;}

	public String getLogicalReplication(){return this.logicalReplication;}


	@Override
	public String toString() {
		return "UserTableOracle{"+
			"tableName="+tableName+","+
			"tablespaceName="+tablespaceName+","+
			"clusterName="+clusterName+","+
			"iotName="+iotName+","+
			"status="+status+","+
			"pctFree="+pctFree+","+
			"pctUsed="+pctUsed+","+
			"iniTrans="+iniTrans+","+
			"maxTrans="+maxTrans+","+
			"initialExtent="+initialExtent+","+
			"nextExtent="+nextExtent+","+
			"minExtents="+minExtents+","+
			"maxExtents="+maxExtents+","+
			"pctIncrease="+pctIncrease+","+
			"freelists="+freelists+","+
			"freelistGroups="+freelistGroups+","+
			"logging="+logging+","+
			"backedUp="+backedUp+","+
			"numRows="+numRows+","+
			"blocks="+blocks+","+
			"emptyBlocks="+emptyBlocks+","+
			"avgSpace="+avgSpace+","+
			"chainCnt="+chainCnt+","+
			"avgRowLen="+avgRowLen+","+
			"avgSpaceFreelistBlocks="+avgSpaceFreelistBlocks+","+
			"numFreelistBlocks="+numFreelistBlocks+","+
			"degree="+degree+","+
			"instances="+instances+","+
			"cache="+cache+","+
			"tableLock="+tableLock+","+
			"sampleSize="+sampleSize+","+
			"lastAnalyzed="+lastAnalyzed+","+
			"partitioned="+partitioned+","+
			"iotType="+iotType+","+
			"temporary="+temporary+","+
			"secondary="+secondary+","+
			"nested="+nested+","+
			"bufferPool="+bufferPool+","+
			"flashCache="+flashCache+","+
			"cellFlashCache="+cellFlashCache+","+
			"rowMovement="+rowMovement+","+
			"globalStats="+globalStats+","+
			"userStats="+userStats+","+
			"duration="+duration+","+
			"skipCorrupt="+skipCorrupt+","+
			"monitoring="+monitoring+","+
			"clusterOwner="+clusterOwner+","+
			"dependencies="+dependencies+","+
			"compression="+compression+","+
			"compressFor="+compressFor+","+
			"dropped="+dropped+","+
			"readOnly="+readOnly+","+
			"segmentCreated="+segmentCreated+","+
			"resultCache="+resultCache+","+
			"clustering="+clustering+","+
			"activityTracking="+activityTracking+","+
			"dmlTimestamp="+dmlTimestamp+","+
			"hasIdentity="+hasIdentity+","+
			"containerData="+containerData+","+
			"inmemory="+inmemory+","+
			"inmemoryPriority="+inmemoryPriority+","+
			"inmemoryDistribute="+inmemoryDistribute+","+
			"inmemoryCompression="+inmemoryCompression+","+
			"inmemoryDuplicate="+inmemoryDuplicate+","+
			"defaultCollation="+defaultCollation+","+
			"duplicated="+duplicated+","+
			"sharded="+sharded+","+
			"externallySharded="+externallySharded+","+
			"externallyDuplicated="+externallyDuplicated+","+
			"external="+external+","+
			"hybrid="+hybrid+","+
			"cellmemory="+cellmemory+","+
			"containersDefault="+containersDefault+","+
			"containerMap="+containerMap+","+
			"extendedDataLink="+extendedDataLink+","+
			"extendedDataLinkMap="+extendedDataLinkMap+","+
			"inmemoryService="+inmemoryService+","+
			"inmemoryServiceName="+inmemoryServiceName+","+
			"containerMapObject="+containerMapObject+","+
			"memoptimizeRead="+memoptimizeRead+","+
			"memoptimizeWrite="+memoptimizeWrite+","+
			"hasSensitiveColumn="+hasSensitiveColumn+","+
			"admitNull="+admitNull+","+
			"dataLinkDmlEnabled="+dataLinkDmlEnabled+","+
			"logicalReplication="+logicalReplication+
		"}";
	}
}