package com.gitee.oeoe.tisimple.util;

/**
 * 单线程控制台百分比进度体条
 *
 * @author zhu qiang
 * @since 1.8+ 2022/01/29
 */
public class ConsolePerProgress implements Progress {

    private double max = 1.0;
    private int maxLength = 1;
    private double index = 0;
    private int lastStrSize = 0;

    @Override
    public void open(double max) {
        this.max = max;
        this.maxLength = String.valueOf(max).length();
    }

    @Override
    public void complete() {
        System.out.println();
    }

    @Override
    public void change() {
        change(1);
    }

    @Override
    public void change(double degree) {
        change("", degree);
    }

    @Override
    public void change(String prefix, double degree) {
        index = index + degree;
        double per = index / max * 100;
        String log = prefix + "[" + String.format("%" + (maxLength + 1) + ".2f/%.2f", index, max) + "][" + String.format("%6.2f", per) + "%]";
        print(log);
    }

    private void print(String log) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < lastStrSize; i++) {
            b.append("\b");
        }
        System.err.print(b);
        this.lastStrSize = log.length();
        System.err.print(log);
        if (index >= max) {
            complete();
        }
    }
}
