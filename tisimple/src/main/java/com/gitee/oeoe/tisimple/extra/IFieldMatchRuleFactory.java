package com.gitee.oeoe.tisimple.extra;

/**
 * 字段sql生成工厂，用于生成查询语句时，自定义列匹配规则
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public interface IFieldMatchRuleFactory {
    /**
     * @param sqlBuilder 将生成的sql片段放入其中，如果返回的时候长度为0，将不做任何处理
     * @param columnName 表列名
     * @param val        当前列匹配值
     * @return 如果已经将val值使用，生成到sqlBuilder中，必须返回null，否则返回的值将作为列名的匹配值用于设置预编译值。
     */
    Object sql(StringBuilder sqlBuilder, String columnName, Object val);
}
