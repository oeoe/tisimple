package com.gitee.oeoe.tisimple.entityfactory.mysql;

import com.gitee.oeoe.tisimple.entityfactory.ITableInfoFactory;
import com.gitee.oeoe.tisimple.entityfactory.TableColumnInfo;
import com.gitee.oeoe.tisimple.entityfactory.TableInfo;

import java.util.HashMap;
import java.util.List;

/**
 * mysql 数据库表信息工厂
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/12
 */
public class MysqlTableInfoFactory implements ITableInfoFactory {

    @Override
    public TableInfo info(String tableName) {
        MysqlSchemaTable tableCondition = new MysqlSchemaTable();
        tableCondition.setTableName(tableName);
        List<MysqlSchemaTable> table = tableCondition.query();
        if (table.size() != 1) {
            return null;
        }
        TableInfo tableInfo = new TableInfo();
        tableInfo.setTableName(tableName);
        tableCondition.setTableComment(table.get(0).getTableComment());

        MysqlSchemaTableColumn columnCondition = new MysqlSchemaTableColumn();
        columnCondition.setTableName(tableName);
        List<MysqlSchemaTableColumn> columns = columnCondition.query();
        if (columns.isEmpty()) {
            return tableInfo;
        }
        HashMap<String, TableColumnInfo> columnInfos = new HashMap<>();
        for (MysqlSchemaTableColumn x : columns) {
            TableColumnInfo columnInfo = new TableColumnInfo();
            columnInfo.setColumnName(x.getColumnName());
            columnInfo.setDbType(x.getDataType());
            columnInfo.setColumnComment(null == x.getColumnComment() || x.getColumnComment().trim().length() < 1 ? null : x.getColumnComment().trim());
            columnInfo.setKey("PRI".equals(x.getColumnKey()));
            columnInfo.setNullAble("YES".equals(x.getIsNullable()));
            columnInfo.setAutoIncrement("auto_increment".equalsIgnoreCase(x.getExtra()));
            columnInfos.put(x.getColumnName(), columnInfo);
        }
        tableInfo.setColumns(columnInfos);
        return tableInfo;
    }

    @Override
    public boolean exist(String tableName) {
        MysqlSchemaTable tableCondition = new MysqlSchemaTable();
        tableCondition.setTableName(tableName);
        Long count = tableCondition.count();
        return count == 1;
    }
}
