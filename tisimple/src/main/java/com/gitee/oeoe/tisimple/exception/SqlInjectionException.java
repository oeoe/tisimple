package com.gitee.oeoe.tisimple.exception;

/**
 * SQL注入异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class SqlInjectionException extends Exception {
    public SqlInjectionException(String message) {
        super(message);
    }
}
