package com.gitee.oeoe.tisimple.exception;

/**
 * 包装异常类-删除异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class DeleteException extends RuntimeException {
    public DeleteException(String message) {
        super(message);
    }

    public DeleteException(String message, Throwable cause) {
        super(message, cause);
    }
}
