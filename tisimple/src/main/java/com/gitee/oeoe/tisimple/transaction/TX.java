package com.gitee.oeoe.tisimple.transaction;

import com.gitee.oeoe.tisimple.exception.TxException;
import com.gitee.oeoe.tisimple.transaction.impl.DefaultTxManager;

import java.util.LinkedHashMap;

/**
 * 事务工厂
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/15
 */
public class TX {
    private static final LinkedHashMap<Class<? extends ITxManager>, Class<? extends ITxManager>> txPool = new LinkedHashMap<>();
    //默认的事务管理器
    private static Class<? extends ITxManager> defaultManager = DefaultTxManager.class;

    static {
        register(DefaultTxManager.class);
    }

    /**
     * 注册一个事务管理器
     *
     * @param manager 事务管理器
     */
    public static void register(Class<? extends ITxManager> manager) {
        txPool.put(manager, manager);
    }

    /**
     * 更新默认的事务管理器
     *
     * @param manager 事务管理器
     */
    public static void use(Class<? extends ITxManager> manager) {
        if (!txPool.containsValue(manager)) {
            register(manager);
        }
        defaultManager = manager;
    }

    /**
     * 开启一个新事务
     *
     * @return ITxManager
     */
    public static ITxManager open() {
        try {
            ITxManager tx = defaultManager.newInstance();
            tx.open();
            return tx;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new TxException("创建事务失败", e);

        }
    }

}
