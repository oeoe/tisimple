package com.gitee.oeoe.tisimple.template;

/**
 * sql 模板方法
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/8
 */
public interface SqlFunc {

    /**
     * 自定义模版方法公共接口
     *
     * @param template 模版sql
     * @param p        主参数
     * @param param    额外(副)参数
     * @return 结果
     */
    String apply(ISQL template, Object p, Object[] param);
}
