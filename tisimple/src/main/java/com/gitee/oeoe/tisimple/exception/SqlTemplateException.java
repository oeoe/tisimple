package com.gitee.oeoe.tisimple.exception;

/**
 * SQL模板异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class SqlTemplateException extends RuntimeException {
    public SqlTemplateException(String message) {
        super(message);
    }

    public SqlTemplateException(String message, Throwable cause) {
        super(message, cause);
    }
}
