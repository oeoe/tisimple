package com.gitee.oeoe.tisimple.exception;

/**
 * 包装异常类-更新异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class UpdateException extends RuntimeException {
    public UpdateException(String message) {
        super(message);
    }

    public UpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
