package com.gitee.oeoe.tisimple.exception;

/**
 * 事务提交异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class TxCommitException extends RuntimeException {
    public TxCommitException(String message) {
        super(message);
    }

    public TxCommitException(String message, Throwable cause) {
        super(message, cause);
    }
}
