package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.exception.InsertException;
import com.gitee.oeoe.tisimple.template.ISQL;
import com.gitee.oeoe.tisimple.template.SqlFactory;

import java.util.List;

/**
 * 提供基于模版sql新增接口
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/26
 */
public interface ITemplateInsertAbility<T> {

    /**
     * 新增
     *
     * @param sql 使用模版语法的sql语句
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    int insert(String sql) throws InsertException;

    /**
     * 新增
     *
     * @param template sql模版对象
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    int insert(ISQL template) throws InsertException;

    /**
     * 批量模式新增-多记录-高级，将开启独立的事务进行插入，主键若指定了生成器将自动赋值，或指定为自增，也会自动赋值
     *
     *
     * @param entities 列表对象
     * @return 插入成功的数量
     * @throws InsertException 插入各个阶段出现异常
     */
    int insert(List<T> entities) throws InsertException;
}