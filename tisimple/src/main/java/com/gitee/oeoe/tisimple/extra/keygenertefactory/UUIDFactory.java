package com.gitee.oeoe.tisimple.extra.keygenertefactory;

import com.gitee.oeoe.tisimple.extra.IKeyGenerateFactory;

import java.util.UUID;

/**
 * 生成一个UUID
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/9
 */
public class UUIDFactory implements IKeyGenerateFactory<String> {
    @Override
    public String key() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
