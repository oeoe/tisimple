package com.gitee.oeoe.tisimple.entityfactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 表信息
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/29
 */
public class TableInfo {
    private String tableName;

    private String tableComment;

    private Map<String, TableColumnInfo> columns=new HashMap<>();

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public Map<String, TableColumnInfo> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, TableColumnInfo> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        return "TableInfo{" +
                "tableName='" + tableName + '\'' +
                ", tableComment='" + tableComment + '\'' +
                ", columns=" + columns +
                '}';
    }
}
