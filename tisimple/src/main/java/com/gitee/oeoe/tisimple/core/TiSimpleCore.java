package com.gitee.oeoe.tisimple.core;

import com.gitee.oeoe.tisimple.annotation.TiColumn;
import com.gitee.oeoe.tisimple.exception.CastEntityException;
import com.gitee.oeoe.tisimple.exception.ConditionNotMetException;
import com.gitee.oeoe.tisimple.exception.SqlInjectionException;
import com.gitee.oeoe.tisimple.exception.UnPackResultSetException;
import com.gitee.oeoe.tisimple.template.ISQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 越简单越好用，越直接越高效
 * <p>
 * TiSimpleCore 是Connection功能的简单封装，提供有限且易于使用的功能，主要完成的了sql的执行与结果的映射
 *
 * @author zhu qiang
 * @see TiDBPoolManager
 * @since 1.8+ 2022/1/25
 */
public class TiSimpleCore {
    private static final Logger logger = LoggerFactory.getLogger(TiSimpleCore.class);

    public static final Pattern patternSqlInjection = Pattern.compile("\\b(and|exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare|or)\\b|([*;+'%])");

    /**
     * 将 unpackOne 方法返回的结果解析为一个对象
     * <p>
     * 需要实体和数据库的字段类型绝对匹配，否则实体的属性不会被处理
     *
     * @param cls  实体类型，不能是基本类型，且必须保留空参构造函数
     * @param data 表数据
     * @param <T>  实体类型
     * @return 由T类型实体构成的列表，如果数据空，则会返回null
     * @throws CastEntityException 实体转换异常
     * @see TiSimpleCore#unpackOne(ResultSet)
     */
    public static <T> T toEntityOne(Class<T> cls, Map<String, Object> data) throws CastEntityException {
        if (null == data || data.isEmpty()) {
            return null;
        }
        try {
            Field[] fields = cls.getDeclaredFields();
            T t = cls.newInstance();
            for (Field f : fields) {
                TiColumn alis = f.getDeclaredAnnotation(TiColumn.class);
                if (null != alis && alis.nonColumn()) {
                    continue;
                }
                String aName = f.getName();
                if (null != alis) {
                    aName = alis.value();
                }
                Object o = data.get(aName);
                if (null == o) {
                    continue;
                }
                String oTName = o.getClass().getTypeName();
                String fTName = f.getType().getTypeName();
                if (fTName.equals(oTName)) {
                    f.setAccessible(true);
                    f.set(t, o);
                } else {
                    StringBuilder log = new StringBuilder("类：").append(cls.getTypeName());
                    log.append(" 属性名称：").append(f.getName());
                    log.append(" 定义类型：").append(fTName);
                    log.append(" 实际类型：").append(oTName);
                    log.append(" ,类型不一致,属性将忽略处理");
                    logger.error(String.valueOf(log));
                }
            }
            return t;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CastEntityException("转换实体对象失败", e);
        }
    }

    /**
     * 将 unpackList 方法返回的结果解析为list对象
     *
     * @param cls     实体类型，不能是基本类型，且必须保留空参构造函数
     * @param data    表数据
     * @param <T>实体类型
     * @return 由T类型实体构成的列表，如果解析失败，返回一个空list
     * @throws CastEntityException 实体转换异常
     * @see TiSimpleCore#unpackList(ResultSet)
     */
    public static <T> List<T> toEntityList(Class<T> cls, List<Map<String, Object>> data) throws CastEntityException {
        LinkedList<T> list = new LinkedList<>();
        for (Map<String, Object> datum : data) {
            T t = toEntityOne(cls, datum);
            if (t != null) {
                list.add(t);
            }
        }
        return list;
    }

    /**
     * 将ResultSet 解析为map对象，只会解析第一条数据
     *
     * @param resultSet 查询结果
     * @return 查询结果解析结果
     * @throws UnPackResultSetException 解析过程中
     */
    public static Map<String, Object> unpackOne(ResultSet resultSet) throws UnPackResultSetException {
        Map<String, Object> map = new LinkedHashMap<>();
        ResultSetMetaData metaData;
        try (ResultSet rs = resultSet) {
            metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount() + 1;
            if (rs.next()) {
                for (int i = 1; i < columnCount; i++) {
                    String name = metaData.getColumnLabel(i);
                    Object o = rs.getObject(name);
                    map.put(name, o);
                }
            }
        } catch (SQLException e) {
            throw new UnPackResultSetException("解析ResultSet失败", e);
        }
        return map;
    }

    /**
     * 将ResultSet 解析为list-map对象
     *
     * @param resultSet 查询结果
     * @return 解析结果
     * @throws UnPackResultSetException 解析过程中出现的异常
     */
    public static List<Map<String, Object>> unpackList(ResultSet resultSet) throws UnPackResultSetException {
        LinkedList<Map<String, Object>> list = new LinkedList<>();
        ResultSetMetaData metaData;
        try (ResultSet rs = resultSet) {
            metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount() + 1;
            while (rs.next()) {
                LinkedHashMap<String, Object> map = new LinkedHashMap<>();
                for (int i = 1; i < columnCount; i++) {
                    String name = metaData.getColumnLabel(i);
                    Object o = rs.getObject(name);
                    map.put(name, o);
                }
                list.add(map);
            }
        } catch (SQLException e) {
            throw new UnPackResultSetException("解析ResultSet失败", e);
        }
        return list;
    }


    /**
     * 低级查询-单记录
     *
     * @param connection 数据库连接
     * @param template   模版sql
     * @return 一个map结果
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     */
    public static Map<String, Object> queryOne(Connection connection, ISQL template) throws SQLException, UnPackResultSetException {
        try {
            connection.setReadOnly(true);
            String parsedSql = template.getParsedSql();
            logger.debug("*Precompiled SQL  :\n\t{}\n*Refer SQL  :\n\t{}", parsedSql, template.getReferSql());
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection cnn = connection; PreparedStatement ps = cnn.prepareStatement(template.getParsedSql())) {
            template.assemble(ps);
            ResultSet rs = ps.executeQuery();
            return unpackOne(rs);
        }
    }

    /**
     * 低级查询-多记录
     *
     * @param connection 数据库连接
     * @param template   模版sql
     * @return 多个map结果
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     */
    public static List<Map<String, Object>> queryList(Connection connection, ISQL template) throws SQLException, UnPackResultSetException {
        try {
            connection.setReadOnly(true);
            String parsedSql = template.getParsedSql();
            logger.debug("*Precompiled SQL  :\n\t{}\n*Refer SQL  :\n\t{}", parsedSql, template.getReferSql());
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection cnn = connection; PreparedStatement ps = cnn.prepareStatement(template.getParsedSql())) {
            template.assemble(ps);
            ResultSet rs = ps.executeQuery();
            return unpackList(rs);
        }
    }

    /**
     * 低级更新和删除
     *
     * @param connection 数据库连接
     * @param template   模版sql
     * @return 删除或更新的数据量
     * @throws SQLException sql执行过程出现的异常
     */
    public static int updateOrDelete(Connection connection, ISQL template) throws SQLException {
        try {
            String parsedSql = template.getParsedSql();
            logger.debug("*Precompiled SQL  :\n\t{}\n*Refer SQL  :\n\t{}", parsedSql, template.getReferSql());
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection cnn = connection; PreparedStatement ps = cnn.prepareStatement(template.getParsedSql())) {
            template.assemble(ps);
            return ps.executeUpdate();
        }
    }

    /**
     * 低级插入
     *
     * @param connection 数据库连接
     * @param template   模版sql
     * @return InsertResult
     * @throws SQLException sql执行过程出现的异常
     */
    public static InsertResult insert(Connection connection, ISQL template) throws SQLException {
        boolean supportGeneratorKey;
        try {
            supportGeneratorKey = template.supportGeneratorKey();
            String parsedSql = template.getParsedSql();
            logger.debug("*Precompiled SQL  :\n\t{}\n*Refer SQL  :\n\t{}", parsedSql, template.getReferSql());
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection conn = connection; PreparedStatement ps = supportGeneratorKey ? conn.prepareStatement(template.getParsedSql(), Statement.RETURN_GENERATED_KEYS) : conn.prepareStatement(template.getParsedSql())) {
            template.assemble(ps);
            int i = ps.executeUpdate();
            InsertResult insertResult = new InsertResult();
            insertResult.setEffectRows(i);

            if (supportGeneratorKey && i > 0) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        Object key = generatedKeys.getInt(1);
                        insertResult.setReturnKey(key);
                    }
                }
            }
            return insertResult;
        }
    }

    /**
     * 低级批量插入
     *
     * @param connection 数据库连接
     * @param templates  模版sql列表
     * @return InsertResult
     * @throws SQLException sql执行过程出现的异常
     */
    public static InsertResult[] insert(Connection connection, List<ISQL> templates) throws SQLException {
        boolean supportGeneratorKey;
        if (templates.isEmpty()) {
            return new InsertResult[]{};
        }
        ISQL first = templates.get(0);
        long count = templates.stream().filter(x -> !first.equals(x)).count();
        if (count > 0) {
            connection.close();
            throw new ConditionNotMetException("批量插入失败,存在不一致的模版sql");
        }
        try {
            supportGeneratorKey = first.supportGeneratorKey();
            String parsedSql = first.getParsedSql();
            logger.debug("*Precompiled SQL Batch Mode :\n{}\n", parsedSql);
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection conn = connection; PreparedStatement ps = supportGeneratorKey ? conn.prepareStatement(first.getParsedSql(), Statement.RETURN_GENERATED_KEYS) : conn.prepareStatement(first.getParsedSql())) {
            for (ISQL template : templates) {
                template.assemble(ps);
                ps.addBatch();
                logger.debug("*Refer SQL Batch Mode :\n{}", template.getReferSql());
            }
            int[] ints = ps.executeBatch();
            InsertResult[] insertResults = new InsertResult[ints.length];
            for (int i = 0; i < ints.length; i++) {
                InsertResult insertResult = new InsertResult();
                insertResult.setEffectRows(ints[i]);
                insertResults[i] = insertResult;
            }
            if (supportGeneratorKey && ints.length > 0) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    int index = 0;
                    while (generatedKeys.next()) {
                        Object key = generatedKeys.getInt(1);
                        insertResults[index++].setReturnKey(key);
                    }
                }
            }
            return insertResults;
        }
    }

    /**
     * 中级插入
     *
     * @param template 模版sql列表
     * @return 长度为2的int数组，[0]-插入的数据量，[1]-如果存在自增主键，返回不为-1的主键自增值
     * @throws SQLException sql执行过程出现的异常
     */
    public static InsertResult insert(ISQL template) throws SQLException {
        Connection connection = TiDBPoolManager.getConnection();
        return insert(connection, template);
    }

    /**
     * 中级批量插入
     *
     * @param templates 模版sql列表
     * @return 长度为templates.size()的int数组，index对应templates中的template索引，[index][0]-插入的数据量，[index][1]-如果存在自增主键，返回不为-1的主键自增值
     * @throws SQLException sql执行过程出现的异常
     */
    public static InsertResult[] insert(List<ISQL> templates) throws SQLException {
        Connection connection = TiDBPoolManager.getConnection();
        return insert(connection, templates);
    }

    /**
     * 中级更新和删除
     *
     * @param template 模版sql
     * @return 删除或更新的数据量
     * @throws SQLException sql执行过程出现的异常
     */
    public static int updateOrDelete(ISQL template) throws SQLException {
        Connection connection = TiDBPoolManager.getConnection();
        return updateOrDelete(connection, template);
    }

    /**
     * 中级查询-单记录
     *
     * @param template 模版sql
     * @return 一条记录，无记录则返回null
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     */
    public static Map<String, Object> queryOne(ISQL template) throws SQLException, UnPackResultSetException {
        Connection connection = TiDBPoolManager.getConnection();
        return queryOne(connection, template);
    }

    /**
     * 中级查询-多记录
     *
     * @param template 模版sql
     * @return 多条记录，无记录则返回空list
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     */
    public static List<Map<String, Object>> queryList(ISQL template) throws SQLException, UnPackResultSetException {
        Connection connection = TiDBPoolManager.getConnection();
        return queryList(connection, template);
    }

    /**
     * 高级查询-单记录
     *
     * @param template 模版sql
     * @param cls      实体类型
     * @param <T>      实体类型
     * @return 一个实体对象，无记录则返回null
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     * @throws CastEntityException      实体属性和表字段的类型转换异常
     */
    public static <T> T queryOne(ISQL template, Class<T> cls) throws SQLException, UnPackResultSetException, CastEntityException {
        return toEntityOne(cls, queryOne(template));
    }

    /**
     * 高级查询-多记录
     *
     * @param template 模版sql
     * @param cls      实体类型
     * @param <T>      实体类型
     * @return 多个实体对象，无记录则返回空list
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果集中出现的异常
     * @throws CastEntityException      实体属性和表字段的类型转换异常
     */
    public static <T> List<T> queryList(ISQL template, Class<T> cls) throws SQLException, UnPackResultSetException, CastEntityException {
        return toEntityList(cls, queryList(template));
    }

    /**
     * 低级-执行存储过程
     *
     * @param connection 数据库连接
     * @param template   模版sql
     * @return CallResult 存储过程执行返回的结果集
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果出现的异常
     */
    public static CallResult execProc(Connection connection, ISQL template) throws SQLException, UnPackResultSetException {
        try {
            String parsedSql = template.getParsedSql();
            logger.debug("*Precompiled SQL  :\n\t{}\n*Refer SQL  :\n\t{}", parsedSql, template.getReferSql());
        } catch (Exception e) {
            connection.close();
            throw e;
        }
        try (Connection cnn = connection; CallableStatement ps = cnn.prepareCall(template.getParsedSql())) {
            template.assemble(ps);
            boolean hashResult = ps.execute();
            Map<Integer, List<Map<String, Object>>> moreData = new HashMap<>();
            Map<Integer, Integer> moreEffect = new HashMap<>();
            int order = 0;
            while (true) {
                //判断本次循环是否为数据集
                if (hashResult) {
                    ResultSet rs = ps.getResultSet();
                    List<Map<String, Object>> maps = unpackList(rs);
                    moreData.put(order++, maps);
                } else {
                    int updateCount = ps.getUpdateCount();
                    if (updateCount == -1) {
                        break;
                    }
                    moreEffect.put(order++, updateCount);
                }
                hashResult = ps.getMoreResults();
            }
            return new CallResult(moreData, moreEffect);
        }
    }

    /**
     * 中级-执行存储过程
     *
     * @param template 模版sql
     * @return CallResult 存储过程执行返回的结果集
     * @throws SQLException             sql执行过程出现的异常
     * @throws UnPackResultSetException 解析结果出现的异常
     */
    public static CallResult execProc(ISQL template) throws SQLException, UnPackResultSetException {
        Connection connection = TiDBPoolManager.getConnection();
        return execProc(connection, template);
    }

    /**
     * 存储过程调用返回
     */
    public static class CallResult {
        /**
         * 一条select语句对应一个项，一个结果集
         */
        private final Map<Integer, List<Map<String, Object>>> queryData;
        /**
         * 一条update或delete语句对应一个项，一个影响数据量
         */
        private final Map<Integer, Integer> updateCount;

        public CallResult(Map<Integer, List<Map<String, Object>>> queryData, Map<Integer, Integer> updateCount) {
            this.queryData = queryData;
            this.updateCount = updateCount;
        }

        public Map<Integer, List<Map<String, Object>>> getQueryData() {
            return queryData;
        }

        public Map<Integer, Integer> getUpdateCount() {
            return updateCount;
        }

        /**
         * 获取最后一条select语句的结果
         *
         * @return 有key为表字段名，value为值的map构成的列表
         */
        public List<Map<String, Object>> getLastQueryData() {
            if (null == queryData) {
                return new LinkedList<>();
            }
            Set<Integer> keys = queryData.keySet();
            Optional<Integer> max = keys.stream().max(Comparator.naturalOrder());
            Integer ke = max.orElse(0);
            List<Map<String, Object>> maps = queryData.get(ke);
            return maps == null ? new LinkedList<>() : maps;
        }

        /**
         * 获取最后一条update或delete语句的影响的记录数
         *
         * @return 记录数
         */
        public Integer getLastUpdateCount() {
            if (null == updateCount) {
                return 0;
            }
            Set<Integer> keys = updateCount.keySet();
            Optional<Integer> max = keys.stream().max(Comparator.naturalOrder());
            Integer ke = max.orElse(0);
            Integer rs = updateCount.get(ke);
            return rs == null ? 0 : rs;
        }

        @Override
        public String toString() {
            return "CallResult{" +
                    "queryData=" + queryData +
                    ", updateCount=" + updateCount +
                    '}';
        }
    }


    /**
     * insert 调用返回
     */
    public static class InsertResult {
        //插入成功的数量
        private int effectRows;
        //返回的自增值
        private Object returnKey;

        public int getEffectRows() {
            return effectRows;
        }

        public void setEffectRows(int effectRows) {
            this.effectRows = effectRows;
        }

        public Object getReturnKey() {
            return returnKey;
        }

        public void setReturnKey(Object returnKey) {
            this.returnKey = returnKey;
        }

        @Override
        public String toString() {
            return "InsertResult{" +
                    "effectRows=" + effectRows +
                    ", returnKey=" + returnKey +
                    '}';
        }
    }

    /**
     * sql注入风险检查，将只检查对象的String类型属性值
     * 容器类型将不会检查，如果要检查容器中的数据，需要手动再检查
     *
     * @param o 待检查的参数
     * @throws SqlInjectionException 存在sql注入风险异常
     */
    public static void checkSqlInjection(Object o) throws SqlInjectionException {
        if (null == o) {
            return;
        }
        if (o instanceof String) {
            Matcher matcher = patternSqlInjection.matcher(((String) o).toLowerCase());
            if (matcher.find()) {
                throw new SqlInjectionException(o + ",检测到非法字符,存在SQL注入风险！");
            }
        }
        Field[] fields = o.getClass().getDeclaredFields();
        String typeName = String.class.getTypeName();
        for (Field f : fields) {
            if (typeName.equals(f.getType().getTypeName())) {
                f.setAccessible(true);
                String val;
                try {
                    val = (String) f.get(o);
                    if (null == val || val.trim().length() < 1) {
                        continue;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    continue;
                }
                Matcher matcher = patternSqlInjection.matcher(val.toLowerCase());
                if (matcher.find()) {
                    String log = "对象：" + o +
                            " 属性名称：" + f.getName() +
                            " 值：" + val +
                            " ,检测到非法字符,存在SQL注入风险！";
                    throw new SqlInjectionException(log);
                }
            }
        }
    }

}
