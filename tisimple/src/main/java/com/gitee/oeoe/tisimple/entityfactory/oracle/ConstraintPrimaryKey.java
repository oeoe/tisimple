package com.gitee.oeoe.tisimple.entityfactory.oracle;

import com.gitee.oeoe.tisimple.ability.TiEntity;
import com.gitee.oeoe.tisimple.annotation.TiColumn;

/**
 * @author zhu qiang
 * @since 1.8+ 2022/02/06
 */
public class ConstraintPrimaryKey extends TiEntity<ConstraintPrimaryKey> {
    @TiColumn(value = "CONSTRAINT_NAME")
    private String constraintName;
    @TiColumn(value = "COLUMN_NAME")
    private String columnName;
    @TiColumn(value = "CONSTRAINT_TYPE")
    private String constraintType;

    public String getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }

    @Override
    public String toString() {
        return "ConstraintPrimaryKey{" +
                "constraintName='" + constraintName + '\'' +
                ", columnName='" + columnName + '\'' +
                ", constraintType='" + constraintType + '\'' +
                '}';
    }
}
