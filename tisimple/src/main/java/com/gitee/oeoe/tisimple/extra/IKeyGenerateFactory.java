package com.gitee.oeoe.tisimple.extra;

/**
 * 生成主键的工厂
 *
 * @param <T> 返回类型
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public interface IKeyGenerateFactory<T> {
    /**
     * 返回的值将作为对应属性的值
     *
     * @return T
     */
    T key();
}
