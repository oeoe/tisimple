package com.gitee.oeoe.tisimple.entityfactory;

/**
 * 获取表和字段的注释说明
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/12
 */
public interface ITableInfoFactory {

    /**
     * 获取表的信息
     *
     * @param tableName 表名
     * @return TableInfo 如果不存在则返回null
     */
    TableInfo info(String tableName);

    /**
     * 表是否存在
     *
     * @param tableName 表名
     * @return 是否存在，存在返回true
     */
    boolean exist(String tableName);
}
