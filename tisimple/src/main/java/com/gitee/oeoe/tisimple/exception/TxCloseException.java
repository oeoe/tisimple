package com.gitee.oeoe.tisimple.exception;

/**
 * 事务关闭异常，只为在编码的时候提醒需要执行事务提交或回滚
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/14
 */
public class TxCloseException extends Exception {
    public TxCloseException(String message) {
        super(message);
    }

    public TxCloseException(String message, Throwable cause) {
        super(message, cause);
    }
}
