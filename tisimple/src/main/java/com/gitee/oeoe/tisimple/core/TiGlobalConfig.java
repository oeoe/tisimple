package com.gitee.oeoe.tisimple.core;

import com.gitee.oeoe.tisimple.page.IPageFactory;

/**
 * 全局配置
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/11
 */
public class TiGlobalConfig {
    //全局默认分页工厂
    private static Class<? extends IPageFactory> pageFactoryClass;
    private static int globalDefaultPageSize = 10;

    public static void setGlobalPageFactory(Class<? extends IPageFactory> factory, int defaultPageSize) {
        pageFactoryClass = factory;
        globalDefaultPageSize = defaultPageSize;
    }

    public static IPageFactory getGlobalPageFactory() {
        if (null != pageFactoryClass) {
            try {
                IPageFactory factory = pageFactoryClass.newInstance();
                factory.setDefaultPageSize(globalDefaultPageSize);
                return factory;
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }
}
