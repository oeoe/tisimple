package com.gitee.oeoe.tisimple.ability;

import com.gitee.oeoe.tisimple.exception.QueryException;
import com.gitee.oeoe.tisimple.page.IPage;
import com.gitee.oeoe.tisimple.template.ISQL;

import java.util.List;
import java.util.Map;

/**
 * 提供基于模版sql的查询接口
 *
 * @author zhu qiang
 * @see ISQL
 * @since 1.8+ 2022/1/9
 */
public interface ITemplateQueryAbility<T> {

    /**
     * 查一个list-map结果，如果没有异常，必将返回一个或为空的列表，实体本身的属性将放入到参数空间
     *
     * @param sqlTemplate 使用模版语法的sql语句
     * @return 由key为表字段，value为值的map构成的列表
     * @throws QueryException 查询中各个阶段出现异常
     */
    List<Map<String, Object>> queryMap(String sqlTemplate) throws QueryException;

    /**
     * 查一个list-map结果，如果没有异常，必将返回一个或为空的列表，实体本身的属性将放入到参数空间
     *
     * @param sqlTemplate 使用模版语法的sql语句
     * @return 由key为表字段，value为值的map构成的列表
     * @throws QueryException 查询中各个阶段出现异常
     */
    List<Map<String, Object>> queryMap(ISQL sqlTemplate) throws QueryException;

    /**
     * 查一个list结果，如果没有异常，必将返回一个或为空的列表，实体本身的属性将放入到参数空间
     *
     * @param sqlTemplate 使用模版语法的sql语句
     * @return 由T类型实体构成的列表
     * @throws QueryException 查询中各个阶段出现异常
     */
    List<T> queryList(String sqlTemplate) throws QueryException;

    /**
     * 查一个list结果，如果没有异常，必将返回一个或为空的列表
     *
     * @param sqlTemplate sql模版对象
     * @return 由T类型实体构成的列表
     * @throws QueryException 查询中各个阶段出现异常
     */
    List<T> queryList(ISQL sqlTemplate) throws QueryException;

    /**
     * 查一个实体结果，如果没有异常，必将返回一个或为null的T类型实体，实体本身的属性将放入到参数空间
     *
     * @param sqlTemplate 使用模版语法的sql语句
     * @return 一个T类型的实体，如果不存在，则返回null
     * @throws QueryException 查询中各个阶段出现异常
     */
    T queryOne(String sqlTemplate) throws QueryException;

    /**
     * 查一个实体结果，如果没有异常，必将返回一个或为null的T类型实体
     *
     * @param sqlTemplate sql模版对象
     * @return 一个T类型的实体，如果不存在，则返回null
     * @throws QueryException 查询中各个阶段出现异常
     */
    T queryOne(ISQL sqlTemplate) throws QueryException;

    /**
     * 统计记录数量，实体本身的属性将放入到参数空间
     *
     * @param sqlTemplate 使用模版语法的sql语句
     * @param selectName  代表返回数量的字段名，例如 'select count(a) as rows'中的rows
     * @return 记录数量，如果给定的 selectName 无误，将不会返回null
     * @throws QueryException 查询中各个阶段出现异常
     */
    Long queryCount(String sqlTemplate, String selectName) throws QueryException;

    /**
     * 统计记录数量
     *
     * @param sqlTemplate sql模版对象
     * @param selectName  代表返回数量的字段名，例如 'select count(a) as rows'中的rows
     * @return 记录数量，如果给定的 selectName 无误，将不会返回null
     * @throws QueryException 查询中各个阶段出现异常
     */
    Long queryCount(ISQL sqlTemplate, String selectName) throws QueryException;


    /**
     * 查一个list结果，如果没有异常，必将返回一个或为空的列表，实体本身的属性将放入到参数空间
     *
     * @param sql 使用模版语法的sql语句
     * @return Page对象
     * @throws QueryException 查询中各个阶段出现异常
     */
    IPage<T> page(String sql) throws QueryException;

    /**
     * 查一个list结果，如果没有异常，必将返回一个或为空的列表
     *
     * @param template sql模版对象
     * @return Page对象
     * @throws QueryException 查询中各个阶段出现异常
     */
    IPage<T> page(ISQL template) throws QueryException;
}