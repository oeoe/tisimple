package com.gitee.oeoe.tisimple.exception;

/**
 * SQL模板方法异常
 *
 * @author zhu qiang
 * @since 1.8+ 2022/1/1
 */
public class SqlFunException extends RuntimeException {
    public SqlFunException(String message) {
        super(message);
    }

    public SqlFunException(String message, Throwable cause) {
        super(message, cause);
    }
}
